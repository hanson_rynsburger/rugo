# == Schema Information
#
# Table name: embedded_forms
#
#  id         :integer          not null, primary key
#  klass      :string(255)
#  company_id :integer
#  form_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class EmbeddedForm < ActiveRecord::Base

  # relationship
  belongs_to :form
  belongs_to :company

  # validations
  validates :company_id,  presence: true
  validates :klass,       presence: true,
                          inclusion: { in: %w(User) }
  validates :form_id,     presence: true,
                          uniqueness: { scope: :klass, case_sensitive: false }


  def self.dyno_forms(company_id, klass)
    embedded_forms = self.where(company_id: company_id, klass: klass)
    Form.where(id: embedded_forms.map(&:form_id))
  end
end
