class CreditCard
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  # constants
  # CARD_TYPES = ['American Express', 'Carte Blanche', 'China UnionPay',
  #               'Diners Club', 'Discover', 'JCB', 'Laser', 'Maestro',
  #               'MasterCard', 'Switch', 'Visa', 'Unknown']
  CARD_TYPES = ['American Express', 'MasterCard', 'Visa']


  # attributes
  attr_accessor :name, :card_type, :card_number, :cvv, :month, :year, :remember, :result, :subscription_result

  # validations
  validates :card_number, presence: true
  validates :name,        presence: true
  validates :card_type,   presence: true,
                          inclusion: { in: CARD_TYPES }


  # constructor
  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end


  # make a purchase + add credit card to customer
  #
  def purchase!(company, plan, billing_params)
    if self.valid?
      self.result = Braintree::CreditCard.create(
        :customer_id          => company.customer_uid,
        :cardholder_name      => name,
        :cvv                  => cvv,
        :number               => card_number,
        :expiration_month     => month,
        :expiration_year      => year,
        :billing_address => {

        },
        :options => {
          :verify_card                      => true,
          :fail_on_duplicate_payment_method => true,
          :make_default                     => true
        }
      )

      # continue to pay subscription if success
      # even the subscription is zero / month
      #
      if result.success?
        subscription_result = Braintree::Subscription.create(
          :payment_method_token => result.credit_card.token,
          :plan_id              => plan.uid
        )

        if subscription_result.success?
          company.subscription.update({
            plan_id:  plan.id,
            status:   subscription_result.subscription.status,
            uid:      subscription_result.subscription.id
          })
        end
      end
    end

    # return
    self
  end

end
