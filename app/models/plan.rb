# == Schema Information
#
# Table name: public.plans
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#  uid                 :string(255)
#  trial_period        :boolean          default(FALSE)
#  trial_duration      :integer
#  trial_duration_unit :string(255)
#  currency_iso_code   :string(255)
#  price               :decimal(8, 2)    default(0.0)
#  num_employees       :integer          default(0)
#

class Plan < ActiveRecord::Base

  # relationships
  has_many :companies

  # validations
  validates :name,                presence: true,
                                  uniqueness: { case_sensitive: false }
  validates :uid,                 presence: true,
                                  uniqueness: { case_sensitive: true }
  validates :currency_iso_code,   presence: true
  validates :price,               presence: true,
                                  numericality: { greater_than_or_equal_to: 0 }


  # default scoe
  default_scope { order(name: :asc) }


  # this transaction will be critical
  # because will related with paymnet
  #
  def upgrade_plan(plan_uid)
    # TO DO
  end


end
