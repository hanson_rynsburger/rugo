# encoding: UTF-8
# == Schema Information
#
# Table name: timeoffs
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  author_id         :integer
#  user_id           :integer
#  assignee_id       :integer
#  start_at          :datetime
#  end_at            :datetime
#  ancestry          :string(255)
#  attachments       :text
#  amount            :float
#  created_at        :datetime
#  updated_at        :datetime
#  properties        :hstore           default({}), not null
#  timeoff_parent_id :integer
#  timeoff_type      :string(255)
#  status            :string(255)
#  category_id       :integer
#  memo              :text
#

class Timeoff < ActiveRecord::Base
  extend Enumerize
  include PublicActivity::Model
  include Concerns::Timeoff::Flow
  include Concerns::Timeoff::Activities
  include Concerns::Timeoff::FilterAndSort

  after_save :log_activities

  # capabilities
  tracked only: [:create], owner: Proc.new { |controller, model|
    if controller.present?
      controller.current_user
    else
      model.author
    end
  }

  acts_as_followable
  acts_as_votable
  has_ancestry
  mount_uploaders :attachments, FileUploader

  enumerize :timeoff_type,  in: Enum::Timeoff::TIMEOFF_TYPE[:options],
                            default: Enum::Timeoff::TIMEOFF_TYPE[:default],
                            predicates: { prefix: true }

  enumerize :status,        in: Enum::Timeoff::STATUS[:options],
                            default: Enum::Timeoff::STATUS[:default],
                            predicates: { prefix: true },
                            scope: true

  # serialized attributes
  serialize :attachments, Array

  # relationships
  belongs_to  :author,          class_name: 'User'
  belongs_to  :user
  belongs_to  :category,        class_name: 'TimeoffPolicyCategory', foreign_key: :category_id
  belongs_to  :assignee,        class_name: 'User'
  belongs_to  :timeoff_parent,  class_name: 'Timeoff',  foreign_key: :timeoff_parent_id
  has_many    :sub_timeoffs,    class_name: 'Timeoff',  foreign_key: :timeoff_parent_id

  # validation
  validates :timeoff_type,  presence: true
  validates :name,          presence: true
  validates :status,        presence: true
  validates :category_id,   presence: true
  validates :user_id,       presence: true

  validate  :check_user_policy
  validate  :check_remaining_days

  with_options if: Proc.new{|rec| rec.timeoff_type == 'manual' || rec.timeoff_type == 'custom'} do |assoc|
    assoc.validates :author_id,     presence: true
    assoc.validates :user_id,       presence: true
  end

  with_options if: Proc.new{|rec| rec.timeoff_type == 'manual'} do |assoc|
    assoc.validates :start_at,      presence: true
    assoc.validates :end_at,        date: { after: :start_at }
  end


  # callbacks
  after_validation :calculate_hours

  # scopes
  scope :children_timeoffs,   -> (parent_id) { where(timeoff_parent_id: parent_id)}
  scope :this_year,           -> {
    beginning_of_year = Time.zone.now.utc.beginning_of_year
    end_of_year       = Time.zone.now.utc.end_of_year

    where(created_at: beginning_of_year..end_of_year)
  }
  scope :used,                    -> { where("timeoffs.amount <= ?", 0) }
  scope :added,                   -> { where("timeoffs.amount > ?", 0) }
  scope :author_by,               -> (user_id) { where(author_id: user_id) }
  scope :assigned_to,             -> (user_id) { where(assignee_id: user_id) }
  scope :owner_by,                -> (user_id) { where(user_id: user_id) }
  scope :manual_entries,          -> { where(timeoff_type: 'manual') }
  scope :manual_custom_entries,   -> { where("timeoff_type != ?", 'auto') }
  scope :newest,                  -> { order(created_at: :desc) }

  def company_id
    @company_id ||= self.author.company_id
  end

  # @todo calculate the total hours used for this task.
  def calculate_hours
    case timeoff_type
    when 'manual'
      self.amount = Time.with(self.user.policy) {self.start_at.business_time_until(self.end_at)/3600 * -1 }
    when 'auto'
      # nothing todo, will auto-fill due the timeoff policy
    when 'custom'
      # should be filled by admin manually
    end
  end

  def self.policy_class
    TimeoffRequestPolicy
  end

  private

  def check_remaining_days
    # make sure hours calculated properly
    return if self.timeoff_type_auto? || self.timeoff_type_custom?
    self.calculate_hours

    case self.category.category_type
    when 'vacation'
      unless user.total_remain_vacation_hours >= amount
        if user.policy.allow_negative
          if (user.total_remain_vacation_hours + user.allowed_borrow_vacation_hours) < amount
            errors[:base] << "#{self.user.name} doens't have enough available vacation days"
            false
          end

        else
          errors[:base] << "#{self.user.name} doens't have enough available vacation days"
          false
        end
      end

    when 'personal'
      unless user.total_remain_personal_hours >= amount
        errors[:base] << "#{self.user.name} doens't have enough available personal days"
        false
      end

    when 'sick'
      unless user.total_remain_vacation_hours >= amount
        errors[:base] << "#{self.user.name} doens't have enough available sick days"
        false
      end

    when 'others'
      # nothing to do for this category

    end
  end

  def check_user_policy
    if self.user.policy.blank?
      errors[:base] << "Please assign timeoff policy first before create timeoff"
    end
  end

  def log_activities
      activity_array = []
      # if self.workflow_state_changed?
      #   case self.workflow_state
      #   when 'approved'
      #     activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_approved], owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      #   when 'cancelled'
      #     activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_cancelled], owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      #   when 'booked'
      #     activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_booked], parameters: {data: self.id}, owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      #   when 'paid'
      #     activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_paid], parameters: {data: self.id}, owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      #   end
      # end
      #
      # if self.attachments_changed?
      #   if self.attachments_was.present?
      #     log_attachments =  self.attachments[self.attachments_was.size, self.attachments.size - 1]
      #   else
      #     log_attachments = self.attachments
      #   end
      #   if log_attachments.present?
      #     log_attachments.each do |a|
      #       activity_array << PublicActivity::Activity.new(
      #         trackable: self,
      #         owner: self.editor,
      #         key: ACTIVITY_AVAILABLE_KEYS[:task_add_attachment],
      #         parameters: {
      #           data: "uploads/#{self.class.to_s.underscore}/#{self.id}/#{a.mounted_as.to_s}/#{a.file.filename}",
      #           name: a.file.filename
      #         }
      #       ).attributes.except('id', 'created_at', 'updated_at')
      #     end
      #   end
      # end
      PublicActivity::Activity.create(activity_array) if activity_array.present?
    end
end
