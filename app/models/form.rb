# == Schema Information
#
# Table name: forms
#
#  id               :integer          not null, primary key
#  creator_id       :integer
#  company_id       :integer
#  name             :string(255)
#  introduction     :text
#  created_at       :datetime
#  updated_at       :datetime
#  icon             :string(255)
#  default          :boolean          default(FALSE)
#  show_on_calendar :boolean          default(FALSE)
#  require_sign     :boolean          default(FALSE)
#

class Form < ActiveRecord::Base
  extend Enumerize

  # relationship
  belongs_to :creator, class_name: 'User '
  belongs_to :company
  has_many   :fields,     class_name: 'FormField',    dependent: :destroy
  has_many   :approvals,  class_name: 'FormApproval', dependent: :destroy
  has_many   :approvers,  class_name: 'User',         through: :approvals, source: :approver
  has_many   :entries,    class_name: 'FormEntry',    dependent: :destroy

  # accept nested
  accepts_nested_attributes_for :fields,    reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :approvals, reject_if: :all_blank, allow_destroy: true

  # validation
  validates :name,  presence: true,
                    uniqueness: { case_sensitive: false, scope: :company_id }

  # scopes
  scope :not_default, -> {  where(default: false) }

  def date_range_field
    return unless show_on_calendar
    @date_range_field ||= fields.where(field_type: 'date_range').where("properties -> 'unremoved' = 'true'").first
  end


  def total_sub_fields
    fields.map do |field|
      config = FormField::FieldConfig[field.field_type.to_sym]
      config[:sub_fields].present? ? config[:sub_fields].length : 1
    end.sum
  end

end
