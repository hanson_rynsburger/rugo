# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  role                   :integer
#  join_on                :date
#  avatar                 :string(255)
#  status                 :integer          default(0), not null
#  employment_type        :integer          default(0), not null
#  timezone               :string(255)      default("Singapore")
#  company_id             :integer
#  team_id                :integer
#  location_id            :integer
#  backup_id              :integer
#  policy_id              :integer
#  properties             :hstore           default({}), not null
#  first_name             :string(255)
#  last_name              :string(255)
#  manager1_id            :integer
#  manager2_id            :integer
#

class User < ActiveRecord::Base
  include Concerns::Shared::DynoForm
  include Concerns::User::TimeoffHelper
  include Concerns::User::ImportExport

  # attributes
  attr_accessor :subdomain
  before_save :set_default_properties
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :confirmable, :registerable,
         :recoverable, :rememberable, :trackable, request_keys: [:subdomain]

  # serialize attribtues
  serialize :properties,    ActiveRecord::Coders::NestedHstore

  # enum
  enum role:              [:user, :corp_admin, :admin]
  enum employment_type:   [:full_time, :part_time, :contractor]
  enum status:            [:active, :terminated]


  # relationships
  belongs_to  :location
  belongs_to  :team
  belongs_to  :policy,    class_name: "TimeoffPolicy"
  belongs_to  :company
  belongs_to  :backup
  belongs_to  :manager1,  class_name: 'User'
  belongs_to  :manager2,  class_name: 'User'
  has_many    :author_timeoffs,    class_name: "Timeoff", foreign_key: 'author_id'
  has_many    :assigned_timeoffs,  class_name: "Timeoff", foreign_key: 'assignee_id'
  has_many    :timeoffs,           class_name: "Timeoff", foreign_key: 'user_id'

  # validations
  validates :timezone,    presence: true
  validates :status,      presence: true
  validates :company_id,  presence: true
  validates :join_on,     presence: true
  validates :policy_id,   presence: true

  validates :email, presence: true,
                    uniqueness: { case_sensitive: false, scope: :company_id },
                    format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  validates_presence_of     :password, if: :password_required?
  validates_confirmation_of :password, if: :password_required?
  validates_length_of       :password, within: Devise.password_length,
                            allow_blank: true

  validate  :have_valid_managers

  # abilities
  accepts_nested_attributes_for :company


  # scopes
  scope :search_employee, -> (term) {
      joins("LEFT JOIN teams ON teams.id = users.team_id")
      .joins("LEFT JOIN locations ON locations.id = users.location_id")
      .where("LOWER(users.first_name) like :search or LOWER(users.last_name) like :search or LOWER(teams.name) like :search or LOWER(locations.name) like :search",
              search: "%#{term.downcase}%")
    }

  scope :with_company, -> (company_id) {
    where(company_id: company_id)
  }

  def set_default_role
    self.role ||= :user
  end

  def set_timezone
    self.timezone ||= "Singapore"
  end

  def avatar_text
    [first_name.try(:first), last_name.try(:first)].compact.join
  end

  def location_name
    self.location.try :name
  end

  def team_name
    self.team.try :name
  end

  def short_name
    self.name ? self.name.split(' ').map{|w| w.first}.join.upcase : ''
  end

  def name
    [first_name, last_name].compact * ' '
  end

  def self.policy_class
    EmployeePolicy
  end

  def self.find_for_authentication(warden_conditions)
    User.joins(:company)
      .where(:email => warden_conditions[:email])
      .where("companies.url = ?", warden_conditions[:subdomain])
      .first
  end

  def set_default_properties
    properties ||= {}
  end

  protected

  def have_valid_managers
    if manager1_id.present? && manager2_id.present?
      if manager1_id == manager2_id
        errors.add(:manager2, "have the same value with manager1")
      end
    end
  end


  # Checks whether a password is needed or not. For validations only.
  # Passwords are always required if it's a new record, or if the password
  # or confirmation are being set somewhere.
  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

end
