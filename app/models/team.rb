# == Schema Information
#
# Table name: teams
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  ancestry    :string(255)
#  company_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Team < ActiveRecord::Base

  has_ancestry

  # relationship
  belongs_to  :company
  has_many    :users

  # validations
  validates :name, presence: true,
                   uniqueness: {  scope: :company_id, case_sensitive: false }


  # callbacks
  before_destroy :check_users

  protected

  def check_users
    if users.present?
      errors.add(:base, "Team can't be deleted because some users still related with this team")
      false
    end
  end

end
