# == Schema Information
#
# Table name: timeoff_policy_categories
#
#  id                :integer          not null, primary key
#  timeoff_policy_id :integer
#  name              :string(255)
#  category_type     :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  default           :boolean          default(FALSE)
#

class TimeoffPolicyCategory < ActiveRecord::Base
  extend Enumerize

  enumerize :category_type,   in: Enum::TimeoffPolicyCategory::CATEGORY_TYPE[:options],
                              default: Enum::TimeoffPolicyCategory::CATEGORY_TYPE[:default],
                              predicates: { prefix: true }

  # relationships
  belongs_to :timeoff_policy

  # validations
  validates :category_type,   presence: true
  validates :name,            presence: true,
                              uniqueness: { case_sensitive: false, scope: :timeoff_policy_id }


  # callbacks
  before_destroy :check_policy
  before_update  :check_policy

  # scopes
  scope :default, -> { where(default: true) }

  private

  def check_policy
    if timeoff_policy.present?
      errors.add(:base, "Timeoff category can't be updated / deleted")
      return false
    end
  end
end
