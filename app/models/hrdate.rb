# == Schema Information
#
# Table name: hrdates
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  start      :date
#  repeat     :boolean
#  day_type   :string(255)
#  company_id :integer
#  created_at :datetime
#  updated_at :datetime
#  end        :date
#

class Hrdate < ActiveRecord::Base
  extend Enumerize
  include Concerns::Hrdate::FilterAndSort

  enumerize :day_type,  in: Enum::Hrdate::DAY_TYPE[:options],
                        default: Enum::Hrdate::DAY_TYPE[:default],
                        precates: { prefix: true },
                        scope: true

  # relationships
  belongs_to :company
  has_and_belongs_to_many :locations, class_name: "Location", join_table: "locations_hrdates"


  # validations
  validates :start,     presence: true
  validates :day_type,  presence: true
  validates :name,      presence: true,
                        uniqueness: { case_sensitive: false, scope: [:company_id, :start] }

  validates :locations, length: { minimum: 1 , message: "Minimum 1 location selected" }


  def self.policy_class
    HrdatePolicy
  end

end
