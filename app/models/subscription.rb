# == Schema Information
#
# Table name: public.subscriptions
#
#  id                   :integer          not null, primary key
#  uid                  :string(255)
#  company_id           :integer
#  payment_method_token :string(255)
#  status               :string(255)
#  created_at           :datetime
#  updated_at           :datetime
#  plan_id              :integer
#

class Subscription < ActiveRecord::Base

  # extra attributes
  attr_accessor :payment_result, :address_result, :subscription_result, :result_errors

  # relationship
  belongs_to :company
  belongs_to :plan

  # validations
  validates :plan_id,     presence: true
  validates :company_id,  presence: true,
                          uniqueness: true
  # validates :status,      presence: true
  # validates :uid,         presence: true,
  #                         uniqueness: { case_sesntive: true }



  def self.policy_class
    SubscriptionPolicy
  end

  def braintree_subscription
    return nil if uid.blank?
    @braintree_subscription ||= Braintree::Subscription.find(uid)
  end

  def change_plan!(params = ())
    new_plan = Plan.where("name = ?", "plan#{params[:subscription][:plan].to_s.rjust(2, '0')}").first
    return false if new_plan.blank?
    return false if new_plan == self.plan

    # first we need to validate the employees
    if new_plan.num_employees < self.plan.num_employees
      if self.company.users.length > new_plan.num_employees
        raise Exceptions::DowngradeMaxEmployeesReached, "Downdgrade plan not allowed because company have employees more than new plan max-employees allowed"
        return false
      end
    end

    @payment_result =   Braintree::Subscription.update(
                          self.uid,
                          plan_id: new_plan.uid,
                          price: new_plan.price
                        )

    # return true / false
    if @payment_result.success?
      self.update(plan_id: new_plan.id)
      true
    end
  end


  def change_payment_method(params = {})
    set_as_default = params[:default] == '1'

    @payment_result = Braintree::PaymentMethod.create(
      customer_id:          company.customer_uid,
      payment_method_nonce: params[:payment_method_nonce],
      billing_address_id:   company.braintree_customer.addresses.first.id,
      options: {
        make_default: set_as_default
      }
    )

    if @payment_result.success?
      if set_as_default
        @subscription_result = Braintree::Subscription.update(
          self.uid,
          payment_method_token: @payment_result.payment_method.token
        )

        unless @subscription_result.success?
          @result_errors = @subscription_result.errors.map(&:message)
          false
        end
      end

      # return true
      true
    else
      @result_errors = @payment_result.errors.map(&:message)
      false
    end
  end


  def update_billing!(params = {})
    return false if self.uid.blank?

    @address_result = Braintree::Address.update(
      company.customer_uid, company.braintree_customer.addresses.first.id, {
        :company             => params[:billing_address][:company],
        :street_address      => params[:billing_address][:street_address],
        :extended_address    => params[:billing_address][:extended_address],
        :locality            => params[:billing_address][:locality],
        :region              => params[:billing_address][:region],
        :postal_code         => params[:billing_address][:postal_code],
        :country_code_alpha2 => params[:billing_address][:country_code_alpha2]
      }
    )

    # return true or false
    if @address_result.success?
      true
    else
      @result_errors = @address_result.errors.map(&:message)
      false
    end
  end

  def purchase!(params = {})
    new_plan = Plan.where("name = ?", "plan#{params[:subscription][:plan].to_s.rjust(2, '0')}").first
    return false if new_plan.blank?

    @address_result = Braintree::Address.create(
      :customer_id         => company.customer_uid,
      :company             => params[:subscription][:billing_address][:company],
      :street_address      => params[:subscription][:billing_address][:street_address],
      :extended_address    => params[:subscription][:billing_address][:extended_address],
      :locality            => params[:subscription][:billing_address][:locality],
      :region              => params[:subscription][:billing_address][:region],
      :postal_code         => params[:subscription][:billing_address][:postal_code],
      :country_code_alpha2 => params[:subscription][:billing_address][:country_code_alpha2]
    )

    if @address_result.success?
      @payment_result = Braintree::PaymentMethod.create({
        :customer_id          => company.customer_uid,
        :payment_method_nonce => params[:payment_method_nonce],
        :billing_address_id   => @address_result.address.id
      })

      # continue to pay subscription if success
      # even the subscription is zero / month
      #
      if @payment_result.success?
        @subscription_result = Braintree::Subscription.create(
          payment_method_token: @payment_result.payment_method.token,
          plan_id:              new_plan.uid,
          options: {
            start_immediately: true
          }
        )

        if @subscription_result.success?
          self.update({
            plan_id:  new_plan.id,
            status:   @subscription_result.subscription.status,
            uid:      @subscription_result.subscription.id
          })
        else
          @result_errors = @subscription_result.errors.map(&:message)
        end
      else
        @result_errors = @payment_result.errors.map(&:message)
      end
    end

    # return true or false
    @address_result && @payment_result.success? && @subscription_result.success?
  end

end
