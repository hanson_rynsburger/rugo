# == Schema Information
#
# Table name: form_fields
#
#  id         :integer          not null, primary key
#  form_id    :integer
#  field_type :string(255)
#  name       :string(255)
#  required   :boolean          default(FALSE)
#  properties :hstore
#  position   :integer
#  created_at :datetime
#  updated_at :datetime
#

class FormField < ActiveRecord::Base
  include Concerns::FormField::FieldTypeCollection

  # attributes
  serialize :properties,  ActiveRecord::Coders::NestedHstore

  # relationship
  belongs_to :form

  # validation
  validates :position,  presence: true
  validates :name,      presence: true,
                        uniqueness: { case_sensitive: false, scope: :form_id }


  # scopes
  default_scope { order(:position) }

  # instances methods
  # -----------------------------
  def as_json(options = {})
    super(options).merge(persisted: persisted?)
  end

end
