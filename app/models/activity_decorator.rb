PublicActivity::Activity.class_eval do
  after_create  :notify_user
  def notify_user
    return if !self.recipient.is_a? User
    case self.key
    when 'task.create'
      # notify assignee
      UserMailer.task_new(trackable)
    when 'task.approve'
      # notify user
      UserMailer.task_approve(trackable)
    when 'task.change_assignee'
      # notify next assignee
      UserMailer.task_change_assignee(trackable)
    else
      puts '4'
    end
  end
end
