module Concerns::Hrdate::FilterAndSort
  extend ActiveSupport::Concern

  included do

    # scopes
    scope :with_filter, ->(params = {}) {
      collection = self

      case params[:day_type]
      when 'workday'
        collection = collection.with_day_type(:workday)
      when 'holiday'
        collection = collection.with_day_type(:holiday)
      end

      if params[:start_at].present? && params[:end_at].present?
        # TO DO
      end


      # filter by keywords
      if params[:keyword].present?
        collection = collection.where("lower(hrdates.name) LIKE ?", "%#{params[:keyword].downcase}%")
      end

      # return collection
      collection.all
    }

  end
end