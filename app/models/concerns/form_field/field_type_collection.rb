module Concerns::FormField::FieldTypeCollection
  extend ActiveSupport::Concern

  FieldConfig = {
    percentage:     {type: 'number',    options: ['label', 'hint', 'required', 'range', 'placeholder']},
    boolean:        {type: 'radio',     options: ['label', 'hint', 'required', 'boolean_label']},
    checkbox:       {type: 'checkbox',  options: ['label', 'hint', 'required', 'field_options']},
    date:           {type: 'date',      options: ['label', 'hint', 'required', 'placeholder']},
    datetime:       {type: 'datetime',  options: ['label', 'hint', 'required', 'placeholder']},
    date_range:     {type: 'datetime',  options: ['label', 'hint', 'required', 'placeholder', 'date_range']},
    dropdown:       {type: 'select',    options: ['label', 'hint', 'required', 'field_options', 'prompt']},
    email:          {type: 'email',     options: ['label', 'hint', 'required', 'placeholder']},
    file:           {type: 'file',      options: ['label', 'hint', 'required', 'placeholder', 'multiple']},
    radio:          {type: 'radio',     options: ['label', 'hint', 'required', 'field_options']},
    number:         {type: 'number',    options: ['label', 'hint', 'required', 'range']},
    textarea:       {type: 'textarea',  options: ['label', 'hint', 'required', 'placeholder', 'rows']},
    price:          {type: 'number',    options: ['label', 'hint', 'required', 'currency']},
    range:          {type: 'number',    options: ['label', 'hint', 'required', 'range']},
    rating:         {type: 'radio',     options: ['label', 'hint', 'required', 'rating']},
    section:        {type: 'section',   options: ['label', 'description']},
    single_line:    {type: 'text',      options: ['label', 'hint', 'required', 'placeholder']},
    time:           {type: 'time',      options: ['label', 'hint', 'required', 'placeholder']},
    website:        {type: 'url',       options: ['label', 'hint', 'required', 'placeholder']},
    question_group: {type: 'text',      options: ['label', 'hint', 'required', 'groups']},
    advance_group:  {type: 'text',      options: ['label', 'hint', 'required', 'advance_groups']},
    expense:        {options: ['label', 'hint', 'required', 'expense']},
    travel:         {options: ['label', 'hint', 'required', 'travel']},
    bank:           {options: ['label', 'hint', 'required', 'bank'],          sub_fields: [:bank_name, :bank_type, :routing, :account_number], multiple: true},
    personal:       {options: ['label', 'hint', 'required', 'personal'],      sub_fields: [:address, :date_of_birth, :social_tax, :address_optional, :gender, :marital_status, :city, :state, :zip, :country], multiple: false},
    tax:            {options: ['label', 'hint', 'required', 'tax'],           sub_fields: [:filling_status, :decline_with_holding, :with_holding_allowance, :additional_with_holding], multiple: true},
    compensation:   {options: ['label', 'hint', 'required', 'compensation'],  sub_fields: [:amount, :currency, :period, :effective_date, :reason], multiple: true},
    emergency:      {options: ['label', 'hint', 'required', 'emergency'],     sub_fields: [:name, :relationship, :phone_number], multiple: true}
  }

  included do
    extend Enumerize
    enumerize :field_type, in: Enum::FormField::FIELD_TYPE[:options],
                           default: Enum::FormField::FIELD_TYPE[:default],
                           predicates: { prefix: true }
  end
end
