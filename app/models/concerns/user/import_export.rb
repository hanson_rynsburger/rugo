module Concerns::User::ImportExport
  extend ActiveSupport::Concern

  included do
  end

  module ClassMethods
    def disallowed_import_fields
      [ 'id', 'encrypted_password', 'reset_password_token', 'reset_password_sent_at',
        'remember_created_at', 'sign_in_count', 'current_sign_in_at', 'last_sign_in_at', 'company_id',
        'current_sign_in_ip', 'last_sign_in_ip', 'created_at', 'updated_at', 'confirmation_token',
        'confirmed_at', 'avatar', 'confirmation_sent_at', 'unconfirmed_email', 'properties' ]
    end


    def allowed_import_fields
      self.columns.map{|column| column.name} - self.disallowed_import_fields
    end


    def batch_import(company, file_path)
      begin
        file        = Roo::Spreadsheet.open(file_path, extension: :xlsx)
        sheet       = file.sheet(file.sheets.first) # get first sheet
        alphabets   = ('a'..'z').to_a.map{|c| c.upcase}
        dyno_forms  = self.respond_to?(:embedded_forms) ? self.embedded_forms : []
        assigned_attributes = {}

        # read line by line, start index: 3 (becase 2 top rows is just header)
        idx_row   = 3

        # begin looping row by row
        begin
          row     = file.row(idx_row)
          col_idx = 0

          # set static attributes
          assigned_attributes = Hash.new.tap do |hash|
            allowed_import_fields.each do |field|
              hash[field.to_s] = row[col_idx]
              col_idx += 1
            end


            # set dynamic attributes
            hash['properties'] = Hash.new.tap do |properties_hash|
              dyno_forms.each do |embedded_form|
                dyno_form = embedded_form.form
                properties_hash[dyno_form.id.to_s] = Hash.new.tap do |dyno_hash|
                  dyno_form.fields.each do |field|

                    # in this steps, we need to check what field type
                    # because some field question have sub-fields
                    # so we need to parse the data very well
                    #
                    case field.field_type
                    when 'personal', 'tax', 'bank', 'emergency', 'compensation'
                      config = FormField::FieldConfig[field.field_type.to_sym]

                      # check if it's multiple or not
                      if config[:multiple]
                        dyno_hash[field.id.to_s] = {
                          '1' =>  Hash.new.tap do |sub_hash|
                                    config[:sub_fields].sort.each do |sub_field|
                                      sub_hash[sub_field.to_s] = row[col_idx]
                                      col_idx += 1
                                    end
                                  end
                        }

                      else
                        dyno_hash[field.id.to_s] =  Hash.new.tap do |sub_hash|
                                                      config[:sub_fields].sort.each do |sub_field|
                                                        sub_hash[sub_field.to_s] = row[col_idx]
                                                        col_idx += 1
                                                      end
                                                    end
                      end

                    else
                      dyno_hash[field.id.to_s] = row[col_idx]
                      col_idx += 1
                    end

                  end
                end
              end
            end
            # end of properties
          end
          #
          # end of row checking data
          # and next is about save it into database
          #

          # puts here
          # ap assigned_attributes
          # ap "---------------------------------------------------------------------------------------------------"

          rec           = company.users.new(assigned_attributes)
          rec.password  = '123123123'
          rec.password_confirmation = '123123123'

          if rec.save
            Rails.logger.info "Successfully import user with email: #{rec.email}"
          else
            Rails.logger.info "Failed to import user with email: #{rec.email}, \n with errors: #{rec.errors.full_messages.to_sentence}"
          end

          idx_row += 1
        end until idx_row > file.last_row


      rescue IOError => e
        Rails.logger.debug e.message
      end
    end
  end

end