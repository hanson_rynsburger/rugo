module Concerns::User::TimeoffHelper
  extend ActiveSupport::Concern

  included do
    # TO DO
    after_create :adjust_timeoffs
  end

  def months_after_join
    date2 = Time.zone.now
    date1 = self.join_on.in_time_zone
    (date2.year * 12 + date2.month) - (date1.year * 12 + date1.month)
  end

  def weeks_after_join
    date2 = Time.zone.now
    date1 = self.join_on.in_time_zone
    date2.to_date.cweek - date1.to_date.cweek
  end

  def days_after_join
    date2 = Time.zone.now
    date1 = self.join_on.in_time_zone
    (date2.to_date - date1.to_date).to_i
  end

  # calculate the total vacation days this user has in this calendar year.
  # It can include days from previous years, depending on the accural type.
  # @return [Integer] the total vacation days
  def available_vacation_hours_per_year
    available_vacation_days_per_year * self.policy.work_hours_per_day
  end

  # calculate total vacation days of this year
  def available_vacation_days_per_year
    if months_after_join >= 12
      res = self.policy.base_vacation_days

      self.policy.tentures.each do |tenture|
        anniversary_months = tenture[:anniversary].to_i * 12
        if (months_after_join >= anniversary_months)
          res += tenture[:extra_days].to_i
        end
      end

      res
    else
      (self.policy.base_vacation_days * months_after_join / 12).floor
    end
  end

  def available_tenture_hours
    hours = 0
    (self.policy.tentures || []).each do |tenture|
      months = tenture['anniversary'].to_i * 12
      if months_after_join >= months
        hours += (tenture['extra_days'].to_i * policy.work_hours_per_day)
      end
    end

    # return
    hours
  end

  def available_sick_days_per_year
    if months_after_join >= 12
      self.policy.total_sick_days
    else
      (self.policy.total_sick_days * months_after_join / 12).floor
    end
  end

  def available_personal_days_per_year
    if months_after_join >= 12
      self.policy.total_personal_days
    else
      (self.policy.total_personal_days.to_i * months_after_join / 12).floor
    end
  end

  Enum::Timeoff::CATEGORY[:options].each do |category|
    define_method "total_used_#{category}_hours" do
      self.timeoffs.this_year.used
    end

    define_method "total_used_#{category}_days" do
      send("total_used_#{category}_hours") / self.policy.work_hours_per_day
    end
  end

  def allowed_borrow_vacation_days
    return 0 unless policy.allow_negative
    policy.negative_cap
  end

  def allowed_borrow_vacation_hours
    (allowed_borrow_vacation_days * self.policy.work_hours_per_day).round(2)
  end

  def total_remain_vacation_hours
    self.timeoffs.this_year.joins(:category)
                           .where("timeoff_policy_categories.category_type = ?", 'vacation')
                           .where(status: 'approved')
                           .sum(:amount)
                           .round(2)
  end

  def total_remain_vacation_days
    (total_remain_vacation_hours / self.policy.work_hours_per_day).round(2)
  end

  def total_used_vacation_hours
    self.timeoffs.this_year.joins(:category)
                           .where("timeoff_policy_categories.category_type = ?", 'vacation')
                           .where(status: 'approved')
                           .where(timeoff_type: 'manual')
                           .sum(:amount)
                           .round(2)
  end

  def total_remain_personal_hours
    self.timeoffs.this_year.joins(:category)
                           .where("timeoff_policy_categories.category_type = ?", 'personal')
                           .where(status: 'approved')
                           .sum(:amount)
                           .round(2)
  end

  def total_remain_personal_days
    (total_remain_personal_hours / self.policy.work_hours_per_day).round(2)
  end

  def total_remain_sick_hours
    return nil if self.policy.base_sick_hours.blank?
    self.policy.base_sick_hours + self.timeoffs.this_year.joins(:category)
                                                         .where("timeoff_policy_categories.category_type = ?", 'sick')
                                                         .where(status: 'approved')
                                                         .sum(:amount)
                                                         .round(2)
  end

  def total_remain_sick_days
    return nil if self.policy.base_sick_hours.blank?
    (total_remain_sick_hours / self.policy.work_hours_per_day).round(2)
  end

  # should always return positive
  #
  def total_used_sick_hours
    self.timeoffs.this_year.joins(:category)
                           .where("timeoff_policy_categories.category_type = ?", 'sick')
                           .where(status: 'approved')
                           .where(timeoff_type: 'manual')
                           .sum(:amount)
                           .round(2)
                           .abs
  end

  # TODO:
  # check if user is on vacation, depending on his vacation requests
  # @return [Boolean] true or false
  def on_vacation?
    false
  end

  # TODO:
  # check if user is sick
  # @return [Boolean] true or false
  def is_sick?
    false
  end

  private

  def adjust_timeoffs
    if policy.present? && join_on_changed? && join_on_was.nil?
      if policy.accrue_type == :accrue_by_time_adv
        BusinessTime::Config.work_hours = policy.work_hours

        case policy.accrue_rate
        when 'weekly'
          hours = policy.work_hours_a_week

          vacation_hours_per_week = (self.join_on.business_time_until(Time.zone.now.end_of_week) / hours) * (policy.base_vacation_hours.to_f / Time.zone.now.end_of_year.to_date.cweek)
          personal_hours_per_week = (self.join_on.business_time_until(Time.zone.now.end_of_week) / hours) * ((policy.total_personal_days.to_f * policy.work_hours_per_day) / Time.zone.now.end_of_year.to_date.cweek)

          # auto add vacation self
          Timeoff.create( user: self, timeoff_type: 'auto', amount: vacation_hours_per_week, category: 'vacation',
                          status: 'approved', name: 'Auto Weekly Accrue By Time Adv')

          # auto add personal hours
          Timeoff.create( user: self, timeoff_type: 'auto', amount: personal_hours_per_week, category: 'personal',
                          status: 'approved', name: 'Auto Weekly Accrue By Time Adv')


        when 'monthly'
          hours = policy.work_hours_a_month

          vacation_hours_per_week   = (self.join_on.business_time_until(Time.zone.now.end_of_month) / hours) * (policy.base_vacation_hours.to_f / 12)
          personal_hours_per_month  = (self.join_on.business_time_until(Time.zone.now.end_of_month) / hours) * ((policy.total_personal_days.to_f * work_hours_per_day.to_f) / 12)

          # auto add vacation hours
          Timeoff.create( user: self, timeoff_type: 'auto', amount: vacation_hours_per_month, category: 'vacation',
                          status: 'approved', name: 'Auto Monthly Accrue By Time Adv')

          # auto add personal hours
          Timeoff.create( user: self, timeoff_type: 'auto', amount: personal_hours_per_month, category: 'personal',
                          status: 'approved', name: 'Auto Monthly Accrue By Time Adv')


        when 'yearly'
          months = Date.today.end_of_year.month - Date.today.month

          vacation_hours_per_year = (months.to_f / 12) * policy.base_vacation_hours
          personal_hours_per_year = (months.to_f / 12) * (policy.total_personal_days * policy.work_hours_per_day)

          # auto add vacation hours
          ActiveRecord::Base::Timeoff.create( user: self, timeoff_type: 'auto', amount: vacation_hours_per_year, category: 'vacation',
                          status: 'approved', name: 'Auto Yearly Accrue By Time Adv')

          # auto add personal hours
          ActiveRecord::Base::Timeoff.create( user: self, timeoff_type: 'auto', amount: personal_hours_per_year, category: 'personal',
                          status: 'approved', name: 'Auto Yearly Accrue By Time Adv')

        end
      end
    end

    # make sure to set default again
    BusinessTime::Config.work_hours = {}
    return
  end
end