module Concerns::Timeoff::Flow
  extend ActiveSupport::Concern

  included do
    before_create :auto_fill_assignee_manager
    after_create  :send_notification_assigned
  end

  def approve!(approver)
    return unless self.status_pending?

    self.transaction do
      if self.assignee.present?
        # if current approver is by manager1, then forward to manager2
        #
        if self.user.manager1 == approver
          # check if manager2 exists
          if self.user.manager2.present?
            self.assignee_id  = self.user.manager2_id
            self.status       = 'pending'
            self.send_notification_assigned
          else
            self.status   = 'approved'
            self.assignee = nil
          end

        # if current approver is by manager2, then finish
        #
        elsif self.user.manager2 == approver
          self.status   = 'approved'
          self.assignee = nil
        end

      else
        # approved by himself
        self.status   = 'approved'
        self.assignee = nil
      end

      self.save!

      # send notification if it's approved already
      NotificationMailer.timeoff_approved(self.user.company, self, self.user).deliver if self.status_approved?

      # create activities
      self.create_activity key: 'timeoff.approve', owner: approver
    end

    # return self
    self
  end

  def cancel!(canceler)
    return unless self.status_pending? || self.status_approved?

    self.transaction do
      # hold previous assignee
      previous_assignee = self.assignee

      # mark as canceled
      self.status   = 'canceled'
      self.assignee = nil
      self.save!

      # create activities
      self.create_activity key: 'timeoff.cancel', owner: canceler

      # send notification to assignee
      if previous_assignee.present? && (previous_assignee != canceler)
        NotificationMailer.timeoff_canceled(self.user.company, self, previous_assignee).deliver
      end

      # send to owner
      unless (self.user == canceler) && (self.author != self.user)
        NotificationMailer.timeoff_canceled(self.user.company, self, self.user).deliver
      end

      # send to author
      if (self.author != canceler.id) && self.author.present?
        NotificationMailer.timeoff_canceled(self.user.company, self, self.author).deliver
      end
    end

    # return self
    self
  end

  def decline!(decliner)
    return unless self.status_pending?

    self.transaction do
      # hold previous assignee
      previous_assignee = self.assignee

      # mark as declined
      self.status   = 'declined'
      self.assignee = nil
      self.save!

      # create activities
      self.create_activity key: 'timeoff.decline', owner: decliner

      # send notification
      if previous_assignee.present? && (previous_assignee != decliner)
        NotificationMailer.timeoff_declined(self.user.company, self, previous_assignee).deliver
      end

      # send to author
      unless self.author == decliner
        NotificationMailer.timeoff_declined(self.user.company, self, self.author).deliver
      end

      # send to owner
      unless (self.user == decliner) && (self.author != self.user)
        NotificationMailer.timeoff_declined(self.user.company, self, self.user).deliver
      end
    end

    # return self
    self
  end


  protected

  def send_notification_assigned
    if self.timeoff_type_manual? && self.assignee.present? && (self.assignee != self.user)
      NotificationMailer.timeoff_assigned(self.user.company, self).deliver
    end
  end

  # will be assignee to manager1
  def auto_fill_assignee_manager
    return if self.timeoff_type_auto?

    # check if user.manager1 is present or not
    if self.user.manager1.present?

      # check if author is user.manager1
      if self.author == self.user.manager1

        # check if manager2 is present?
        if self.user.manager2.present?
          self.status   = 'pending'
          self.assignee = self.user.manager2
        else
          self.status = 'approved'
        end


      # if author is not manager1, then assign to manager1
      else
        self.assignee = self.user.manager1

      end

    elsif self.user.manager2.present?
      self.assignee = self.user.manager2
      self.status   = 'pending'


    else
      self.assignee = nil
      self.status = 'approved'

    end
  end
end
