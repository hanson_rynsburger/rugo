module Concerns::Timeoff::Activities
  extend ActiveSupport::Concern

  included do
    after_save :log_activities
  end


  def all_activities
    PublicActivity::Activity.where(
      trackable_id: self.sub_tasks.pluck(:id) << self.id,
      trackable_type: self.class.to_s
    ).order('activities.created_at desc')
  end

  def showable_activities
    all_activities.where(key: [ACTIVITY_AVAILABLE_KEYS[:task_add_attachment], ACTIVITY_AVAILABLE_KEYS[:task_new_comment], ACTIVITY_AVAILABLE_KEYS[:task_approved], ACTIVITY_AVAILABLE_KEYS[:task_cancelled]])
  end

  def self_attachment_activities
    activities.where(key: ACTIVITY_AVAILABLE_KEYS[:task_add_attachment]).order(:created_at)
  end


  protected

  def log_activities
    activity_array = []
    if self.workflow_state_changed?
      case self.workflow_state
      when 'approved'
        activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_approved], owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      when 'cancelled'
        activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_cancelled], owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      when 'booked'
        activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_booked], parameters: {data: self.id}, owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      when 'paid'
        activity_array << PublicActivity::Activity.new(trackable: self, key: ACTIVITY_AVAILABLE_KEYS[:task_paid], parameters: {data: self.id}, owner: self.editor).attributes.except('id', 'created_at', 'updated_at')
      end
    end

    if self.attachments_changed?
      if self.attachments_was.present?
        log_attachments =  self.attachments[self.attachments_was.size, self.attachments.size - 1]
      else
        log_attachments = self.attachments
      end
      if log_attachments.present?
        log_attachments.each do |a|
          activity_array << PublicActivity::Activity.new(
            trackable: self,
            owner: self.editor,
            key: ACTIVITY_AVAILABLE_KEYS[:task_add_attachment],
            parameters: {
              data: "uploads/#{self.class.to_s.underscore}/#{self.id}/#{a.mounted_as.to_s}/#{a.file.filename}",
              name: a.file.filename
            }
          ).attributes.except('id', 'created_at', 'updated_at')
        end
      end
    end

    PublicActivity::Activity.create(activity_array) if activity_array.present?
  end
end