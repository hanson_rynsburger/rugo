module Concerns::Timeoff::FilterAndSort
  extend ActiveSupport::Concern

  included do
    # will return collection of timeoff with filtered & sorted
    #
    # optional params:
    #   For filter:
    #   - type: [:all, :managed, :assigned, :owner]
    #
    #   For sort:
    #   - sort: [:author, :assignee, :date, :status, :name]
    #
    scope :filter_and_sort, -> (user_id, opt = {}) {
      collection = self.manual_custom_entries

      opt[:type] ||= 'all'
      opt[:sort] ||= 'date'
      opt[:page] ||= '1'


      # filter by type
      collection =  case opt[:type]
                    when 'managed'
                      collection.where(author_id: user_id)
                    when 'assigned'
                      collection.where(assignee_id: user_id)
                    when 'owner'
                      collection.where(user_id: user_id)
                    else
                      collection
                    end

      # filter by status
      collection =  case opt[:status]
                    when 'pending', 'approved', 'canceled', 'declined'
                      collection.where(status: opt[:status])
                    else
                      collection
                    end

      # sort by
      # collection = []
      collection =  case opt[:sort]
                    when 'author'
                      collection.joins("LEFT JOIN users AS authors ON timeoffs.author_id = authors.id")
                                .order("authors.first_name ASC")

                    when 'assignee'
                      collection.joins("LEFT JOIN users AS assignees ON timeoffs.assignee_id = assignees.id")
                                .order("assignees.first_name ASC")

                    when 'date'
                      collection.order("created_at DESC")

                    when 'status'
                      collection.order("status ASC")

                    when 'name'
                      collection.order("name ASC")

                    else
                      collection.order("created_at DESC")

                    end


      # filter by keyword
      if opt[:keyword].present?
        collection = collection.joins("INNER JOIN users AS owners ON owners.id = timeoffs.user_id
                                       LEFT JOIN teams ON owners.team_id = teams.id")
                               .where("lower(teams.name) LIKE :search OR lower(timeoffs.name) LIKE :search",
                                    search: "%#{opt[:keyword].downcase}%")
      end

      # pagination
      if opt[:page].present?
        collection = collection.page(opt[:page])
      end

      # return the collection
      collection.newest
    }
  end

end