module Concerns::FormEntry::Flow
  extend ActiveSupport::Concern

  included do
    before_create :auto_fill_approver
    after_create  :send_notification_assigned
  end


  def approve!(performer)
    return unless self.status_pending?

    approvers = form.approvers.to_a

    # if form.approvers is setup
    #
    if approvers.present?
      approvers.each_with_index do |usr, idx|
        if (usr == performer) && (approvers[idx+1]).present?
          self.approver = approvers[idx+1]
          self.status   = 'pending'

        else
          self.status   = 'approved'
          self.approver = nil

        end
      end

    # if form.approvers is not setup
    #
    else
      self.status   = 'approved'
      self.approver = nil
    end

    self.save!


    # send notification if entry just approved
    if self.status_approved?
      NotificationMailer.task_approved(self.user.company, self, self.user).deliver
    end

    # create activities
    self.create_activity key: 'form_entry.approve', owner: performer

    # return self
    self
  end

  def cancel!(performer)
    return unless self.status_pending?

    # hold previous approver
    previous_approver_id = self.approver_id

    # mark as canceled
    self.status   = 'canceled'
    self.approver = nil
    self.save!

    # create activities
    self.create_activity key: 'form_entry.cancel', owner: performer

    # send notification to approver
    if previous_approver_id.present? && (previous_approver_id != performer.id)
      NotificationMailer.task_canceled(self.user.company, self, previous_approver).deliver
    end

    # send to owner
    unless (self.user == performer)
      NotificationMailer.task_canceled(self.user.company, self, self.user)
    end

    # return self
    self
  end

  # def decline!(decliner)
  #   return unless self.status_pending?

  #   # mark as declined
  #   self.update_attribute :status, 'declined'

  #   # create activities
  #   self.create_activity key: 'form_entry.decline', owner: decliner

  #   # return self
  #   self
  # end


  private

  # will be assignee to form.approvers[0]
  def auto_fill_approver
    # check if first approver is himself
    if self.user == form.approvers.first
      self.approver = form.approvers.second

    else
      self.approver = form.approvers.first

    end
  end

  def send_notification_assigned
    if self.approver.present? && (self.approver != self.user)
      NotificationMailer.task_assigned(self.user.company, self, self.approver).deliver
    end
  end

end