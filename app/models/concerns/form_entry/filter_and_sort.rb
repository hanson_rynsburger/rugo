module Concerns::FormEntry::FilterAndSort
  extend ActiveSupport::Concern

  included do

    scope :with_filter, ->(params = {}) {
      collection = self.not_default
                       .includes(:user, :approver, :form)
                       .joins(form: :fields)

      # only filter entries with related form.show_on_calendar == true
      if params[:show_on_calendar].present?
        if (params[:show_on_calendar] == true) || (params[:show_on_calendar] == '1') || (params[:show_on_calendar] == 'true')
          collection = collection.where("forms.show_on_calendar = ?", true)
        else (params[:show_on_calendar] == false) || (params[:show_on_calendar] == '0') || (params[:show_on_calendar] == 'false')
          collection = collection.where("forms.show_on_calendar = ?", false)
        end
      end

      if params[:page].present?
        collection = collection.page(params[:page])
      end

      # return
      collection

    }

  end

end