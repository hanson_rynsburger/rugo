module Concerns::Shared::DynoForm
  extend ActiveSupport::Concern

  included do
    before_validation :process_properties
  end

  module ClassMethods
    def embedded_forms
      EmbeddedForm.where(klass: self.name)
    end
  end

  protected

  # Callback to process properties entries for all types that need a process
  #
  def process_properties
    if self.properties.present?
      EmbeddedForm.dyno_forms(self.company_id, self.class.name).each do |embedded_form|
        embedded_form.fields.each do |field|

          if self.properties[embedded_form.id.to_s].present?
            # remove nil values
            if field.field_type_checkbox?
              self.properties[embedded_form.id.to_s][field.id.to_s] = if self.properties[embedded_form.id.to_s][field.id.to_s].is_a?(String)
                                                                         JSON.parse(self.properties[embedded_form.id.to_s][field.id.to_s]).reject(&:blank?).uniq
                                                                      elsif self.properties[embedded_form.id.to_s][field.id.to_s].is_a?(Array)
                                                                         self.properties[embedded_form.id.to_s][field.id.to_s].reject(&:blank?).uniq
                                                                      end

            # Process to store attachment file
            elsif field.field_type_file?
              data = self.properties[embedded_form.id.to_s][field.id.to_s]
              if data.present?
                uploader = AttachmentUploader.new
                uploader.question_id = field.id.to_s
                uploader.form_id = form_id
                uploader.store!(data)
                self.properties[embedded_form.id.to_s][field.id.to_s] = uploader.url
              end

            end
          end
        end
      end
    end
  end


end