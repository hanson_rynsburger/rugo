module Concerns::Company::AutoSeed
  extend ActiveSupport::Concern

  # will do nothing if data_generated is true
  #
  def generate_default_data
    return if self.data_generated

    self.generate_default_timeoff_policy
    self.generate_default_dyno_forms
    self.assign_default_policy_to_users
    self.update_attribute :data_generated, true
  end

  # generate default timeoff policy for this company
  #
  def generate_default_timeoff_policy
    self.timeoff_policies.create do |policy|
      policy.name                 = 'Default'
      policy.zone                 = 'UTC'
      policy.allow_negative       = false
      policy.negative_cap         = 0
      policy.carry_over           = false
      policy.accrue_rate          = :yearly
      policy.accrue_type          = :accrue_by_time
      policy.base_vacation_days   = 20
      policy.total_personal_days  = 5
      policy.work_hours_per_day   = 8
      policy.beginning_of_workday = "9:00 AM"
      policy.end_of_workday       = "5:00 PM"
      policy.work_week            = ["mon", "tue", "wed", "thu", "fri"]
      policy.work_hours           = {
        "mon" => ["09:00", "17:00"],
        "tue" => ["09:00", "17:00"],
        "wed" => ["09:00", "17:00"],
        "thu" => ["09:00", "17:00"],
        "fri" => ["09:00", "17:00"]
      }
    end
  end

  def assign_default_policy_to_users
    default_policy = self.timeoff_policies.first

    self.users.each do |user|
      if user.policy.blank?
        user.policy = default_policy
        user.save
      end
    end
  end


  # generate default dyno-forms for this company
  # and will attached to model User (personal, emergency, bank, tax, compensation)
  #
  def generate_default_dyno_forms
    # dyno-form
    default_form = self.forms.create do |form|
      form.name     = 'User Additonal Form'
      form.default  = true

      # personal
      form.fields.build do |field|
        field.field_type  = 'personal'
        field.name        = 'Personal Information'
        field.position    = 0
        field.properties  = Hash.new.tap do |hash|
          hash['label'] = 'Personal'
          hash['personal_genders'] = [
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' }
          ]
          hash['personal_marital_statuses'] = [
            { label: 'Single', value: 'single'},
            { label: 'Married', value: 'married' },
            { label: 'Divoced', value: 'divoced' },
            { label: 'Widowed', value: 'widowed' }
          ]
        end
      end

      # emergency
      form.fields.build do |field|
        field.field_type  = 'emergency'
        field.name        = 'Emergency Contact Information'
        field.position    = 1
        field.properties  = Hash.new.tap do |hash|
          hash['label'] = 'Emergency'
        end
      end

      # bank
      form.fields.build do |field|
        field.field_type  = 'bank'
        field.name        = 'Bank Account Information'
        field.position    = 2
        field.properties  = Hash.new.tap do |hash|
          hash['label'] = 'Bank'
          hash['bank_account_types'] = [
            {label: 'Checking', value: 'checking'},
            {label: 'Saving', value: 'saving'}
          ]
        end
      end

      # tax
      form.fields.build do |field|
        field.field_type  = 'tax'
        field.name        = 'Tax Information'
        field.position    = 3
        field.properties  = Hash.new.tap do |hash|
          hash['label'] = 'Tax'
          hash['tax_filling_status_prompt'] = nil
          hash['tax_decline_with_holding_prompt'] = nil
          hash['tax_filling_status_options'] = [
            { label: 'Joint', value: 'joint' },
            { label: 'Single', value: 'single' }
          ]
          hash['tax_decline_with_holdings'] = [
            { label: 'Yes', value: '1' },
            { label: 'No', value: '0' }
          ]
        end
      end

      # compensation
      form.fields.build do |field|
        field.field_type  = 'compensation'
        field.name        = 'Compensation'
        field.position    = 4
        field.properties  = Hash.new.tap do |hash|
          hash['label'] = 'Compensation'
          hash['compensation_currencies'] = [
            { label: 'USD', value: 'usd' },
            { label: 'SGD', value: 'sgd' }
          ]
          hash['compensation_periods'] = [
            { label: 'Year',     value: 'year' },
            { label: 'Quarter',  value: 'quarter' },
            { label: 'Month',    value: 'Month' }
          ]
        end
      end
    end


    self.embedded_forms.create do |embedded_form|
      embedded_form.klass = 'User'
      embedded_form.form  = default_form
    end
  end
end