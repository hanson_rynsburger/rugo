module Concerns::Company::CompanySubscription
  extend ActiveSupport::Concern

  included do
    after_create :create_basic_subscription
  end

  protected

  # you should call this method after company created
  def create_basic_subscription
    return if self.subscription.present?

    basic_plan        = Plan.find_by name: 'plan05'
    self.subscription = Subscription.create do |rec|
                          rec.plan = basic_plan
                        end
  end

end