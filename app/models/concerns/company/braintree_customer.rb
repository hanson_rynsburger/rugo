module Concerns::Company::BraintreeCustomer
  extend ActiveSupport::Concern

  included do
    after_destroy :destroy_braintree_customer
  end

  # create customer in brainstree, for subscription
  # will do nothing if customer_uid has been created before
  #
  def create_braintree_customer
    return if customer_uid.present?

    result = Braintree::Customer.create(
      first_name: default_corp_admin.first_name,
      last_name:  default_corp_admin.last_name,
      company:    name,
      email:      email
      # :phone => "312.555.1234",
      # :fax => "614.555.5678",
      # :website => "www.example.com"
    )

    # set customer_uid
    self.update_attribute :customer_uid, result.customer.id

  end

  def destroy_braintree_customer
    begin
      return if customer_uid.blank?
      Braintree::Customer.delete(customer_uid)

    rescue Braintree::NotFoundError => e
      Rails.logger.info e.message
    end
  end

  def braintree_customer
    begin
      @braintree_customer ||= Braintree::Customer.find(customer_uid)

    rescue Braintree::NotFoundError => e
      Rails.logger.info e.message
      nil
    end
  end
end