module Concerns::Company::Tenant
  extend ActiveSupport::Concern

  def create_tenant
    begin
      if self.url.present?
        # create tenant if needed
        if self.use_tenant
          Apartment::Tenant.create(self.url)
          Rails.logger.info "Creating tenant '#{self.url}' success!"
        end
      end

    rescue Apartment::TenantExists => e
      puts e.message
    end
  end
end
