# encoding: UTF-8
# == Schema Information
#
# Table name: form_entries
#
#  id          :integer          not null, primary key
#  form_id     :integer
#  user_id     :integer
#  answers     :hstore
#  created_at  :datetime
#  updated_at  :datetime
#  approver_id :integer
#  status      :string(255)      default("pending")
#  start_at    :datetime
#  end_at      :datetime
#  description :string(255)
#

class FormEntry < ActiveRecord::Base
  extend Enumerize
  include PublicActivity::Model
  include Concerns::FormEntry::Flow
  include Concerns::FormEntry::FilterAndSort

  # extra attributes
  attr_accessor :skip_process_answers

  enumerize :status,  in: Enum::FormEntry::STATUS[:options],
                      default: Enum::FormEntry::STATUS[:default],
                      predicates: { prefix: true }

  serialize :answers, ActiveRecord::Coders::NestedHstore

  # capabilities
  tracked only: [:create], owner: Proc.new{ |controller, model| controller.present? ? controller.current_user : nil }

  # relationship
  belongs_to :user
  belongs_to :form
  belongs_to :approver, class_name: 'User'


  # validation
  validates :form, presence: true
  validates :user, presence: true
  validate  :validate_answers, if: Proc.new { |entry| entry.answers.present? }

  # callbacks
  before_validation :process_answers


  scope :not_default, -> {  joins(:form).where("forms.default = ?", false) }

  def self.policy_class
    FormEntryPolicy
  end

  # cache the fields in variable
  def fields
    @fields ||= form.fields
  end

  def upload_attachment(field_id, attachment, options = {})
    field = form.fields.find(field_id)
    return if attachment.blank?

    case field.field_type
    when 'expense'
      row_index = Integer(options[:row_index])

      self.answers[field.id.to_s] ||= {}
      self.answers[field.id.to_s][row_index] ||= {}
      self.answers[field.id.to_s][row_index]['files'] ||= []

      # now process the attachment, save it and get the url
      current_total_files   = self.answers[field.id.to_s][row_index]['files'].length
      uploader              = ExpenseUploader.new
      uploader.question_id  = field.id.to_s
      uploader.form_id      = field.form_id.to_s
      uploader.row_index    = row_index
      uploader.file_index   = current_total_files
      uploader.store!(attachment)
      self.answers[field.id.to_s][row_index]['files'] << uploader.url

      # save and skip process answers
      self.skip_process_answers = true
      self.save

      # returl url
      uploader.url
    end
  end



  def validate_answers
    # loop all fields
    fields.each_with_index do |field, idx|
      case field.field_type
      when 'single_line', 'paragraph', 'website', 'radio', 'date', 'picture_choice', 'dropdown', 'rating', 'boolean', 'file'
        if field.required && answers[field.id.to_s].blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

      when 'date_range'
        if form.show_on_calendar && ((field.properties['unremoved'] == 'true') || (field.properties['unremoved'] == '1'))
          if field.required && (start_at.blank? || end_at.blank?)
            errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
          end
        else
          if field.required && (answers[field.id.to_s]['from'].blank? || answers[field.id.to_s]['to'].blank?)
            errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
          end
        end

      when 'checkbox'
        if field.required && (answers[field.id.to_s] || []).reject(&:blank?).blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

      when 'price'
        if field.required && answers[field.id.to_s].blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

        if answers[field.id.to_s].present? && answers[field.id.to_s].try(:to_f) < 0
          errors[:base] << "#{idx+1}) #{field.properties['label']} should be greater or equal than 0"
        end

      when 'website'
        if field.required && answers[field.id.to_s].blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

      when 'percentage', 'number'
        if field.required && answers[field.id.to_s].blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

        if answers[field.id.to_s].present? && field.properties['from_number'].present? && field.properties['to_number'].present?
          value       = answers[field.id.to_s].to_f
          from_number = field.properties['from_number'].to_f
          to_number   = field.properties['to_number'].to_f

          if (value < from_number)
            errors[:base] << "#{idx+1}) #{field.properties['label']} can't be lower than #{ from_number }"
          end

          if (value > to_number)
            errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_greater_than'} #{ to_number }"
          end
        end

      when 'range'
        if field.required && (answers[field.id.to_s].blank? || (answers[field.id.to_s]['from'].blank? || answers[field.id.to_s]['to'].blank?))
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"

        elsif answers[field.id.to_s].present?
          if answers[field.id.to_s]['from'].present? && answers[field.id.to_s]['to'].present?
            value1 = answers[field.id.to_s]['from'].to_f
            value2 = answers[field.id.to_s]['to'].to_f

            from_number = field.properties['from_number'].to_f
            to_number   = field.properties['to_number'].to_f

            if ((from_number != 0.0) && ((value1 <= from_number) || (value1 >= to_number))) || ((to_number != 0.0) && ((value2 >= to_number) || (value2 <= from_number)))
              errors[:base] << "#{idx+1}) #{field.properties['label']} values should be between #{ from_number.to_i } and #{ to_number.to_i }"
            end
          end
        end

      when 'email'
        if field.required && answers[field.id.to_s].blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} #{I18n.t 'errors.cant_be_blank'}"
        end

        if answers[field.id.to_s].present? && answers[field.id.to_s].match(/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/).blank?
          errors[:base] << "#{idx+1}) #{field.properties['label']} is not a valid email format"
        end

      when 'datetime'
        if field.required
          if answers[field.id.to_s]['date'].blank? || answers[field.id.to_s]['hours'].blank? || answers[field.id.to_s]['minutes'].blank?
            errors[:base] << "#{idx+1}) #{field.properties['label']} for date, hours, and minutes #{I18n.t 'errors.cant_be_blank'}"
          end
        end

      end
    end
  end


  private

  # Callback to process answer entries for all types that need a process
  #
  def process_answers
    return if skip_process_answers

    self.answers ||= {}
    fields.each do |field|
      # remove nil values
      case field.field_type
      when 'checkbox'
        self.answers[field.id.to_s] =  if self.answers[field.id.to_s].is_a?(String)
                                         JSON.parse(self.answers[field.id.to_s]).reject(&:blank?).uniq
                                       elsif self.answers[field.id.to_s].is_a?(Array)
                                         self.answers[field.id.to_s].reject(&:blank?).uniq
                                       end

      # Process to store attachment file
      when 'file'
        data = self.answers[field.id.to_s]
        if field.properties['multiple'] == '1'
          data            ||= []
          previous_values = self.new_record? ? [] : FormEntry.find(self.id).answers.try(:[], field.id.to_s)
          previous_values ||= []

          self.answers[field.id.to_s] = previous_values + Array.new.tap do |arr|
            data.each_with_index do |item, idx|
              uploader = AttachmentUploader.new
              uploader.question_id = field.id.to_s
              uploader.form_id = form_id
              sleep 0.1
              item.read
              uploader.store!(item)
              arr << uploader.url
            end

            # return array
            arr
          end

        elsif data.present?
          uploader = AttachmentUploader.new
          uploader.question_id = field.id.to_s
          uploader.form_id = form_id
          uploader.store!(data)
          self.answers[field.id.to_s] = uploader.url

        end



      when 'expense'
        expense_rows = self.answers[field.id.to_s]
        expense_rows ||= []

        # fetch previous values
        previous_values = self.new_record? ? [] : FormEntry.find(self.id).answers.try(:[], field.id.to_s)
        previous_values ||= []

        expense_rows.each_with_index do |row, idx|
          files           = row['files']
          files           ||= []

          # fetch previous files value for each expense row
          previous_files  = previous_values[idx].try(:[], 'files')
          previous_files  ||= []

          self.answers[field.id.to_s][idx]['files'] = previous_files + Array.new.tap do |arr|
            files.each_with_index do |file, f_idx|
              uploader              = ExpenseUploader.new
              uploader.question_id  = field.id.to_s
              uploader.form_id      = form_id
              uploader.row_index    = idx
              uploader.file_index   = previous_files.length + f_idx
              sleep 0.1
              file.read
              uploader.store!(file)
              arr << uploader.url

              # return array
              arr
            end
          end
        end
        # end looping
      end
    end
  end
  # end process answers

end
