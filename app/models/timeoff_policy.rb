# encoding: UTF-8
# == Schema Information
#
# Table name: timeoff_policies
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  base_vacation_days   :integer          default(20), not null
#  accrue_type          :integer          default(0), not null
#  accrue_rate          :integer          default(0), not null
#  allow_negative       :boolean
#  negative_cap         :integer
#  carry_over           :boolean          default(FALSE), not null
#  carry_over_cap       :integer
#  carry_over_expire_on :date
#  total_sick_days      :integer
#  total_personal_days  :integer
#  work_hours_per_day   :integer          default(8), not null
#  beginning_of_workday :string(255)      default("9:00 AM"), not null
#  end_of_workday       :string(255)      default("5:00 PM"), not null
#  work_week            :text             default("---\n- mon\n- tue\n- wed\n- thu\n- fri\n"), not null
#  work_hours           :text             default("--- {}\n")
#  tentures             :text             default("--- []\n")
#  holidays             :text             default("--- []\n")
#  work_dates           :text             default("--- []\n")
#  company_id           :integer
#  zone                 :string(255)      default("UTC"), not null
#  created_at           :datetime
#  updated_at           :datetime
#

class TimeoffPolicy < ActiveRecord::Base
  extend Enumerize

  # @!attribute name
  #   @return [String] the name of the policy

  # @!attribute base_vacation_days
  #   @return [Integer] total vacation days per year

  # @!attribute accrue_rate
  #   @return [Enumerable<String>] indicate how often this policy should be ran to calculate employees vacation days balance.
  enumerize :accrue_rate, in: Enum::TimeoffPolicy::ACCRUE_RATE[:options],
                          default: Enum::TimeoffPolicy::ACCRUE_RATE[:default],
                          scope: :having_accrue_rate,
                          predicates: { prefix: true }

  # @!attribute accrue_type
  #   @return [Enumerable<String>] Vacation Data Accural Type
  #   @example  Assuming you have 20 vacation days per year (假设你每年有20天休假)
  #       :accrue_by_time  => if it is accrue per month, u will have 20/12 = 1.66 days on Feb 1st
  #       :accrue_by_time_adv  => if it is accrue per month, u will have 20/12 = 1.66 days on January 1st
  #   @note if accrue_by_time_adv and :yearly, means all days will be given to user at once
  enumerize :accrue_type, in: Enum::TimeoffPolicy::ACCRUE_TYPE[:options],
                          default: Enum::TimeoffPolicy::ACCRUE_TYPE[:default],
                          predicates: { prefix: true }

  # @!attribute tentures
  #   We’ll automatically increase your employees’ vacation accrual rate based on the milestones you set below. (These bonuses will be your default policy, but you can turn this feature off for individual employees.)
  #   根据您设置下面的里程碑，我们将自动增加员工的假期累积率。 （这些奖金将是你的默认策略，但您可以关闭此功能对于员工个人。）
  #   @return [Array<Object>] List of tenture. 年假逐年递增 年假递增率
  #   @example
  #     self.tentures = [{anniversary:2, extra_days: 2}, {anniversary:4, extra_days: 2}]
  #     means if you worked for 2 years, then you have 22 days vacation. If 4 years then u have 24 days.
  #     如果你工作了2年指，那么你有22天假期。如果4年那么你有24天假期。
  serialize :tentures, Array


  # @!attribute carry_over
  #   @return [Boolean] Carry Over ? （跨年累积）

  # @!attribute carry_over_cap
  #   @return [Integer] total carry over hours
  #   @note if carry_over_cap is ZERO, it means cannot carry over any hours.

  # @!attribute carry_over_expire_on
  #   @return [Date] total carry over hours expiration date
  #   @example
  #      carry_over：true，carry_over_cap：16， carry_over_expire_on：nil  
  #      with this setting, every year carry_over 16 hours and it never expires。 
  #      carry_over：true，carry_over_cap：16， carry_over_expire_on："12/31/1999"
  #      with this setting, every year carry_over 16 hours，it means all the accumulated vacations days will be expired at the end of the year. 
  # carry_over：true，carry_over_cap：16， carry_over_expire_on：“03/01/2000”
  #      with this setting, every year carry_over 16 hours，it means all the accumulated vacations days will be expired at the end of Feburary.
  #      carry_over：true，carry_over_cap：16， carry_over_expire_on：“06/01/2000”
  #      with this setting, every year carry_over 16 hours，it means all the accumulated vacations days will be expired at the end of May.

  # @!attribute allow_negative
  #   @return [Boolean] Do you allow user to borrow vacation days in advance?

  # @!attribute negative_cap
  #   @return [Integer] how many vacation hours that user can borrow in advance

  # @!attribute total_sick_days
  #   @return [Integer] total allowed sick days （病假）

  # @!attribute total_personal_days
  #   @return [Integer] total allowed personal days （个人年假）

  # @!attribute work_hours_per_day
  #   @return [Integer] total work hours per day

  # @!attribute beginning_of_workday
  #   @return [String] start time for every working day. '9:00 am'

  # @!attribute end_of_workday
  #   @return [Integer] end time for every working day. '5:00 pm'
  #   @example We can adjust the start and end time of our business hours
  #     self.beginning_of_workday = "8:30 am"
  #     self.end_of_workday = "5:30 pm"

  # @!attribute work_week
  #   @return [Array<String>] list of working week days. '%w(mon tue wed thu fri)'
  #   @example
  #     self.work_week = '%w(mon tue wed thu fri)'
  serialize :work_week

  # @!attribute work_hours
  #   @return [Hash] list different working hours per day. Not sure if it is necessary.
  #   @example as alternative we also can change the business hours for each work day:
  #     self.work_hours = {
  #       :mon=>["9:00","17:00"],
  #       :fri=>["9:00","17:00"],
  #       :sat=>["10:00","15:00"]
  #     }
  serialize :work_hours

  # @!attribute work_dates
  #   @return [Array<Hash>] a list of working dates, which addition to the work_week setting.
  #   @example
  #     work_dates = ["02/01/2015","02/14/2015"]
  serialize :work_dates

  # @!attribute holidays
  #   @return [Array<Hash>] a list of holidays
  #   @example
  #     holidays = [{:name=>"Indepedence Day", :date=>"10/01/2014", :repeat => true}, {:name=>"New Year", :date=>"1/01/2015", :repeat => true}]
  serialize :holidays

  belongs_to :company,  class_name: "Company"
  has_many :users,      class_name: "User", foreign_key: :policy_id
  has_many :categories, class_name: 'TimeoffPolicyCategory', foreign_key: :timeoff_policy_id

  # validations
  validates :name,        presence: true,
                          uniqueness: { case_sensitive: false, scope: :company_id }
  validates :company_id,  presence: true

  # accept nested attributes
  accepts_nested_attributes_for :categories, allow_destroy: true, reject_if: proc { |attributes| attributes['name'].blank? }

  # callbacks
  after_create :generate_default_categories
  after_create :assign_users_default_policy


  # calculate how many hours has user accrued in this policies period.
  # @todo calculate how many hours has user accrued in this policies period.
  # @example Every first day of the month, this accrue method will run and it will add the accrued amount to each user. On Nov 1st, 2014
  #   policy = TimeoffPolicy.create!(
  #     :hours_per_year => 160,
  #     :accrue_rate => :accrue_by_time,
  #     :accrue_rate => :monthly,
  #     :allow_negative => false,
  #     :carry_over => :yearly_cap,
  #     :carry_over_cap => 10,
  #     :total_personal_days => 10,
  #     :total_sick_days => 5,
  #     :work_hours_per_day => 8,
  #     :created_at => '10/1/2014'
  #   )
  #   user1 = User.find(1)
  #   user1.start_date = '10/15/2014'
  #   hours_per_month = 160 / 12
  #   if user1.start_date - Date.today < 1_month
  #     hours_per_month = hours_per_month / weeks_of_worked
  #   end
  #   TimeoffTask.create!(:sub_type => :accrue, :user => user, :amount=> hours_per_month)
  #   *********If it runs on Jan 1st, 2105************
  #   if user.total_accrue_hours > policy.carry_over_cap
  #     TimeoffTask.create!(:sub_type => :adjustment, :user => user, :amount=> policy.carry_over_cap - user.total_accrue_hours)
  #   end
  def accrue
    users.each do |user|
      TimeoffTask.create(:sub_type => :accrue, :user => user, :amount=> amount)
    end
  end


  # return base vacation in hours format (for a year)
  #
  def base_vacation_hours
    base_vacation_days * work_hours_per_day
  end

  def base_sick_days
    total_sick_days
  end

  def base_sick_hours
    total_sick_days * work_hours_per_day
  end


  # will return true / false
  #
  def carry_over_expiration
    self.carry_over_expire_on.present?
  end

  def tentures_attribute=(data)
    if data.is_a?(Hash)
      self.tentures = []
      data.each do |key, value|
        self.tentures << value
      end
    else
      self.tentures = data
    end
  end

  def tentures_attribute
    self.tentures
  end

  def work_hours_a_week
    BusinessTime::Config.work_hours = self.work_hours
    total_hours = Time.zone.now.business_time_until(Time.zone.now.end_of_week)
    BusinessTime::Config.work_hours
    total_hours / 3600
  end

  def work_hours_a_week
    BusinessTime::Config.work_hours = self.work_hours
    total_hours = Time.zone.now.business_time_until(Time.zone.now.end_of_month)
    BusinessTime::Config.work_hours
    total_hours / 3600
  end

  # compatible with business_time
  def each (&block)
    # XXX: _weekdays
    keys = ['beginning_of_workday', 'end_of_workday', 'work_week', 'work_hours']
    keys.each do |k|
      yield k, self.attributes[k]
    end

    # reformat work_dates
    # yield 'work_dates', self.work_dates.map{|i| Date.parse(i)}
    yield 'work_dates', self.company.hrdates.with_day_type(:workday).map{ |workday| workday.start }

    # reformat holidays
    yield 'holidays', self.company.hrdates.with_day_type(:holiday).inject([]) { |res, holiday|
      start_date  = holiday.start
      end_date    = holiday.end

      # calculate if hrdate is have range
      if start_date.present? && end_date.present?
        if holiday.repeat
          this_start_date = start_date.change(year: Date.today.year)
          this_end_date   = end_date.change(year: Date.today.year)
          res += (this_start_date..this_end_date).to_a

          next_start_date = start_date.change(year: Date.today.year + 1)
          next_end_date   = end_date.change(year: Date.today.year + 1)
          res += (next_start_date..next_end_date).to_a
        else
          res += (start_date..end_date).to_a
        end


      # if only one day
      else
        date = holiday.start
        if holiday.repeat
          res << date.change(year: Date.today.year)
          res << date.change(year: Date.today.year + 1)
        else
          res << date
        end
      end

      # return res
      res
    }
  end

  protected

  def generate_default_categories
    Enum::TimeoffPolicyCategory::CATEGORY_TYPE[:options].each do |item|
      self.categories.create do |category|
        category.default        = true
        category.category_type  = item.to_s
        category.name           = case item
                                  when :personal
                                    'Personal Holiday'
                                  else
                                    item.capitalize
                                  end
      end
    end
  end

  def assign_users_default_policy
    company.reload
    policies = company.timeoff_policies

    if company.timeoff_policies.to_a.size == 1
      Rails.logger.info "This is the first policy for company #{company.url} with Policy ID: #{self.id}"

      company.users.where(policy_id: nil).each do |user|
        user.update_attribute :policy_id, self.id
        Rails.logger.info "Assign default polict to user #{user.email}"
      end
    end
  end
end
