# == Schema Information
#
# Table name: locations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  company_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Location < ActiveRecord::Base

  # relationship
  belongs_to :company
  has_many :users
  has_and_belongs_to_many :hrdates, class_name:"Hrdate", join_table:"locations_hrdates"

  # validations
  validates :company_id,  presence: true
  validates :name,        presence: true,
                          uniqueness: { scope: :company_id, case_sensitive: false }


  # callbacks
  before_destroy :check_users

  protected

  def check_users
    if users.present?
      errors.add(:base, "Location can't be deleted because some users still related with this location")
      false
    end
  end

end
