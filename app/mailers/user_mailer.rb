class UserMailer < ActionMailer::Base
  layout 'email'
  default from: "system@#{Rails.application.secrets.domain_name}"

  def task_new(task)
    @task = task
    mail(to: task.assignee.email,
         subject: "New task: " + task.name).deliver_later
  end

  def task_approve(task)
    @task = task
    mail(to: task.user.email,
         subject: "Task approved: " + task.name).deliver_later
  end

  def task_change_assignee(task)
    @task = task
    mail(to: task.assignee.email,
         subject: "Task changed: " + task.name).deliver_later
  end
end
