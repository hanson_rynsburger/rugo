class CompanyMailer < ActionMailer::Base
  layout 'email'
  default from: "system@#{Rails.application.secrets.domain_name}"

  def welcome_notification(company_id)
    @company = Company.find(company_id)
    mail( to: @company.email,
          subject: "Welcome to EasyHR")
  end

  def resend_confirmation(company_id)
    @company = Company.find(company_id)
    mail( to: @company.email,
          subject: "Confirmation Instruction")
  end
end
