class NotificationMailer < ActionMailer::Base
  layout 'email'
  default from: "system@#{Rails.application.secrets.domain_name}"

  def timeoff_assigned(company, timeoff)
    @company  = company
    @timeoff  = timeoff
    @assignee = @timeoff.assignee

    mail( to: @assignee.email,
          subject: "New Timeoff Assigned To You")
  end

  def timeoff_canceled(company, timeoff, target_user)
    @company = company
    @timeoff = timeoff
    @user    = target_user

    mail( to: @user['email'],
          subject: "Timeoff has been canceled" )

  end

  def timeoff_declined(company, timeoff, target_user)
    @company  = company
    @timeoff  = timeoff
    @user     = target_user

    mail( to: @user['email'],
          subject: "Timeoff has been declined" )

  end

  def timeoff_approved(company, timeoff, target_user)
    @company  = company
    @timeoff  = timeoff
    @user     = target_user

    mail( to: @user['email'],
          subject: "Timeoff has been approved" )
  end

  def task_assigned(company, entry, target_user)
    @company      = company
    @form_entry   = entry
    @user         = target_user

    mail( to: @user['email'],
          subject: "New Task entry assigned to you" )
  end

  def task_canceled(company, entry, target_user)
    @company      = company
    @form_entry   = entry
    @user         = target_user

    mail( to: @user['email'],
          subject: "Task entry just canceled" )
  end

  def task_approved(company, entry, target_user)
    @company      = company
    @form_entry   = entry
    @user         = target_user

    mail( to: @user['email'],
          subject: "Task entry just approved" )
  end

  # private

  # def switch_tenant(company)
  #   Apartment::Tenant.switch!(company['url']) if company['use_tenant']
  # end
end
