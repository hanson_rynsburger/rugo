json.extract! @task, :id, :name, :description, :author_id, :user_id, :assignee_id, :start_at, :end_at, :type, :status, :attachments, :details, :created_at, :updated_at
