json.extract! @task, :id, :name, :description, :start_at, :end_at, :type, :workflow_state, :attachments, :details, :created_at, :updated_at
json.url task_url(@task, format: :json)
json.user do
  json.name @task.user.present? ? @task.user.name : ''
  json.id @task.user.present? ? @task.user.id : ''
end
json.author do
  json.name @task.author.present? ? @task.author.name : ''
  json.id @task.author.present? ? @task.author.id : ''
end
json.assignee do
  json.name @task.assignee.present? ? @task.assignee.name : ''
  json.id @task.assignee.present? ? @task.assignee.id : ''
end
