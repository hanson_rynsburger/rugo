json.array!(@teams) do |team|
  json.extract! team, :id, :name, :description, :company_id, :handle_travel, :handle_accounting
  json.url team_url(team, format: :json)
end
