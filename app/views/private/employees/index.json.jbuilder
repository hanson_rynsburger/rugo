json.employees @users do |user|
  json.extract! user, :id, :name, :avatar_text, :location_name, :team_name
end
json.total_count @users.total_count
