json.tasks do
  json.array!(@entries) do |task|
    json.extract! task, :id, :user_id, :approver_id, :status, :answers, :created_at, :updated_at, :start_at, :end_at
    json.url company_form_entry_url(task, format: :json)
    if !task.user.nil?
      json.user do
        json.name task.user.name
        json.id task.user.id
      end
    end
    json.approver do
      json.name task.approver.present? ? task.approver.name : ''
      json.id task.approver.present? ? task.approver.id : ''
    end
    json.form do
      json.name task.form.name
      json.id task.form.id
    end
  end
end

json.forms do
  json.array!(@forms) do |form|
    json.name form.name
    json.id form.id
    json.icon form.icon
    json.introduction form.introduction
    json.show_on_calendar form.show_on_calendar
    json.require_sign form.require_sign
  end
end
