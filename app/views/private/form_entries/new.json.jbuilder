json.form do
  json.name @entry.form.name
  json.id @entry.form.id
  json.icon @entry.form.icon
  json.introduction @entry.form.introduction
  json.show_on_calendar @entry.form.show_on_calendar
  json.require_sign @entry.form.require_sign
end
