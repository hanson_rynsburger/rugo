class Companies::AdminsController < Devise::RegistrationsController
  layout 'visitors'
  before_action :set_company, only: [:new, :create]

  def new
    @user = @company.users.first || @company.users.new
    respond_with(@company)
  end

  # POST /resource
  def create
    authorize_company_token!

    # force to switch into company tenant
    if @company.use_tenant
      Apartment::Tenant.switch!(@company.url)
    else
      Apartment::Tenant.switch!
    end

    build_resource(sign_up_params)

    resource.role       = 'corp_admin'
    resource.email      = @company.email
    resource.company    = @company
    resource.policy     = @company.default_policy
    resource.join_on    = Time.zone.now
    resource.skip_confirmation!

    resource_saved = resource.save
    yield resource if block_given?
    if resource_saved

      # about company stuff
      @company.token   = nil
      @company.status  = 'confirmed'
      @company.save
      @company.create_braintree_customer

      sign_out :user
      set_flash_message :notice, :signed_up
      sign_in :user, resource, bypass: true
      respond_with resource, location: after_sign_in_path_for(resource)

    else
      clean_up_passwords resource
      # flash[:error] = resource.errors.full_messages.join('<br />')
      respond_with(resource)
    end
  end

  protected

  def after_sign_in_path_for(resource)
    company_company_root_url(subdomain: resource.company.url)
  end

  def set_company
    @company = Company.find_by(token: params[:token])
  end

  def authorize_company_token!
    if params[:token].blank?
      flash[:error] = 'You need to sign up your company first'
      return redirect_to new_company_company_path
    end
  end

  def update_params
    params.require(:user).permit(:password, :password_confirmation,
                                  :first_name, :last_name,
                                  company_attributes: [:id, :url])
  end

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name)
  end
end
