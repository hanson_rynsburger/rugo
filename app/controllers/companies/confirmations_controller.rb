class Companies::ConfirmationsController < ApplicationController
  layout 'visitors'
  respond_to :html

  # GET - sign_up_companies_path
  #
  def new
    @company = Company.new
    respond_with(@company)
  end


  # POST - resend confirmation email
  #
  def create
    @company = Company.where(email: params[:company][:email])
                      .where(status: :new)
                      .first

    if @company.blank?
      flash.now[:error] = 'Company not found'
    else
      CompanyMailer.resend_confirmation(@company.id).deliver
    end
    respond_with(@company, location: companies_welcome_path)
  end


  # GET - confirm_company_path
  #
  def edit
    respond_with(company)
  end


  # PATCH -
  def update
    company.update_attributes(company_params)
    company.create_tenant if company.use_tenant
    CompanyTenantJob.new.async.perform(company.id)
    respond_with(company, location: new_companies_admin_path(company.token))
  end


  protected

  def company_params
    params.require(:company).permit(:email)
  end

  def company
    @company ||= Company.find_by(token: params[:id])
  end

  def company_params
    params.require(:company).permit(:url, :use_tenant)
  end
end
