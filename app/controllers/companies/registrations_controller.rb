class Companies::RegistrationsController < ApplicationController
  prepend_before_filter :require_no_authentication, only: [ :new, :create, :index ]
  layout 'visitors'
  respond_to :html

  # GET - sign_up_companies_path
  #
  def new
    @company = Company.new
    respond_with(@company)
  end

  # POST - companies_path
  #
  def create
    @company = Company.new(company_params)
    @company.save
    respond_with(@company, location: companies_welcome_path)
  end

  protected

  def require_no_authentication
    if user_signed_in?
      flash[:alert] = I18n.t("devise.failure.already_authenticated")
      redirect_to dashboard_company_employees_url(subdomain: current_user.company.url)
    end
  end

  def company_params
    params.require(:company).permit(:name, :email)
  end
end