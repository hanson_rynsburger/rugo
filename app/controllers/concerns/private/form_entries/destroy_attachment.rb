module Concerns::Private::FormEntries::DestroyAttachment
  extend ActiveSupport::Concern

  # DELETE - destroy_attachment_company_form_entry_path(entry_id, :field_id)
  # required params:
  #  - order: index of attachment, if multiple == 1
  #
  def destroy_attachment
    if field.properties['multiple'] == '1'
      entry.answers[field.id.to_s].delete_at(Integer(params[:index]))
    else
      entry.answers[field.id.to_s] = nil
    end
    entry.skip_process_answers = true
    entry.save

    respond_with [:edit, :company, entry]
  end


  # DELETE - destroy_expense_attachment_company_form_entry_path(entry_id, field_id, row_index, file_index)
  #
  def destroy_expense_attachment
    entry.answers[field.id.to_s][Integer(params[:row_index])]['files'].delete_at(Integer(params[:file_index]))
    entry.skip_process_answers = true
    entry.save

    respond_with [:edit, :company, entry]
  end

end