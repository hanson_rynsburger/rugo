module Concerns::Private::FormEntries::UploadAttachment
  extend ActiveSupport::Concern


  # # POST - upload_attachment_company_form_entry_path(id, field_id)
  # # require params:
  # #   - file
  # #
  # def upload_attachment
  #   entry.upload_attachment(params[:field_id], params[:file])
  #   respond_with [:company, entry]
  # end

  # POST - upload_expense_attachment_company_form_entry_path(id, field_id, row_index)
  # require params:
  #   - file
  #
  def upload_expense_attachment
    uploaded_url = entry.upload_attachment(params[:field_id], params[:file], {
      row_index: params[:row_index]
    })

    respond_to do |format|
      format.json { render json: { url: uploaded_url } }
    end
  end

end