module Trackable
  extend ActiveSupport::Concern

  included do
    before_filter :activities, :only => [:show,:edit]
  end

  def activities
    @trackable = find_trackable
    @activities = @trackable.activities.order(created_at: :desc)
    @activity   = PublicActivity::Activity.new
  end

  private

  def find_trackable
    controller = params[:controller]
    controller.slice!("private/")
    return controller.singularize.classify.constantize.find(params[:id])
  end


end
