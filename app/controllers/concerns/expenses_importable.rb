module ExpensesImportable
  COLUMN_MAPPING = {
    date: 0,
    mode_of_payment: 1,
    type_of_expense: 2,
    purpose_of_expense: 3,
    transaction_currency: 4,
    transaction_amount: 5,
    exchange_rate: 6,
    expense_detail: 7
  }

  def import_expenses(file)
    import_expense_list = []
    invalid_expense_list = []
    result  = open_spreadsheet(file)
    if result[:success]
      spreadsheet = result[:file]
    else
      return result
    end
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = spreadsheet.row(i)
      expense_attributes = {
        start_at: getDataRow(row, :date),
        details: {
          category: getDataRow(row, :type_of_expense),
          purpose: getDataRow(row, :purpose_of_expense),
          payment: getDataRow(row, :mode_of_payment),
          rate: getDataRow(row, :exchange_rate),
          amount: getDataRow(row, :transaction_amount),
          currency: getDataRow(row, :transaction_currency),
          memo: getDataRow(row, :expense_detail)
        }
      }

      task = Task.new expense_attributes

      if task.valid?
        import_expense_list << task
      else
        invalid_expense_list += task.error.full_messages.collect{ |msg| "Row #{i}: #{msg}"}
      end
    end
      result_import_list = { import_expense_list: import_expense_list, invalid_expense_list: invalid_expense_list, success: true}
  end

  def getDataRow(row, rec_attr)
    value = row[COLUMN_MAPPING[rec_attr]]
    arr = [:mode_of_payment, :type_of_expense, :purpose_of_expense, :transaction_currency]
    if arr.include?(rec_attr)
      value = value.downcase.gsub(/[ ]/, "_")
    end
    value
  end

  def open_spreadsheet(file)
    begin
      openFile = case File.extname(file.original_filename)
        when '.csv' then Roo::CSV.new(file.path, file_warning: :ignore)
        when '.xls' then Roo::Excelx.new(file.path, file_warning: :ignore)
        when '.xlsx' then Roo::Excelx.new(file.path, file_warning: :ignore)
      end
      { success: true, file: openFile}
    rescue
      { success: false, messages: I18n.t('tasks.messages.unknown_file_type')}
    end
  end
end
