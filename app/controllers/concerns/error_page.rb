module Concerns::ErrorPage
  extend ActiveSupport::Concern

  included do
    unless Rails.application.config.consider_all_requests_local
      rescue_from Pundit::NotAuthorizedError,     with: :user_not_authorized
      rescue_from ActiveRecord::RecordNotFound,   with: :render_404
      rescue_from Braintree::NotFoundError,       with: :render_404
      rescue_from Exception,                      with: :render_500
    end
  end

  # Fancy page for 500 errors.
  def render_500(exception)
    notify_exception exception
    render "/errors/internal_server_error", layout: 'error'
  end

  # Fancy page for 404 errors.
  def render_404(exception)
    render "/errors/not_found", layout: 'error'
  end

  protected

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    puts policy_name
    flash.now[:danger] = t "#{policy_name}.#{exception.query}", scope: "timeoff_policies.pundit"
    redirect_to(request.referrer || root_path)
  end

  # Send email of the exception
  def notify_exception(exception)
    Rails.logger.info "Exception: #{exception.inspect}"
    ExceptionNotifier.notify_exception(exception, :env => request.env)
  end

end