module EmployeesImportable
  COLUMN_MAPPING = {
      status_after_import: 0,
      first_name: 1,
      last_name: 2,
      personal_email: 3,
      start_day: 4,
      backup_id: 5,
      manager: 6,
      work_email: 7,
      personal_phone: 8,
      office_phone: 9,
      employee_type: 10,
      permission: 11,
      job_type: 12,
      location: 13,
      employment_status: 14,
      dob: 15,
      social_sec_or_tax_id: 16,
      gender: 17,
      marital_status: 18,
      address: 19,
      address2: 20,
      city: 21,
      state: 22,
      post_code: 23,
      country: 24,
      emergency_contact_first_name: 25,
      emergency_contact_last_name: 26,
      emergency_contact_relation: 27,
      emergency_contact_phone: 28,
      bank_account_name: 29,
      bank_account_type: 30,
      bank_account_route_number: 31,
      bank_account_number: 32,
      compensation_amount: 33,
      compensation_currency: 34,
      compensation_earn_period: 35,
      compensation_date_effective: 36,
      compensation_reason_for_change: 37,
      hardware_laptop: 38,
      hardware_badge: 39,
      hardware_car: 40,
      team_name: 41
    }

    def import_employees(file, current_company_id)
      import_employee_list = []
      invalid_employee_list = []
      result  = open_spreadsheet(file)
      if result[:success]
        spreadsheet = result[:file]
      else
        return result
      end
      header = spreadsheet.row(2)
      (3..spreadsheet.last_row).each do |i|
        row = spreadsheet.row(i)
        user_attributes = {
          email: getDataRow(row, :personal_email),
          password: "12345678",
          password_confirmation: "12345678",
          join_on: getDataRow(row, :start_day),
          name:  [getDataRow(row, :first_name), getDataRow(row, :last_name)].compact.join(" "),
          personal: {
            first_name: getDataRow(row, :first_name),
            last_name: getDataRow(row, :last_name),
            street1: getDataRow(row, :address),
            street2: getDataRow(row, :address2),
            city: getDataRow(row, :city),
            state: getDataRow(row, :state),
            country: getDataRow(row, :country),
            gender: getDataRow(row, :gender),
            dob: getDataRow(row, :dob),
            marital_status: getDataRow(row, :marital_status),
            zip: getDataRow(row, :post_code),
            personal_phone: getDataRow(row, :personal_phone)
          },
          employment: {
            work_email: getDataRow(row, :work_email),
            office_phone_number: getDataRow(row, :office_phone),
            start_date: getDataRow(row, :start_day),
            job_tittle: getDataRow(row, :job_type)
            },
          location: getDataRow(row, :location),
          team_name: getDataRow(row, :team_name),
          status: getDataRow(row, :employment_status),
          bank: [{
            name: getDataRow(row, :bank_account_name),
            account_type: getDataRow(row, :bank_account_type),
            id_number: getDataRow(row, :bank_account_route_number),
            account_number: getDataRow(row, :bank_account_number)
          }],
          emergency: [{
            name: [getDataRow(row, :emergency_contact_first_name), getDataRow(row, :emergency_contact_last_name)].compact.join(" "),
            relationship: getDataRow(row, :emergency_contact_relation),
            phone: getDataRow(row, :emergency_contact_phone)
          }],
          compensation: [{
            effective: getDataRow(row, :compensation_date_effective),
            amount: getDataRow(row, :compensation_amount),
            currency: getDataRow(row, :compensation_currency),
            reason: getDataRow(row, :compensation_reason_for_change),
            per: getDataRow(row, :compensation_earn_period )
          }]
        }

        backup_user_email = getDataRow(row, :backup_id)
        managers_email = getDataRow(row, :manager)

        location = user_attributes.delete(:location)
        team = user_attributes.delete(:team_name)


        user = User.new user_attributes.except(:location, :team_name)

        if backup_user_email.present?
          backup_user = User.where(email: backup_user_email, company_id: current_company_id).first
          user.backup_id = backup_user.id if backup_user
          user.employment['backup_email'] = backup_user_email
        end

        if managers_email.present?
          emails = managers_email.to_s.gsub(/\s+/, '').split(",")
          managers_id = User.where(email: emails, company_id: current_company_id).pluck(:id)

          user.employment['manager_id'] = managers_id
          user.employment['manager_emails'] = managers_email
        end


        if location.present?
          location = Location.find_or_create_by(name: location, company_id: current_company_id)
          user.location_id = location.id
        end

        if team.present?
          team = Team.find_or_create_by(name: team, company_id: current_company_id)
          user.team_id = team.id
        end

        if user.valid?
          import_employee_list << user.as_json(methods: [:location_name, :team_name])
        else
          invalid_employee_list += user.errors.full_messages.collect{|msg| "Row #{i-2}: #{msg}"}
        end
      end

      result_import_list = {import_employee_list: import_employee_list, invalid_employee_list: invalid_employee_list, success: true}
    end


  private
    def getDataRow(row, rec_attr)
      value = row[COLUMN_MAPPING[rec_attr]]
      arr = [:employment_status, :gender, :bank_account_type, :compensation_currency, :marital_status]
      if arr.include?(rec_attr)
        if value.present?
          value = value.downcase
        end
      end
      value
    end

    def open_spreadsheet(file)
      begin
        openFile = case File.extname(file.original_filename)
          when '.csv' then Roo::CSV.new(file.path, file_warning: :ignore)
          when '.xls' then Roo::Excelx.new(file.path, file_warning: :ignore)
          when '.xlsx' then Roo::Excelx.new(file.path, file_warning: :ignore)
        end
        { success: true, file: openFile }
      rescue
        { success: false, message: I18n.t('users.index.import_table.unknown_file_type') }
       end
    end
end