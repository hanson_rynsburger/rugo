require 'axlsx'
require 'roo'

class Private::Employees::ExportImportsController < Private::Employees::ModuleController

  # GET - company_employees_export_imports_path
  #
  def index
    user_dyno_forms
    @users = current_company.users

    respond_to do |format|
      format.xlsx
    end
  end


  # GET - template_company_employees_export_imports_path(format: 'xlsx')
  #
  def template
    user_dyno_forms
    @user = current_company.users.new(
      join_on:  Time.zone.now,
      policy:   current_company.timeoff_policies.first
    )

    respond_to do |format|
      format.xlsx
    end
  end

  # GET - new_company_employees_export_import_path
  #
  def new; end


  # POST - company_employees_export_imports_path
  #
  def create
    User.batch_import(current_company, params[:employees][:file].path)
    redirect_to company_employees_path
  end

end
