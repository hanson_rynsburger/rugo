# encoding: UTF-8

require 'csv'

class Private::FormEntriesController < Private::ModuleController
  include ExpensesImportable
  include Concerns::Private::FormEntries::DestroyAttachment
  include Concerns::Private::FormEntries::UploadAttachment
  respond_to :html, :json

  # GET - company_form_entries_path
  #
  def index
    @forms    = current_company.forms.not_default
    @entries  = current_company.form_entries.includes(:form).with_filter(params)
    respond_with(@entries)
  end


  # GET - all_company_form_entries_path
  #
  def all
    @forms    = current_company.forms.not_default
    @entries  = current_company.form_entries.with_filter(params)

    respond_to do |format|
      format.json {
        render json: @entries.to_json(
          methods: [:calendar_start_on, :calendar_end_on],
          include: [:form])
      }
    end
  end


  # GET - new_company_form_entry_path(form_id)
  #
  def new
    @entry = form.entries.new
  end


  # POST - company_form_entry_entries_path(form_id)
  #
  def create
    @entry = form.entries.new(entry_attributes)
    @entry.user = current_user

    unless @entry.save
      flash[:error] = @entry.errors.full_messages.join('<br />').html_safe
    end

    respond_with @entry, location: [:company, @entry]
  end


  # GET - edit_company_form_entry_path(form_id, entry_id)
  #
  def edit
    @form = entry.form
    respond_with(@entry)
  end


  # PUT - company_form_entry_path(form_id, entry_id)
  #
  def update
    entry.update_attributes(entry_attributes)
    respond_with @entry, location: [:company, @entry]
  end

  def show
    @form = entry.form
    respond_with(entry)
  end

  # PUT -
  #
  def change_status
    case params[:status]
    when 'cancel'
      authorize entry, :cancel?
      entry.cancel!(current_user)
    when 'approve'
      authorize entry, :approve?
      entry.approve!(current_user)
    end

    respond_with entry, location: [:company, :tasks]
  end


  private

  def entry_attributes
    params.fetch(:form_entry, {}).permit(:status, :description, :start_at, :end_at, :answers).tap do |whitelisted|
      whitelisted[:answers] = params[:form_entry].try(:[], :answers)
    end
  end

  def entry
    @entry ||= current_company.form_entries.includes(:form => [:fields]).find(params[:id])
  end

  def field
    @field ||= entry.form.fields.find(params[:field_id])
  end

  def form
    @form ||= current_company.forms.find(params[:form_id])
  end

end
