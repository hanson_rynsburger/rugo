class Private::ActivitiesController < Private::ModuleController
  before_action :set_activity, only: [:show, :edit, :update, :destroy]
  # before_filter :load_trackable
  respond_to :html, :json

  def index
    @activities = PublicActivity::Activity.all
    respond_with(@activities)
  end

  def show
    respond_with(@activity)
  end

  def new
    @trackable = find_trackable
    @activity = PublicActivity::Activity.new( :trackable_id => @trackable.id,
                            :trackable_type => @trackable.class.to_s)
    respond_with(@activity)
  end

  def edit
  end

  def create
    @trackable = find_trackable
    @activity = @trackable.activities.build(activity_params)
    @activity.owner = current_user
    flash[:notice] = 'Comment was successfully created.' if @activity.save
    respond_with([:company, @trackable])
  end

  def update
    flash[:notice] = 'Comment was successfully updated.' if @activity.update(activity_params)
    respond_with(@activity)
  end

  def destroy
    @activity.destroy
    respond_with(@activity)
  end

  def load_trackable(trackable_type,trackable_id)
    @trackable = trackable_type.camelize.constantize.find(trackable_id)
  end

  private
    def set_activity
      @activity = PublicActivity::Activity.find(params[:id])
    end

    def activity_params
      params.require(:public_activity_activity).permit(:key, {:parameters => [:msg]}, :trackable_id, :trackable_type, :owner_id, :owner_type, :recipient_id, :recipient_type)
    end

    def find_trackable
      params.each do |name, value|
        if name =~ /(.+)_id$/
          return "#{$1}".classify.constantize.find(value)
        end
      end
      nil
    end
end
