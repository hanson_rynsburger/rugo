class Private::LocationsController < Private::ModuleController
  respond_to :html, :json
  include ApplicationHelper

  def index
    @location  = Location.new
    respond_with locations
  end

  def create
    @location = locations.create(location_params)
    respond_with @location, location: company_locations_path
  end


  def edit
    location
  end

  def update
    location.update(location_params)
    respond_with location, location: company_locations_path
  end

  def destroy
    if location.destroy
      flash[:notice] = "Success destroy location '#{location.name}'"
    else
      flash[:error] = location.errors.full_messages.join('<br />').html_safe
    end

    respond_with location, location: company_locations_path
  end

  private

  def location_params
    params.require(:location).permit(:name, :description)
  end

  def locations
    @locations ||= current_company.locations
  end

  def location
    @location ||= current_company.locations.find params[:id]
  end
end
