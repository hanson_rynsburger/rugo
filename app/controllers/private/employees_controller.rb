class Private::EmployeesController < Private::ModuleController
  respond_to :html, :json
  include ApplicationHelper
  include EmployeesImportable
  before_action :authorize_limit_employees!, only: [:new, :create]

  def index
    authorize User
    @users = get_user_list(current_company)

    respond_with @users
  end

  def new
    authorize User
    @user  = User.new
    user_dyno_forms
  end

  def create
    authorize User
    @user = current_company.users.new(user_params)
    user_password = Devise.friendly_token.first(8)
    @user.password = user_password
    @user.password_confirmation = user_password

    unless @user.save
      user_dyno_forms
    end

    respond_with(@user, location: [:company, :employees])
  end

  def show
    authorize user
  end

  def edit
    authorize user, :update?
    user_dyno_forms
  end

  def update
    authorize user

    unless user.update_attributes(user_params)
      user_dyno_forms
    end

    respond_with(user, location: [:company, :employees])
  end

  def destroy
    authorize user
    flash[:notice] = 'User deleted' if user.destroy
    respond_with(user, location: [:company, :employees])
  end

  def bulk_terminate
    users = policy_scope(User.where(id: params[:uid]))
    authorize users
    result = users.update_all(:status => params[:status])

    respond_to do |format|
      format.json {
        render json: result
      }
    end
  end

  def bulk_location
    users = policy_scope(User.where(id: params[:uid]))
    authorize users
    users.update_all(:location_id => params[:location_id])

    respond_to do |format|
      format.json {
        render json: result
      }
    end
  end

  def bulk_team
    users = policy_scope(User.where(id: params[:uid]))
    authorize users
    users.update_all(:team_id => params[:team_id])

    respond_to do |format|
      format.json {
        render json: result
      }
    end
  end

  def import_file
    authorize User
    company_id = current_company.id
    result = import_employees(params[:file], company_id)
    respond_to do |format|
      format.json {
        render json: result.as_json
      }
    end
  end

  def dashboard; end

  private

  def authorize_limit_employees!
    unless current_plan.num_employees > current_company.users.length
      redirect_to :back, alert: "You don't have enough slot to add more user"
    end
  end

  def user
    @user ||= current_company.users.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation,
                                  :timezone, :join_on, :location_id, :team_id, :email, :policy_id,
                                  :first_name, :last_name,
                                  :manager1_id, :manager2_id,
                                  company_attributes: [:id, :url]).tap do |whitelisted|
                                    whitelisted[:properties] = params[:user][:properties]
                                  end
  end

  def get_user_list(company)
    users = company.users.includes(:location,:team)
    users = users.where(status: 0)

    if params[:name].present?
      users = users.search_employee(params[:name])
    end

    if params[:sort_by].present?
      if params[:sort_by] == 'name'
        users = users.order(first_name: :asc)
      elsif params[:sort_by] == 'team'
        if params[:name].present?
          users = users.order("teams.name asc")
        else
          users = users.joins("LEFT JOIN teams ON teams.id = users.team_id").order("teams.name asc")
        end
      else
        if params[:name].present?
          users = users.order("locations.name asc")
        else
          users = users.joins("LEFT JOIN locations ON locations.id = users.location_id").order("locations.name asc")
        end
      end
    else
      users = users.order(created_at: :desc)
    end
    users.page((params[:page] || START_PAGE).to_i).per((params[:per_page] || PER_PAGE).to_i)
  end

end
