class Private::CalendarsController < Private::ModuleController
  respond_to :html, :json

  def index
    @hrdates = current_company.hrdates.with_filter(params)
    respond_with @hrdates
  end


  # GET - new_company_calendar_path
  #
  def new
    @hrdate = current_company.hrdates.new
    authorize @hrdate
    respond_with @hrdate
  end

  # POST - company_calendars_path
  #
  def create
    @hrdate = current_company.hrdates.new(hrdate_params)
    flash[:error] = @hrdate.errors.full_messages.join('<br />').html_safe unless @hrdate.save
    respond_with @hrdate, location: company_calendars_path
  end

  def edit
    respond_with hrdate
  end

  private

  def hrdate_params
    params.require(:hrdate).permit(:name, :start, :end, :repeat, :day_type, location_ids: [])
  end

  def hrdate
    @hrdate ||= current_company.hrdates.find params[:id]
  end

end