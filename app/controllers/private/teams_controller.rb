class Private::TeamsController < Private::ModuleController
  include ApplicationHelper
  respond_to :html, :json

  def index
    @team = Team.new
    @teams = current_company.teams
  end

  def new
    @team = Team.new
    respond_with(@team)
  end

  def edit
    team
  end

  def create
    @team = current_company.teams.create(team_params)
    respond_with([:company, :teams])
  end

  def update
    team.update(team_params)
    respond_with([:company, :teams])
  end

  def destroy
    if team.destroy
      flash[:notice] = "Success delete team '#{team.name}'"
    else
      flash[:error] = team.errors.full_messages.join('<br />').html_safe
    end
    respond_with([:company, :teams])
  end

  private

  def team
    @team ||= current_company.teams.find params[:id]
  end

  def team_params
    params.require(:team).permit(:name, :description)
  end
end
