class Private::ModuleController < ApplicationController
  before_action :authenticate_user!
  before_action :check_subdomain

  layout :determine_layout

  def current_policy
    @default_policy ||= current_user.policy
  end
  helper_method :current_policy


  around_filter :set_time_zone

  private

  def check_subdomain
    unless request.subdomain == current_user.company.url
      redirect_to root_url(subdomain: nil), alert: "You are not authorized to access that subdomain."
    end
  end

  def set_time_zone(&block)
    time_zone = current_user.try(:timezone) || 'UTC'
    Time.use_zone(time_zone, &block)
  end

  def determine_layout
    (request.xhr? || params[:modal] == 'true') ? 'ajax' : 'application'
  end

  def authenticate_timeoff_policy!
    if current_policy.blank?
      flash[:error] = "You don't have Timeoff Policy assigned, please contact your administrator"
      return redirect_to company_company_root_path
    end
  end
end