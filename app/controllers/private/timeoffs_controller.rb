class Private::TimeoffsController < Private::ModuleController
  include Trackable
  before_filter :authenticate_timeoff_policy!
  respond_to :html, :json

  # GET - company_timeoffs_path
  #
  def index
    @all_timeoffs = current_company.timeoffs.manual_entries
    @timeoffs     = current_company.timeoffs.filter_and_sort(current_user.id, params)
    respond_with @timeoffs
  end

  # GET - logs_company_timeoffs_path
  #
  def logs
    @timeoffs = current_company.timeoffs.filter_and_sort(current_user.id, params.merge(status: 'approved'))
    respond_with @timeoffs
  end

  def show
    @user = timeoff.user
  end

  def new
    @timeoff = current_user.timeoffs.new(timeoff_type: params[:type], user_id: params[:user_id])
    @user    = current_user
    authorize @timeoff

    respond_with @timeoff
  end


  # POST - company_timeoffs_path
  #
  def create
    @timeoff = current_user.author_timeoffs.new(timeoff_params)
    authorize @timeoff
    @user = @timeoff.user
    @timeoff.save
    # if @timeoff.save
    #   @user         = @timeoff.user
    #   flash[:error] = @timeoff.errors.full_messages
    #                           .join('<br />').html_safe
    # end

    respond_with @timeoff, location: [:company, :timeoffs]
  end

  # GET - edit_company_timeoff_path(id)
  #
  def edit
    authorize timeoff
    @user = current_user
    respond_with timeoff
  end

  def update
    authorize timeoff

    if timeoff.update_attributes(timeoff_params)
      timeoff.create_activity key: 'timeoff.update', owner: current_user
    else
      @user         = timeoff.user
      flash[:error] = timeoff.errors.full_messages.join('<br />').html_safe
    end

    respond_with timeoff, location: [:company, :timeoffs]
  end


  # PUT -
  #
  def change_status
    case params[:status]
    when 'decline'
      authorize timeoff
      timeoff.decline!(current_user)
      flash[:notice] = 'Request timeoff has been declined'
    when 'approve'
      authorize timeoff
      timeoff.approve!(current_user)
      flash[:notice] = 'Request timeoff has been approved'
    when 'cancel'
      authorize timeoff, :cancel?
      timeoff.cancel!(current_user)
      flash[:notice] = 'Request timeoff has been canceled'
    end

    respond_with [:company, timeoff]
  end


  # PUT -
  #
  def change_assignee
    authorize timeoff
    redirect_to company_timeoffs_path

    # to make sure the user_id is exists,
    # we need to check/find on database first
    #
    timeoff.assignee = current_company.users.find(params[:user_id])
    timeoff.save
  end

  protected

  def timeoff
    @timeoff ||= current_company.timeoffs
                                .manual_custom_entries
                                .find(params[:id])
  end

  def timeoff_params
    params.require(:timeoff).permit(:name, :start_at, :end_at, :timeoff_type,
                      :category_id, :author_id, :user_id, :memo,
                      attachments: []).tap do |whitelisted|
                        whitelisted[:amount] = params[:timeoff][:amount] if params[:timeoff][:timeoff_type] == 'custom'
                      end
  end
end
