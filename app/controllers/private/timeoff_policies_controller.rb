class Private::TimeoffPoliciesController < Private::ModuleController
  respond_to :html, :json

  # GET - company_timeoff_policies_path
  #
  def index
    @timeoff_policy = current_company.timeoff_policies.new
    authorize @timeoff_policy

    @timeoff_policies = current_company.timeoff_policies
                                        .includes(:users)
                                        .page((params[:page] || START_PAGE).to_i)
                                        .per((params[:per_page] || PER_PAGE).to_i)
  end


  # GET - company_timeoff_policy_path(policy_id)
  #
  def show
    authorize timeoff_policy
  end


  # POST - company_timeoff_policies_path
  #
  def create
    @timeoff_policy = current_company.timeoff_policies.new(new_timeoff_policy_params)
    authorize @timeoff_policy, :update?
    @timeoff_policy.save
    redirect_to company_timeoff_policy_setting_path(@timeoff_policy, :accrue_rate)
  end


  # DELETE - company_timeoff_policy_path(policy_id)
  #
  def destroy
    authorize timeoff_policy
    timeoff_policy.destroy
    redirect_to company_timeoff_policies_path
  end

  private

  def new_timeoff_policy_params
    params.require(:timeoff_policy).permit(:name)
  end

  def timeoff_policy
    @timeoff_policy ||= current_company.timeoff_policies.find(params[:id])
  end

end
