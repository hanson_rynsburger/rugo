class Private::Admin::ModuleController < Private::ModuleController
  before_action :authenticate_admin!

  protected

  def authenticate_admin!
    raise Pundit::NotAuthorizedError unless current_user.admin? || current_user.corp_admin?
  end
end