class Private::Admin::PlanAndBillingsController < Private::Admin::ModuleController
  respond_to :html, :json

  # GET - company_admin_plan_and_billings
  #
  def show
    prepare_data
  end


  # POST - company_admin_plan_and_billings_path
  #
  def create
    # authorize current_subscription
    if current_subscription.purchase!(params)
      flash[:notice] = 'Success change your plan'
      redirect_to company_admin_plan_and_billings_path
    else
      flash[:alert] = if current_subscription.result_errors.present?
                        current_subscription.result_errors
                      else
                        debugger
                        'Something wrong when we change your plan, please try again later'
                      end

      redirect_to :back
    end
  end


  # PUT
  #
  def change_plan
    authorize current_subscription

    begin
      if current_subscription.change_plan!(params)
        flash[:notice] = 'Success change your plan'
      else
        flash[:alert] = if current_subscription.result_errors.present?
                          current_subscription.result_errors
                        else
                          'Something wrong when we change your plan, please try again later'
                        end
      end

    rescue Exceptions::DowngradeMaxEmployeesReached => e
      flash[:alert] = e.message

    ensure
      redirect_to company_admin_plan_and_billings_path
    end
  end


  protected

  def prepare_data
    if current_company.subscription.uid.present?
      @braintree_subscription ||= current_company.subscription.braintree_subscription
    end

    @plan               ||= current_company.subscription.plan
    @braintree_customer ||= current_company.braintree_customer
    @credit_card        ||= @braintree_customer.credit_cards.first
    @billing_address    ||= @braintree_customer.addresses.first
  end

  def billing_address
    @billing_address ||= current_company.braintree_customer.addresses.first
  end

  def current_subscription
    @current_subscription ||= current_company.subscription
  end

  def billing_address_params
    params.require(:billing_address).permit(:company_name, :address, :address2, :city, :zipcode, :country, :state)
  end
end
