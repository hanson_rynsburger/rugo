class Private::Admin::EmbeddedFormsController < Private::Admin::ModuleController
  # GET - company_admin_embedded_forms_path
  #
  def index
    @user_forms = current_company.embedded_forms.where(klass: 'User')
  end

  # GET - new_company_admin_embedded_form_path
  #
  def new
    @embedded_form = current_company.embedded_forms.new(klass: params[:klass])
  end


  # POST - company_admin_embedded_forms_path
  #
  def create
    @embedded_form = current_company.embedded_forms.new(embedded_form_params)

    if @embedded_form.save
      flash[:notice] = "Success attach form '#{@embedded_form.form.name}' to model #{@embedded_form.klass}"
      redirect_to company_admin_embedded_forms_path
    else
      flash[:error] = @embedded_form.errors.full_messages
                                           .join('<br />')
                                           .html_safe
      render action: :new
    end
  end


  # DELETE - company_admin_embedded_form_path(id)
  #
  def destroy
    embedded_form.destroy
    flash[:notice] = "Success detect dyno-form"
    redirect_to :back
  end

  protected

  def embedded_form_params
    params.require(:embedded_form).permit(:klass, :form_id)
  end

  def embedded_form
    @embedded_form ||= EmbeddedForm.find(params[:id])
  end
end