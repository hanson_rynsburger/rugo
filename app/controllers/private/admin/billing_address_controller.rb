class Private::Admin::BillingAddressController < Private::Admin::ModuleController
  def update
    begin
      result = Braintree::Address.update(
        current_company.customer_uid, billing_address.id,
        billing_address_params
      )

      if result.success?
        flash[:notice] = "Success update your billing address"
      else
        flash[:alert]  = "Something wrong when update your address detail, please contact out customer support"
      end

    rescue Braintree::NotFoundError => e
      flash[:alert] = e.message

    ensure
      redirect_to company_admin_plan_and_billings_path
    end
  end

  protected

  def billing_address_params
    params.require(:billing_address).permit(:company, :street_address, :locality, :region,
      :country_code_alpha2, :postal_code, :extended_address)
  end

  def billing_address
    @billing_address ||= current_company.braintree_customer.addresses.first
  end
end