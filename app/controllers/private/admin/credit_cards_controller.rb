class Private::Admin::CreditCardsController < Private::Admin::ModuleController

  # POST - company_admin_credit_cards_path
  #
  def create
    current_company.subscription.change_payment_method(params)
    redirect_to company_admin_plan_and_billings_path
  end


  # DELETE - company_admin_credit_card_path(token)
  #
  def destroy
    begin
      result = Braintree::CreditCard.update(
        credit_card.token, credit_card_params
      )

      # TO DO

    rescue Braintree::NotFoundError => e
      flash[:alert] = e.message

    ensure
      redirect_to company_admin_plan_and_billings_path
    end
  end

  protected

  def credit_card_params
    params.require(:credit_card).permit(:cardholder_name, :expiration_month, :expiration_year)
  end

end