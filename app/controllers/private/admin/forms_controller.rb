class Private::Admin::FormsController < Private::Admin::ModuleController

  # GET - company_admin_forms_path
  #
  def index
    @forms = current_company.forms
  end

  # GET - new_company_admin_form_path
  #
  def new
    @form = current_company.forms.new
  end

  def create
    @form = current_company.forms.new(form_params)
    if @form.save
      flash[:notice] = 'Success create custom form'
      redirect_to company_admin_forms_path
    else
      flash[:error] = @form.errors.full_messages
                                  .join('<br/>')
                                  .html_safe

      render action: 'new'
    end
  end

  # GET - edit_company_admin_form_path(:id)
  #   id --> (expense, travel, timeoff)
  #
  def edit
    form
  end


  # GET -
  #
  def update
    if form.update_attributes(form_params)
      flash[:notice] = 'Success update custom form'
      redirect_to company_admin_forms_path
    else
      flash[:error] = form.errors.full_messages.join('<br />')
      render action: :edit
    end
  end


  # DELETE - company_admin_form_path(:id)
  #
  def destroy
    form.destroy
    flash[:notice] = 'Success remove form'
    redirect_to company_admin_forms_path
  end

  private

  def form_params
    params.require(:form).permit( :name, :introduction, :icon, :show_on_calendar,
                                  :require_sign, fields_attributes: [:id, :position, :name,
                                    :field_type, :required, :_destroy,
                                    properties: [
                                      :description, :true_label, :false_label, :unremoved,
                                      :label, :hint, :placeholder, :min_number, :max_rows,
                                      :currency,  :add_on, :from_number,  :to_number, :multiple,
                                      :max_rating, :format, :symbol, :max_number, :prompt,
                                      :tax_filling_status_prompt, :tax_decline_with_holding_prompt,
                                      :groups => [:name, :add_on, :add_on_value],
                                      :advance_groups => [
                                        :name, :add_on, :add_on_value, :type, :prompt, options: []
                                      ],
                                      personal_genders: [:label, :value], personal_marital_statuses: [:label, :value],
                                      compensation_currencies: [:label, :value], compensation_periods: [:label, :value],
                                      bank_account_types: [:label, :value],
                                      tax_filling_status_options: [:label, :value], tax_decline_with_holdings: [:label, :value],
                                      currencies: [:label, :value], payments: [:label, :value],
                                      categories: [:label, :value], purposes: [:label, :value],
                                      transportations: [:label, :value], classes: [:label, :value],
                                      select_options: [:label, :value]
                                    ]
                                  ],
                                  approvals_attributes: [:id, :role, :_destroy, :approver_id]
                                )
  end

  def form
    @form ||= current_company.forms.find params[:id]
  end

end