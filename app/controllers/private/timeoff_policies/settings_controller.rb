class Private::TimeoffPolicies::SettingsController < Private::ModuleController
  layout "timeoff_policy_setting"
  include Wicked::Wizard
  steps :accrue_rate, :tenture_workday, :timeoff_types

  def show
    authorize timeoff_policy
    render_wizard
  end

  def update
    authorize timeoff_policy

    case step
    when :tenture_workday
      timeoff_policy.work_hours = timeoff_policy_work_hours_params
    end

    timeoff_policy.update(timeoff_policy_params)
    render_wizard(timeoff_policy)
  end

  private

  def timeoff_policy
    @timeoff_policy ||= current_company.timeoff_policies.find(params[:timeoff_policy_id])
  end

  def timeoff_policy_params
    params.require(:timeoff_policy).permit(:name, :company_id, :accrue_type, :accrue_rate, :base_vacation_days,
      :allow_negative, :negative_cap, :carry_over, :carry_over_cap, :carry_over_expire_on, :total_sick_days,
      :total_personal_days, :work_hours_per_day, :beginning_of_workday, :end_of_workday, :work_week,
      {tentures_attribute: [:anniversary, :extra_days]}, :holidays, {categories_attributes: [:id, :_destroy, :name, :category_type]},
      {holidays: [:name, :date]}, :work_dates)
  end

  def timeoff_policy_work_hours_params
    work_hours = {}
    if params[:timeoff_policy][:work_hours].present?
      params[:timeoff_policy][:work_hours].each do |day, times_in_day|
        unless times_in_day[:checked].blank?
          start_at = DateTime.new(times_in_day[:start_at]["(1i)"].to_i,
                                          times_in_day[:start_at]["(2i)"].to_i,
                                          times_in_day[:start_at]["(3i)"].to_i,
                                          times_in_day[:start_at]["(4i)"].to_i,
                                          times_in_day[:start_at]["(5i)"].to_i).strftime("%H:%M")

          end_at = DateTime.new(times_in_day[:end_at]["(1i)"].to_i,
                                          times_in_day[:end_at]["(2i)"].to_i,
                                          times_in_day[:end_at]["(3i)"].to_i,
                                          times_in_day[:end_at]["(4i)"].to_i,
                                          times_in_day[:end_at]["(5i)"].to_i).strftime("%H:%M")
          work_hours[day] = [start_at, end_at]
        end
      end
    end
    params[:timeoff_policy].delete :work_hours

    work_hours
  end


  def finish_wizard_path
    company_timeoff_policies_path
  end

end