class Private::UsersController < Private::ModuleController
  before_action :set_policy, :set_company
  #after_action :verify_authorized, only: [:show, :edit]
  # Ignore it because of undefined method when update user
  respond_to :html, :json
  include ApplicationHelper

  def dashboard
  end

  def pricing
  end

  def index
    users = current_company.users.where.not(id: current_user.id).as_json(only: [:id, :email])

    respond_to do |format|
      format.json {
        render json: { users: users }
      }
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create new_params
    if @user.valid?
      redirect_to company_users_path(params[:company_id])
    else
      render "new"
    end
  end

  def show
    @user = User.find(params[:id])
    # authorize @user
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    # authorize @user
    if params[:company_id]
      e_params = edit_params

      if e_params[:handle_accouting] == nil
        e_params[:handle_accouting] = false
      end

      if e_params[:handle_travel] == nil
        e_params[:handle_travel] = false
      end

      result = @user.update_attributes(e_params)
      if result
        redirect_to company_users_path(params[:company_id])
      else
        render "edit"
      end
    else
      if @user.update_attributes(secure_params)
        redirect_to users_path, :notice => "User updated."
      else
        redirect_to users_path, :alert => "Unable to update user."
      end
    end
  end

  def destroy
    user = User.find(params[:id])
    # authorize user
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end

  def set_terminate
    users = User.where(id: params[:uid])

    users.update_all(:status => params[:status])

    respond_to do |format|
      format.json {
        render json: { status: 'OK'}
      }
    end
  end

  def set_location
    users = User.where(id: params[:uid])

    users.update_all(:location_id => params[:location_id])

    respond_to do |format|
      format.json {
        render json: { status: 'OK'}
      }
    end
  end

  def set_team
    users = User.where(id: params[:uid])

    users.update_all(:team_id => params[:team_id])

    respond_to do |format|
      format.json {
        render json: { status: 'OK' }
      }
    end
  end

  def import_file
    result = ImportEmployee.import(params[:file])
    respond_to do |format|
      format.json {
        render json: result
      }
    end
  end

  def download_template
    send_file Rails.root.join('private', 'Employee_Import_Template.xlsx'), :type=>"application/xlsx"
  end

  def save_employee_with_confirm
    users = params[:users].permit!
    users = users.values
    users.collect{|user|
      user['confirmed_at'] = Time.now
      user.delete("$$hashKey")
      user["compensation"] = user["compensation"].values
      user["emergency"]= user["emergency"].values
      user['password'] = "12345678"
      user['password_confirmation'] = "12345678"
      user
    }.to_a
    User.create! users

   render json: {message: I18n.t('save_confirm.confirm_successfully')}
  end
  private
  def set_policy
    @policy = policy(current_user)
  end

  def set_company
    @company = Company.find_by_id(params[:company_id])
  end

  def new_params
    new_params = params.require(:user).permit!
    #Dummy data for testing
    company = Company.find_by_id params[:company_id]

    team = company.teams.first

    if !team
      team = company.teams.create
    end
    new_params[:company_id] = company.id
    #######
    new_params[:emergency].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    new_params[:bank].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    new_params[:compensation].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    new_params
  end

  def edit_params
    edit_params = params.require(:user).permit!
    edit_params[:emergency].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    edit_params[:bank].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    edit_params[:compensation].delete_if{|x| x.values.delete_if{|x| x.blank?}.compact.empty?}
    edit_params
  end

  def secure_params
    params.require(:user).permit(:role)
  end

  def get_user_list(company)
    users = company.users
    users = users.where(status: 0)

    if params[:filter_employee].present?
      users = users.search_employee(params[:filter_employee])
    end

    if params[:sort_by].present?
      if params[:sort_by] == 'name'
        users = users.order(name: :asc)
      elsif params[:sort_by] == 'team'
        if params[:filter_employee].present?
          users = users.order("teams.name asc")
        else
          users = users.joins("LEFT JOIN teams ON teams.id = users.id").order("teams.name asc")
        end
      else
        if params[:filter_employee].present?
          users = users.order("locations.name asc")
        else
          users = users.joins("LEFT JOIN locations ON locations.id = users.id").order("locations.name asc")
        end
      end
    else
      users = users.order(created_at: :desc)
    end

    users.page((params[:page] || START_PAGE).to_i).per((params[:per_page] || PER_PAGE).to_i)
  end

end
