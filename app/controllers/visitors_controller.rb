class VisitorsController < ApplicationController
  respond_to :html

  def index
    @company = Company.new
  end

  def tour
  end

  def pricing
  end
  def terms
  end
  def privacy
  end
  def static_tasks_list
    render layout: 'static'
  end

  def static_show_expense_detail
    render layout: 'static'
  end

  def static_show_travel_detail
    render layout: 'static'
  end

  def static_show_timeoff_detail
    render layout: 'static'
  end

end
