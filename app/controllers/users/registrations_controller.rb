class Users::RegistrationsController < Devise::RegistrationsController
  layout 'visitors', except: [:edit, :update, :change_email, :change_password]
  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy, :change_email, :change_password]

  def edit
    @policy     = policy(current_user)
    user_dyno_forms
  end

  def change_email
    @policy     = policy(current_user)
    user_dyno_forms
  end

  def change_password
    @policy     = policy(current_user)
    user_dyno_forms
  end

  # PUT /resource
  # We need to use a copy of the resource because we don't want to change
  # the current user in place.
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if resource.update(update_params)
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end

      resource.confirm!
      resource.confirmation_token = nil
      if resource.save
        sign_in :user, resource, bypass: true
        respond_with resource, location: root_url(subdomain: resource.company.url)
      else
        flash[:error] = resource.errors.full_messages.join('<br />').html_safe
        render '/confirmations/show'
      end
    else
      clean_up_passwords resource
      render '/confirmations/show'
    end
  end

  protected

  def company
    @company ||= Company.find_by(token: params[:token])
  end

  def authorize_company_token!
    if params[:token].blank?
      flash[:error] = 'You need to sign up your company first'
      return redirect_to new_company_company_path
    end
  end

  def update_params
    params.require(:user).permit(:password, :password_confirmation,
                                  :timezone, :join_on, :location_id, :team_id,
                                  :first_name, :last_name, :manager1_id, :manager2_id,
                                  company_attributes: [:id, :url]).tap do |whitelisted|
                                    if params[:user][:properties].present?
                                      whitelisted[:properties] = params[:user][:properties]
                                    end
                                  end
  end

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, personal: [:first_name, :last_name])
  end
end