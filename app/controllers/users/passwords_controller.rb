class Users::PasswordsController < Devise::PasswordsController
  layout 'visitors'

  # POST - user_password_path
  def create
    self.resource = company.users.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
    else
      respond_with(resource)
    end
  end

  protected

  def company
    @company ||= Company.find_by(url: request.subdomain)
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    root_url(subdomain: nil)
  end
end