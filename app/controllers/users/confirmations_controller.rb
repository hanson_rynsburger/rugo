class Users::ConfirmationsController < Devise::ConfirmationsController
  layout 'visitors', only: [:show]

  def show
    sign_out
    token = Devise.token_generator.digest(User, :confirmation_token, params[:confirmation_token])
    self.resource = resource_class.find_by(confirmation_token: token)

    if resource.present? && resource.valid?
      resource.confirmed_at = Time.zone.now
      resource.save!
      sign_in(resource)

    else
      redirect_to new_user_confirmation_path, error: I18n.t('confirmations.show.token_invalid')
    end
  end

end