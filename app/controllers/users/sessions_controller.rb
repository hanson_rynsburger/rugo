class Users::SessionsController < Devise::SessionsController
  layout 'visitors'

  def company
    sign_out :user
  end

  # POST - users_company_verify_path
  #
  def company_verify
    if company.present?
      redirect_to new_user_session_url(subdomain: company.url)
    else
      flash[:error] = 'Your company not found'
      redirect_to :back
    end
  end

  def new
    return redirect_to users_company_url(subdomain: nil) if company.blank?
    @subdomain = request.subdomain
    super
  end

  # DELETE /resource/sign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    redirect_to root_url(subdomain: nil)
  end

  protected

  def after_sign_in_path_for(resource)
    dashboard_company_employees_url(subdomain: request.subdomain)
  end

  def company
    @company ||=  if request.subdomain.present?
                    Company.find_by(url: request.subdomain)
                  elsif params[:subdomain].present?
                    Company.find_by(url: params[:subdomain])
                  end
  end

end