class Api::CurrencyController < Api::ModuleController

  # GET -
  #
  def show
    from = params[:from]
    to   = params[:to]

    result =  RestClient.get("http://currency-api.appspot.com/api/#{from}/#{to}.json", params: {
                key: Rails.application.secrets.currency_api_key
              })

    respond_with JSON.parse(result)
  end

end