class ErrorsController < ApplicationController
  layout 'error'

  # GET - /404
  #
  def not_found
  end

  # GET - /500
  #
  def internal_server_error
  end

end