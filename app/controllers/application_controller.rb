class ApplicationController < ActionController::Base
  include Concerns::ErrorPage
  include PublicActivity::StoreController
  include Pundit

  protect_from_forgery
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :print_request

  def current_company
    @current_company ||= Company.find_by(url: request.subdomain)
  end
  helper_method :current_company

  def current_plan
    @current_plan ||= current_company.subscription.plan
  end
  helper_method :current_plan

  def user_dyno_forms
    @user_dyno_forms ||= EmbeddedForm.dyno_forms(current_company.id, 'User')
  end

  protected

  def print_request
    if Rails.env.development?
      ap params
      ap "Use Tenant: #{current_company.use_tenant}" if user_signed_in? && current_company.present?
      ap "Tenant: #{Apartment::Tenant.current}"
      ap "Subdomain: #{request.subdomain}"
      ap "==========================================="
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) << [:email, :password, :subdomain]
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    root_url(subdomain: false)
  end

  # Generates not authorized exception message
  def user_not_authorized
    Rails.logger.error I18n.t('authorized.not_authorized')
    respond_to do |format|
      format.html {
        flash.now[:error] = I18n.t('authorized.not_authorized')
        redirect_to (request.referrer || root_path)
      }
      format.json {
        render json: { success: false, messages: I18n.t('authorized.not_authorized') }
      }
    end
  end
end
