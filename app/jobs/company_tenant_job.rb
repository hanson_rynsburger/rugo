class CompanyTenantJob
  include SuckerPunch::Job

  def perform(company_id)
    company = Company.find(company_id)

    unless  company.data_generated
      # now it's time to generate default data for this company
      # - default timeoff policy
      # - default dyno-forms (and will be attached into model User)
      #
      if company.use_tenant
        Apartment::Tenant.switch!(company.url)
      else
        Apartment::Tenant.switch!
      end

      # generate data
      company.generate_default_data

      # switch to default again
      Apartment::Tenant.switch!
    end

  end
end