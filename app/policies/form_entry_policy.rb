class FormEntryPolicy < GeneralPolicy
  attr_reader :user, :form_entry

  def initialize(user, model)
    @user       = user
    @form_entry = model
  end

  def approve?
    (form_entry.approver == user) && form_entry.status_pending?
  end

  def cancel?
    (form_entry.user == user || form.approvers.include?(user)) && form_entry.status_pending?
  end

  private

  def form
    @form ||= form_entry.form
  end

end