class UserPolicy
  attr_reader :current_user, :model
  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  def index?
    @current_user.admin?
  end

  def show?
    @current_user.admin? or @current_user == @user
  end

  def update?
    @current_user.admin?
  end

  def destroy?
    return false if @current_user == @user
    @current_user.admin?
  end

  def permitted_attributes
    if @current_user.admin?
      [:email, :encrypted_password, :name, :role, :role, :avatar, :handle_travel, :handle_accouting, :status, :employment_type, :personal, :personal, { :bank => [:name, :account_type, :id_number, :account_number] }, { :tax => [:filing_status, :decline_withholding, :withholding_allowance, :additional_withholding] }, :manager_id, { :employment => [:work_email, :office_phone_number, :start_date] }, { :compensation => [:amount, :currency, :per, :effective, :reason] }, :timezone, :company_id, :team_id, :location_id, :backup_id, :policy_id]
    else
      [:email, :encrypted_password, :name, :role, :role, :avatar, :handle_travel, :handle_accouting, :status, :personal, :personal, { :bank => [:name, :account_type, :id_number, :account_number] }, { :tax => [:filing_status, :decline_withholding, :withholding_allowance, :additional_withholding] }, { :employment => [:work_email, :office_phone_number, :start_date] }, { :compensation => [:amount, :currency, :per, :effective, :reason] }, :timezone, :company_id, :backup_id, :policy_id]
    end
  end
end
