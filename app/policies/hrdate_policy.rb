class HrdatePolicy < GeneralPolicy
  attr_reader :user, :hrdate

  def initialize(user, model)
    @user = user
    @hrdate = model
  end


  def new?
    check_permission
  end

  def create?
    check_permission
  end

  def edit?
    check_permission
  end

  def update?
    check_permission
  end

  def destroy?
    check_permission
  end

  private

  def check_permission
    user.admin?
  end
end
