class TimeoffRequestPolicy < GeneralPolicy
  attr_reader :user, :timeoff

  def initialize(user, model)
    @user = user
    @timeoff = model
  end

  def change_status?
    timeoff.status_pending? && ((user == timeoff.assignee) || (timeoff.assignee.blank? && timeoff.user == user))
  end

  def new?
    case timeoff.timeoff_type
    when 'auto'
      timeoff.user.blank? && timeoff.author.blank? && timeoff.assignee.blank?
    when 'manual'
      timeoff.user.policy.present?
    when 'custom'
      timeoff.user.policy.present? && (user.admin? || user.corp_admin?)
    end

  end

  def cancel?
    (timeoff.status_pending? || timeoff.status_approved?) && ((timeoff.assignee == user) || (timeoff.user == user))
  end

  def change_assignee?
    timeoff.status_pending? && ((timeoff.assignee == user) || (timeoff.assignee.blank? && timeoff.user == user))
  end

  def update?
    new? && timeoff.status_pending? && (timeoff.user.company == user.company) && ((user == timeoff.assignee) || (timeoff.user == user))
  end
end
