class AdminOnlyPolicy
  attr_reader :user, :record

  def initialize(user, record)
    raise Pundit::NotAuthorizedError, "must be logged in" unless user
    @user   = user
    @record = record
  end

  def index?
    user.admin? or user.corp_admin?
  end

  def show?
    scope.where(:id => record.id).exists? and (user.admin? or user.corp_admin?)
  end

  def create?
    user.admin? or user.corp_admin?
  end

  def new?
    create?
  end

  def update?
    user.admin? or user.corp_admin?
  end

  def edit?
    update?
  end

  def destroy?
    user.admin? or user.corp_admin?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end


  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    #@note @user = policy_scope(User)
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(:company => user.company)
      end
    end
  end
end