class TaskPolicy < GeneralPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
    @current_user = current_user
    @task = model
  end

  def approve?
    if @task.task_parent_id.present?
      if @task.task_type == Task.task_types[:expense]
        @current_user == @task.parent_task.assignee #&& @current_user.handle_accounting
      else
        @current_user == @task.parent_task.assignee #&& @current_user.handle_booking
      end
    else
      @current_user == @task.assignee
    end
  end

	def update?
		@current_user == @task.author || @current_user == @task.user
  end

  def show?
    scope.where(:id => record.id).exists? and (user.admin? or user.corp_admin? or user == record.user or user == record.author or user == record.assignee)
  end
end
