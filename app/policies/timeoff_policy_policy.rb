class TimeoffPolicyPolicy
  attr_reader :current_user, :model
  def initialize(current_user, model)
    @current_user = current_user
    @timeoff_policy = model
  end

  def index?
    @current_user.admin? or @current_user.corp_admin?
  end

  def show?
    checkPermission
  end

  def edit?
    checkPermission
  end

  def update?
    checkPermission
  end

  def destroy?
    checkPermission
  end

  private
  def checkPermission
    @current_user.admin? or (@current_user.corp_admin? && (@current_user.company == @timeoff_policy.company))
  end
end

