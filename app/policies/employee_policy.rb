class EmployeePolicy < AdminOnlyPolicy

  def show?
    has_self_permissions_access_employee
  end

  def update?
    has_self_permissions_access_employee
  end

  def bulk_terminate?
    create?
  end

  def bulk_location?
    create?
  end

  def bulk_team?
    create?
  end

  def import_file?
    create?
  end

  def save_employee_with_confirm?
    create?
  end

  def has_self_permissions_access_employee
    @user.admin? or @user.corp_admin? or @user == @user
  end

  def permitted_attributes
    if @user.admin?
      [:email, :encrypted_password, :name, :role, :role, :avatar, :handle_travel, :handle_accouting, :status, :employment_type, :personal, :personal, { :bank => [:name, :account_type, :id_number, :account_number] }, { :tax => [:filing_status, :decline_withholding, :withholding_allowance, :additional_withholding] }, :manager_id, { :employment => [:work_email, :office_phone_number, :start_date] }, { :compensation => [:amount, :currency, :per, :effective, :reason] }, :timezone, :company_id, :team_id, :location_id, :backup_id, :policy_id]
    else
      [:email, :encrypted_password, :name, :role, :role, :avatar, :handle_travel, :handle_accouting, :status, :personal, :personal, { :bank => [:name, :account_type, :id_number, :account_number] }, { :tax => [:filing_status, :decline_withholding, :withholding_allowance, :additional_withholding] }, { :employment => [:work_email, :office_phone_number, :start_date] }, { :compensation => [:amount, :currency, :per, :effective, :reason] }, :timezone, :company_id, :backup_id, :policy_id]
    end
  end
end
