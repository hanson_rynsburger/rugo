class SubscriptionPolicy < GeneralPolicy
  attr_reader :user, :subscription

  def initialize(user, model)
    @user         = user
    @subscription = model
  end

  def update?
    user.corp_admin? && subscription.uid.blank?
  end

  def change_plan?
    user.corp_admin? && subscription.uid.present?
  end

end