module FormEntriesHelper
  def travel_transportation_icon(row)
    "icon-#{row['transportation'].downcase}"
  end

  def form_entry_properties_hash(question, property_name)
    Hash.new.tap do |data|
      question.properties[property_name].each do |hash|
        data[hash['value']] = hash['label']
      end
    end
  end

  def form_entry_properties_selected_text(question, property_name, value)
    form_entry_properties_hash(question, property_name)[value]
  end
end
