module ApplicationHelper
  def call_js_init(something_js)
    content_for :javascript do
      "<script type='text/javascript'>
          $(document).ready(function(){
            #{something_js}
          });
      </script>".html_safe
    end
  end

  def bootstrap_class_for(flash_type)
    case flash_type.to_sym
      when :success
        "alert-success" # Green
      when :error
        "alert-danger" # Red
      when :alert
        "alert-warning" # Yellow
      when :notice
        "alert-info" # Blue
      else
        flash_type.to_s
    end
  end

  def list_title_of_timeoff
    current_user.policy ? current_user.policy.timeoff_types.map{|t| t.with_indifferent_access[:name]} : DEFAULT_TIMEOFF_TYPES
  end

  # Return collection of rating field type options
  #
  def build_rating_options(form_field)
    max_rating = form_field.properties['max_rating'].to_i
    symbol     = form_field.properties['symbol']

    Array.new.tap do |arr|
      (1..max_rating).to_a.each do |num|
        arr <<  if (symbol == 'number')
                  [num.to_s, num]
                else
                  [(content_tag(:i, '', class: "fa #{symbol}") * num).html_safe, num]
                end
      end
    end
  end


  # return class for group's column base on how much column is it
  # Min Columns  1
  # Max Columns  5
  #
  def build_class_question_group_column(form_field)
    return nil if form_field.field_type != 'question_group'
    return nil if form_field.properties.blank?

    case form_field.properties['groups'].length
    when 1
      "col-xs-10"
    when 2
      "col-xs-5"
    when 3
      "col-xs-3"
    when 4
      "col-xs-2"
    when 5
      "col-xs-2"
    else
      nil
    end
  end


  # return class for group's column base on how much column is it
  # Min Columns  1
  # Max Columns  5
  #
  def build_class_advance_question_group_column(form_field)
    return nil if form_field.field_type != 'advance_group'
    return nil if form_field.properties.blank?

    case form_field.properties['advance_groups'].length
    when 1
      "col-xs-10"
    when 2
      "col-xs-5"
    when 3
      "col-xs-3"
    when 4
      "col-xs-2"
    when 5
      "col-xs-2"
    else
      nil
    end
  end

  def credit_card_logo(credit_card)
    case credit_card.card_type
    when 'Visa'
      'gfx-cc-visa.png'
    when 'MasterCard'
      'gfx-cc-mastercard.png'
    when 'American Express'
      'gfx-cc-amex.png'
    when 'Maestro'
      'gfx-cc-maestro.gif'
    when 'Discover'
      'gfx-cc-discover.gif'
    when 'JCB'
      'gfx-cc-jcb.gif'
    end
  end


  # will return client token string from braintree
  #
  def braintree_client_token(company)
    @braintree_client_token ||= Braintree::ClientToken.generate(customer_id: company.customer_uid)
  end
end
