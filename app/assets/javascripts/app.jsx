var React = require('react');
var Reflux = require('reflux');
var Activities = require('./components/activities.jsx');
var FormHeader = require('./components/form-header.jsx');
var AlertBlock = require('./components/alert-block.jsx');
var FormStore = require('./stores/form-store.js');
var FormActions = require("./actions/form-action.js");
var EmployeeActions = require("./actions/employee-action.js");
var EmployeeStore = require("./stores/employee-store.js");
var ValidationStore = require('./stores/validation-store.js');
var ValidationActions = require("./actions/validation-action.js");
var allFields = {
	'expense': require('./components/expense-field.jsx'),
	'travel': require('./components/travel-field.jsx'),
	'date_range':require('./components/date-range-field.jsx'),
	'single_line': require('./components/single-line-field.jsx'),
	'boolean': require('./components/boolean-field.jsx'),
	'textarea': require('./components/textarea-field.jsx'),
	'rating': require('./components/rating-field.jsx'),
	'number': require('./components/number-field.jsx'),
	'checkbox': require('./components/checkbox-field.jsx'),
	'dropdown': require('./components/dropdown-field.jsx'),
	'date': require('./components/date-field.jsx')
};

var http = require('http');
window.http = http;

//register superagent for other libs
var request = require('superagent');
window.request = request;

let App = React.createClass({
	mixins: [Reflux.listenTo(FormStore, 'onDataChange'), Reflux.listenTo(EmployeeStore, 'onEmployeeLoad')],
	getInitialState: function(){
		return {
			employees: []
		};
	},
	onEmployeeLoad: function(){
		var employees = EmployeeStore.getData();
		var userArray = {'': 'Select'};
		_.each(employees.employees, function(emp){
			userArray[emp.id] = emp.name;
		})
		this.setState({employees: userArray});
	},
	onDataChange: function(){
		this.setState({
			data: FormStore.getData()
		});
		ValidationActions.init();
	},
	formSave: function(e){
		e.preventDefault();
		ValidationActions.validate(true);
	},
	componentDidMount: function(){
		FormActions.init();
		EmployeeActions.get();
	},
	render: function(){
		var data = this.state.data;
		console.log(data);
		if (typeof data === 'undefined'){
			return (<h1>Loading...</h1>);
		}

		var forms = [];
		for (var i in data.form.fields){
			var id = data.form.fields[i].id;
			var field_type = data.form.fields[i].field_type;

			forms.push(
				React.createElement(allFields[field_type],{
					fields: data.form.fields[i],
					value: data.answers[id],
					id: data.form.fields[i].id
				})
			);
		}
		return (
			<div>
				<AlertBlock />
				<FormHeader data={data} employees={this.state.employees} />
				{forms}
				<div>
					<button className="btn btn-success" onClick={this.formSave}>Update</button>
				</div>
				<Activities />
			</div>
		);
	}
});

export default App;