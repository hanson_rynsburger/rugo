// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//= require i18n
//= require i18n/translations
//= require jquery
//= require jquery-ui/sortable
//= require jquery_ujs
//= require jquery_nested_form
//= require easyhr/config/constants
//= require turbolinks
//= require components
//= require moment
//= require lodash
//= require js-routes
//= require seiyria-bootstrap-slider
//= require bootstrap/dropdown
//= require bootstrap/alert
//= require bootstrap/transition
//= require bootstrap/tab
//= require select2/select2.full
//= require fontawesome-iconpicker
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require eonasdan-bootstrap-datetimepicker
//

// Angular Stuff
//-----------------------
//= require easy_hr_backend



// Custom Stuff
//--------------------------
//= require timeoff_policies

//= require_tree ../templates
//= require_self
//


I18n.missingTranslationPrefix = 'EE: ';

function stickyHeader() {
  var stickyHeaderPos = $(".sticky-header").offset().top + $(".sticky-header").height();
  var scrollPos = 0;

  $(window).on('scroll', function(){
    scrollPos = $(window).scrollTop();

    if(scrollPos > stickyHeaderPos){
      $(".sticky-header").addClass("affix");
    }else{
      $(".sticky-header").removeClass("affix");
    }
  })
}

$(document).on('ready resize page:load', function(){
  if($(window).width() <= 768){
    $(".wrapper").addClass("sidebar-min");
  }else{
    $(".wrapper").removeClass("sidebar-min");
  }
  $(document).on("click", function(e){
    if(!$(e.target).hasClass('dropdown-toggle')){
      var dropdown = $('.dropdown');
      if(dropdown.hasClass('open')){
        dropdown.removeClass('open')
      }
    }
  })
  $(".sidebar-toggle").on("click", function(e){
    e.stopPropagation();
    $(".wrapper").toggleClass("sidebar-min");
  });
  $(".dropdown-toggle").on('click',function(e){
    e.preventDefault();
    e.stopPropagation();
    var dropdownOpen = $(".dropdown.open");

    dropdownOpen.removeClass('open');

    $(this).parent().toggleClass("open");
  })
  $("[data-target=#ampm]").on('click', function(e){
    e.stopPropagation();
    $(".calendar-to").toggle('fast');
    var targetId = $(this).data('target');
    $(targetId).toggle('fast');
    console.log(targetId)
  });
  $('[data-toggle]').on('click', function(){
    $(this).toggleClass('opened');
    var targetId = $(this).data('target');
    $(targetId).addClass("in");
  });
  $('[data-close=collapse]').on('click', function(){
    var targetId = $(this).data('target');q
    $(targetId).removeClass("in");
    $('[data-target='+targetId+']').removeClass("opened");
  });
  Turbolinks.enableProgressBar();
});
