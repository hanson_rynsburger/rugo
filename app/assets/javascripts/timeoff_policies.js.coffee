window.TimeoffPolicy ||=  {}


TimeoffPolicy.initIndexPage = ()->
  $form_container = $('#form-policy-container')

  $('#show-form-policy').on 'click', ()->
    $form_container.show(0)

  $('#hide-form-policy').on 'click', ()->
    $form_container.hide(0)