var React = require('react');
var Reflux = require('reflux');
var FormHeaderNew = require('./components/form-header-new.jsx');
var AlertBlock = require('./components/alert-block.jsx');
var FormListing = require('./components/form-listing.jsx');
var TaskList = require('./components/tasklist.jsx');
var EmployeeActions = require("./actions/employee-action.js");
var EmployeeStore = require("./stores/employee-store.js");

let ManageTasks = React.createClass({
	mixins: [Reflux.listenTo(EmployeeStore, 'onEmployeeLoad')],
	getInitialState: function(){
		return {
			new_form_id: -1,
			employees: []
		};
	},
	onEmployeeLoad: function(){
		var employees = EmployeeStore.getData();
		var userArray = {};
		_.each(employees.employees, function(emp){
			userArray[emp.id] = emp.name;
		})
		this.setState({employees: userArray});
	},
	onCreateTask: function(form_index){
		this.setState({new_form_id: form_index});
	},
	onCancelTask: function(){
		this.setState({new_form_id: -1});
	},
	componentDidMount: function(){
		EmployeeActions.get();
	},
	render: function(){
		return (
			<div className="pages tasks">
			  <AlertBlock />

			  <div className="page-title">
			    <h1 className="pull-left">Manage Tasks</h1>
			    <FormListing forms={this.props.forms} onCreate={this.onCreateTask} />
			  </div>
			  {
			  	this.state.new_form_id !== -1?<FormHeaderNew {...this.props.forms[this.state.new_form_id]} onCancel={this.onCancelTask} employees={this.state.employees} />:null
			  }
			  <TaskList tasks={this.props.tasks} employees={this.state.employees} />
			</div>
		);
	}
});

export default ManageTasks;