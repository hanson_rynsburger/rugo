"use_strict"
app.factory 'ComboboxService', [
  () ->
    service = {}
    service.updateValueByArray = (mainObject, array, objectDir1, objectDir2, arrayRec, multipleValue) ->
      i = 0

      while i < array.length
        if objectDir1 && !objectDir2
          mainObject[objectDir1] = array[i] if mainObject[objectDir1] == array[i].value
        else if multipleValue
          $scope.temp.push(array[i]) if $.inArray(array[i].value.toString() , mainObject[objectDir1][objectDir2]) != -1

        else if objectDir1 && objectDir2 && !arrayRec
          mainObject[objectDir1][objectDir2] = array[i] if parseInt(mainObject[objectDir1][objectDir2]) == parseInt(array[i].value)
        else if objectDir1 && objectDir2 && arrayRec
          j = 0
          while j < mainObject[objectDir1].length
            mainObject[objectDir1][j][objectDir2] = array[i] if (parseInt(mainObject[objectDir1][j][objectDir2]) == parseInt(array[i].value)) || (mainObject[objectDir1][j][objectDir2] == array[i].value)
            j++
        i++
      if multipleValue
        mainObject[objectDir1][objectDir2] = $scope.temp
    service
]