"use_strict"
app.factory 'TaskService', ["$http", ($http) ->
  service = {}

  service.init = ($scope) ->
    service.scope = $scope
    return service

  service.getListTask = ($scope, params, $defer) ->

    search_params =
      page: params.page()
      per_page: params.count()
      sort_by: $scope.sortBy
      type: $scope.defaultType
      format: 'json'

    $http(
        method: 'get'
        url: Routes.company_form_entries_path search_params
      ).success (response) ->
        params.total response.total_count
        $defer.resolve response.tasks

        $scope.countTimeoffTask   = response.count.TimeoffTask
        $scope.countTravelTask    = response.count.TravelTask
        $scope.countExpenseTask   = response.count.ExpenseTask
        $scope.totalCount         = response.total_count

  service.getListChildTask = ($scope, params, $defer) ->

    search_params =
      page: params.page()
      per_page: params.count()
      sort_by: $scope.sortBy
      id: $scope.taskID
      format: 'json'

    $http(
        method: 'get'
        url: Routes.get_list_children_company_form_entries_path search_params
      ).success (response) ->
        params.total response.total_count
        $defer.resolve response.child_tasks
  service
]