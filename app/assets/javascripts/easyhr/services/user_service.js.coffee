"use_strict"
app.factory 'UserService', [
  "$http"
  "ngTableParams"
  ($http, ngTableParams) ->
    service = {}

    service.init = ($scope) ->
      service.scope = $scope
      return service

    service.getListUser = ($scope, params, $defer) ->
      search_params =
        page: params.page()
        per_page: params.count()
        filter_employee: $scope.searchEmployee
        sort_by: $scope.sortBy
        format: 'json'

      $http(
          method: 'get'
          url: Routes.company_employees_path search_params
        ).success (response) ->
          params.total response.total_count
          $defer.resolve response.employees
          $scope.command.listCheckAll = false

    service
]