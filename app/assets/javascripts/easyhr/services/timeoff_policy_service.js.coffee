app.factory 'PolicyService', [
  "$http"
  "ngTableParams"
  ($http, ngTableParams) ->
    service = {}

    service.init = ($scope) ->
      service.scope = $scope
      return service

    service.getListPolicy = ($scope, params, $defer) ->
      search_params =
        page: params.page()
        per_page: params.count()
        format: 'json'
      $http(
          method: 'get'
          url: Routes.company_timeoff_policies_path
          data: search_params
        ).success (response) ->
          params.total response.total_policy
          $defer.resolve = response.policies

    service
]