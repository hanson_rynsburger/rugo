app = angular.module("easyhrApp")
app.filter 'exrate', ->
  return (t)->
    return parseFloat(t).toPrecision(5)

app.filter 'twodigit', ->
  return (t)->
    return parseFloat(t).toFixed(2)
