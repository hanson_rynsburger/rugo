@START_PAGE = 1
@PER_PAGE = 10
@PAGINATE_ARRAY = [10, 20, 30, 40, 50]
@HOURS_PER_DAY = 8
@STATUS = [
  {
    value: "active"
    label: I18n.t('js.constants.status.active')
  }
  {
    value: "terminated"
    label: I18n.t('js.constants.status.terminated')
  }
]

@ICONS = [
  {
    value: "full_time"
    label: "<i class=\"fa fa-gear\"></i> " + I18n.t('js.constants.icons.full_time')
  }
  {
    value: "part_time"
    label: "<i class=\"fa fa-globe\"></i> " + I18n.t('js.constants.icons.part_time')
  }
  {
    value: "contractor"
    label: "<i class=\"fa fa-heart\"></i> " + I18n.t('js.constants.icons.contractor')
  }
]

@GENDER = [
  {
    value: 'male'
    label: I18n.t('js.constants.gender.male')
  }
  {
    value: 'female'
    label: I18n.t('js.constants.gender.female')
  }
]

@MARITAL_STATUS = [
  {
    value: 'single'
    label: I18n.t('js.constants.marital_status.single')
  }
  {
    value: 'married'
    label: I18n.t('js.constants.marital_status.married')
  }
  {
    value: 'divoced'
    label: I18n.t('js.constants.marital_status.divoced')
  }
  {
    value: 'widowed'
    label: I18n.t('js.constants.marital_status.widowed')
  }
]

@ACCOUNT_TYPES = [
  {
    value: 'checking'
    label: I18n.t('js.constants.account_types.checking')
  }
  {
    value: 'savings'
    label: I18n.t('js.constants.account_types.savings')
  }
]

@TAX_STATUS = [
  {
    value: 'individual'
    label: I18n.t('js.constants.tax_status.individual')
  }
  {
    value: 'joint'
    label: I18n.t('js.constants.tax_status.joint')
  }
]

@BOOLEAN = [
  {
    value: 'yes'
    label: I18n.t('js.constants.boolean.yes')
  }
  {
    value: 'no'
    label: I18n.t('js.constants.boolean.no')
  }
]

@PERIODS = [
  {
    value: 'year'
    label: I18n.t('js.constants.periods.year')
  }
  {
    value: 'quarter'
    label: I18n.t('js.constants.periods.quarter')
  }
  {
    value: 'month'
    label: I18n.t('js.constants.periods.month')
  }
]

@CURRENCIES = [
  {
    value: 'usd'
    label: I18n.t('js.constants.currencies.usd')
  }
  {
    value: 'euro'
    label: I18n.t('js.constants.currencies.euro')
  }
  {
    value: 'cny'
    label: I18n.t('js.constants.currencies.cny')
  }
]

@TERMINATE_STATUS = 1

@TIMEOFF_POLICY_TYPES = $.map(['accrue_by_time', 'accrue_by_time_adv'], (item) ->
  {
    value: 'accrue_by_time',
    label: I18n.t("js.constants.timeoff_polity_types.#{item}")
  }
)

@TIMEOFF_POLICY_RATES = $.map(["weekly", "bi_weekly", "monthly", "yearly"], (item) ->
  {
    value: item,
    label: I18n.t("js.constants.timeoff_polity_rates.#{item}")
  }
  )
@TIMEOFF_POLICY_ANNIVERSARIES = $.map([1, 2, 3, 4, 5, 6, 7], (item) ->
  {
    value: item,
    label: I18n.t("js.constants.anniversaries", {count: item})
  }
  )

@TIMEOFF_POLICY_PTOS = [
  {
    value: 'pto',
    label: I18n.t('js.constants.timeoff_policy_pto.pto')
  },
  {
    value: 'sick',
    label: I18n.t('js.constants.timeoff_policy_pto.sick')
  },
  {
    value: 'other',
    label: I18n.t('js.constants.timeoff_policy_pto.other')
  }
]

@TASKS_TYPE = ['All', 'TimeoffTask', 'TravelTask', 'ExpenseTask']

@SUB_TYPE_TASK = [
  {
    value: 'vacation'
    label: I18n.t('js.constants.sub_type_task.vacation')
  }
  {
    value: 'sick'
    label: I18n.t('js.constants.sub_type_task.sick')
  }
  {
    value: 'others'
    label: I18n.t('js.constants.sub_type_task.others')
  }
]

@FILTER_EXPENSE_TASK = [
  {
    value: 'all'
    label: I18n.t('js.constants.filter_expense_task.all')
  }
  {
    value: 'paid'
    label: I18n.t('js.constants.filter_expense_task.paid')
  }
  {
    value: 'not_paid'
    label: I18n.t('js.constants.filter_expense_task.not_paid')
  }
]

@EXPENSE_TYPE = [
  {
    value: 'accomodation'
    label: I18n.t('js.constants.expense_type.accomodation')
  }
  {
    value: 'air_ticket'
    label: I18n.t('js.constants.expense_type.air_ticket')
  }
  {
    value: 'car_rental'
    label: I18n.t('js.constants.expense_type.car_rental')
  }
  {
    value: 'ent_employee'
    label: I18n.t('js.constants.expense_type.ent_employee')
  }
  {
    value: 'ent_issuer_banker'
    label: I18n.t('js.constants.expense_type.ent_issuer_banker')
  }
  {
    value: 'ent_clients'
    label: I18n.t('js.constants.expense_type.ent_clients')
  }
  {
    value: 'late_night_taxi'
    label: I18n.t('js.constants.expense_type.late_night_taxi')
  }
  {
    value: 'meals'
    label: I18n.t('js.constants.expense_type.meals')
  }
  {
    value: 'misc_office_expense'
    label: I18n.t('js.constants.expense_type.misc_office_expense')
  }
  {
    value: 'other'
    label: I18n.t('js.constants.expense_type.other')
  }
  {
    value: 'overtime_meal'
    label: I18n.t('js.constants.expense_type.overtime_meal')
  }
  {
    value: 'tel_mobile_internet'
    label: I18n.t('js.constants.expense_type.tel_mobile_internet')
  }
  {
    value: 'transportion'
    label: I18n.t('js.constants.expense_type.transportion')
  }
]

@PURPOSE = [
  {
    value: 'ratings_meeting'
    label: I18n.t('js.constants.purpose.ratings_meeting')
  }
  {
    value: 'marketing_meeting'
    label: I18n.t('js.constants.purpose.marketing_meeting')
  }
  {
    value: 'investor_meeting'
    label: I18n.t('js.constants.purpose.investor_meeting')
  }
  {
    value: 'external_conference'
    label: I18n.t('js.constants.purpose.external_conference')
  }
  {
    value: 'external_training'
    label: I18n.t('js.constants.purpose.external_training')
  }
  {
    value: 'fitch_internal_training'
    label: I18n.t('js.constants.purpose.fitch_internal_training')
  }
  {
    value: 'internal_conference'
    label: I18n.t('js.constants.purpose.internal_conference')
  }
  {
    value: 'other_external_travel'
    label: I18n.t('js.constants.purpose.other_external_travel')
  }
  {
    value: 'others'
    label: I18n.t('js.constants.purpose.others')
  }
  {
    value: 'sap_project_travel'
    label: I18n.t('js.constants.purpose.sap_project_travel')
  }
  {
    value: 'visit_other_fitch_office'
    label: I18n.t('js.constants.purpose.visit_other_fitch_office')
  }
]

@PAYMENT_TYPE = [
  {
    value: 'corporate_amex'
    label: I18n.t('js.constants.payment_type.corporate_amex')
  }
  {
    value: 'cash'
    label: I18n.t('js.constants.payment_type.cash')
  }
  {
    value: 'corporate_travel'
    label: I18n.t('js.constants.payment_type.corporate_travel')
  }
  {
    value: 'inter_office_recharge'
    label: I18n.t('js.constants.payment_type.inter_office_recharge')
  }
]

@CURRENCY = [
  {
    value: 'aed'
    label: I18n.t('js.constants.currency.aed')
  }
  {
    value: 'aud'
    label: I18n.t('js.constants.currency.aud')
  }
  {
    value: 'bnd'
    label: I18n.t('js.constants.currency.bnd')
  }
  {
    value: 'chf'
    label: I18n.t('js.constants.currency.chf')
  }
  {
    value: 'cny'
    label: I18n.t('js.constants.currency.cny')
  }
  {
    value: 'eur'
    label: I18n.t('js.constants.currency.eur')
  }
  {
    value: 'gbp'
    label: I18n.t('js.constants.currency.gbp')
  }
  {
    value: 'hkd'
    label: I18n.t('js.constants.currency.hkd')
  }
  {
    value: 'idr'
    label: I18n.t('js.constants.currency.idr')
  }
  {
    value: 'inr'
    label: I18n.t('js.constants.currency.inr')
  }
  {
    value: 'jod'
    label: I18n.t('js.constants.currency.jod')
  }
  {
    value: 'jpy'
    label: I18n.t('js.constants.currency.jpy')
  }
  {
    value: 'krw'
    label: I18n.t('js.constants.currency.krw')
  }
  {
    value: 'kzt'
    label: I18n.t('js.constants.currency.kzt')
  }
  {
    value: 'lkr'
    label: I18n.t('js.constants.currency.lkr')
  }
  {
    value: 'myr'
    label: I18n.t('js.constants.currency.myr')
  }
  {
    value: 'nzd'
    label: I18n.t('js.constants.currency.nzd')
  }
  {
    value: 'php'
    label: I18n.t('js.constants.currency.php')
  }
  {
    value: 'sgd'
    label: I18n.t('js.constants.currency.sgd')
  }
  {
    value: 'thb'
    label: I18n.t('js.constants.currency.thb')
  }
  {
    value: 'twd'
    label: I18n.t('js.constants.currency.twd')
  }
  {
    value: 'usd'
    label: I18n.t('js.constants.currency.usd')
  }
  {
    value: 'vnd'
    label: I18n.t('js.constants.currency.vnd')
  }
]

@TIME_TYPE = [
  {
    value: 'morning'
    label: I18n.t('js.constants.time_type.morning')
  }
  {
    value: 'night'
    label: I18n.t('js.constants.time_type.night')
  }
]

@TRANSPORTATION_TYPE = [
  {
    value: 'flight'
    label: I18n.t('js.constants.transportation_type.flight')
  }
  {
    value: 'train'
    label: I18n.t('js.constants.transportation_type.train')
  }
]

@BOOKING_CLASS = [
  {
    value: 'economy'
    label: I18n.t('js.constants.booking_type.economy_class')
  }
  {
    value: 'business'
    label: I18n.t('js.constants.booking_type.business_class')
  }
]