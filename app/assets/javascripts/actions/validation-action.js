var Reflux = require('reflux');
var ValidationActions = Reflux.createActions([
    'init',
    'validate',
    'validationSuccess',
    'validationError'
]);
export default ValidationActions;
