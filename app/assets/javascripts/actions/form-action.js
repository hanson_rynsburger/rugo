var Reflux = require('reflux');
var FormActions = Reflux.createActions([
    'init',
    'create',
    'update',
    'delete',
    'updateAnswer'
]);
export default FormActions;
