var Reflux = require('reflux');
var AlertActions = Reflux.createActions([
    'error',
    'success',
    'alert'
]);

export default AlertActions;
