var Reflux = require('reflux');
var request = require('superagent');
var TaskActions = Reflux.createActions([
    "taskUpdate",
    "taskAdd",
    "taskDelete",
    "taskCancel",
    "taskApprove",
    "taskReject",

  ]);


TaskActions.taskUpdate.preEmit = function (todo) {
  console.log(JSON.stringify(todo));
    // request.post('/todo/', {todo: todo}, function () {});
};

TaskActions.taskAdd.preEmit = function (id) {
    request.del('/todo/'+id+'/', function () {});
};

TaskActions.taskDelete.preEmit = function (id) {
    request.put('/todo/'+id+'/', {"is_checked": true}, function () {});
};

module.exports = TaskActions;
