var Reflux = require('reflux');
var FormActions = require("../actions/form-action.js");
var ValidationActions = require("../actions/validation-action.js");
var ValidationStore = require("../stores/validation-store.js");

var SimpleFormMixin = {
	mixins: [Reflux.listenTo(ValidationStore, 'onValidate')],
	onValidate: function(save){
		var error = this.refs.form.validate();
		if (error.errors.length){
			ValidationActions.validationError(this.props.id, error.errors[0].message);
		}else{
			var value = this.refs.form.getValue();
			FormActions.updateAnswer(this.props.id, value.value, true);	
			ValidationActions.validationSuccess(this.props.id, save);
		}
	},
};

export default SimpleFormMixin;