var React = require("react");

let TaskFilter = React.createClass({
  onClickFilter: function(value, e){
    e.preventDefault();
    e.stopPropagation();
    return this.props.onFilter(value);
  },
  render: function() {
    var _this = this;
    var valueList = _.map(this.props.filters, function(d, key){
      return (
        <li><a href="#" onClick={_this.onClickFilter.bind(_this, key)}>{d} {key==_this.props.value?(<i className="fa fa-check"></i>):null}</a></li>
      );
    });
    return (
      <ul className="pull-right">
        <li className="dropdown">
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">{this.props.name} <span className="caret"></span></a>
          <div className="dropdown-menu">
            <div className="arrow"></div>
            <ul>
              {valueList}
            </ul>
          </div>
        </li>
      </ul>
    );
  }
});

export default TaskFilter;