var React = require("react");
var Task = React.createClass({
  propTypes: {
    task: React.PropTypes.object
  },

  render: function() {
    var statusClasses="label label-" + this.props.status;
    var task_url = this.props.url.replace('.json', '');
    //TODO update activities

    return (
      <div className="block block-default list-block">
        <div className="block-body">
          <a className="link-wrapper" href={ task_url }>
            <span className="circle-icon icon-money"></span>
            <h4 className="block-title">{ this.props.form.name }
              <span className={statusClasses}>{ this.props.status }</span>
            </h4>
            <span className="list-desc">
              { this.props.start_at }
              to
              { this.props.end_at }
            </span>
            <div className="attributes">
              <span><i className="pi pi-clock"></i>{ this.props.created_at }</span>
              <span><i className="pi pi-person"></i> Submitted by { this.props.employees[this.props.author_id] }.</span>
              <span><i className="pi pi-person"></i> Assigned to { this.props.employees[this.props.assignee_id] }.</span>
              <span><i className="pi pi-person"></i> Request for { this.props.employees[this.props.user_id] }.</span>
            </div>
          </a>
          <div className="block block-alt mt10 last-conversation">
            <div className="block-body clearfix">
              <div className="avatar">
                <div className="avatar-img">
                  <div className="avatar-initials">
                    <i className="fa fa-user fa-2x default-icon"></i>
                  </div>
                </div>
              </div>
              <h4 className="pull-left">Vito Goh</h4>
              <span className="pull-right text-12 text-muted-light">less than a minute ago</span>
              <p className="msg text-14 text-primary">Have fun . =)</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
module.exports = Task
