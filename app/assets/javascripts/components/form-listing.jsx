var React = require('react');

let FormListing = React.createClass({
	onCreate: function(form_index, e){
		e.preventDefault();
		this.props.onCreate(form_index);
	},
	render: function(){
		var formLis = [];
		for (var i in this.props.forms){
			formLis.push(<li><a href="#" onClick={this.onCreate.bind(this, i)}>{this.props.forms[i].name}</a></li>);
		}

		return (
			<div className="dropdown pull-right">
	      <a className="btn btn-lg btn-primary dropdown-toggle" data-toggle="dropdown">
	      	<i className="pi pi-plus"></i>Create New<i className="pi pi-arrow-drop-down"></i>
	      </a>
	      <div className="dropdown-menu">
	        <div classNae="arrow"></div>
	        <ul>
	          {formLis}
	        </ul>
	      </div>
	    </div>
    );
	}
});

export default FormListing;
