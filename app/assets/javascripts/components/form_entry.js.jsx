var React = require("react");
var Reflux = require('reflux');
var ExpenseList = require("./fields/expense");
var Activities = require("./activities");
var TaskStore = require("../stores/taskStore");
var TaskAction = require("../actions/tasks");
var FormApp = React.createClass({
  mixins: [Reflux.connect(TaskStore,"task")],
  propTypes: {
    task: React.PropTypes.object
  },
  getInitialState: function () {
    return {editing:false};
  },
  toggleEdit:function(){
    this.setState({editing: !this.state.editing});
  },
  updateAnswer:function(answer, id){
    this.state.task.answers[id] = answer;
    TaskAction.taskUpdate(this.state.task);
  },
  render: function() {
    var self = this;
    var nodes = this.state.task.form.fields.map(function ( obj ) {
      var FormField = fields[obj.field_type];
      return (
        <FormField question={obj}  answer={self.state.task.answers[obj.id]} updateAnswer={self.updateAnswer}/>
        );
    });
    var header;
    if(this.state.editing){
      header = <FormEditHeader  user={this.state.task.user} approver={this.state.task.approver} toggleEdit={this.toggleEdit}/>
    }else{
      header = <FormHeader user={this.state.task.user} approver={this.state.task.approver} icon={this.state.task.form.icon} created_at={this.state.task.created_at} status={this.state.task.status} toggleEdit={this.toggleEdit}/>
    }
    return (
      <div>
        <div className="sticky-header">
          <div className="content-wrapper">
            <div className="breadcrumb">
              <a href ="/tasks"><i className='pi pi-arrow-back'></i>Back To Tasks</a>
            </div>
            <div className="page-actions">
              <a href="javascript:void(0)" className="btn btn-sm btn-primary">Approve</a>
              <div className="dropdown">
                <a href="javascript:void(0)" className="btn btn-sm btn-secondary dropdown-toggle">Assign <span className="caret"></span></a>
              </div>
              <div className="btn-group">
                <a href="javascript:void(0)" className= "btn btn-sm btn-secondary-light"><i className="pi pi-chevron-left"></i></a>
                <a href="javascript:void(0)" className= "btn btn-sm btn-secondary-light"><i className="pi pi-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div className="pages tasks">
          { header }
          { nodes }
          <Activities activities={[]} />
        </div>
      </div>
    );
  }
});

var FormHeader = React.createClass({
  propTypes: {
    icon: React.PropTypes.string,
    status: React.PropTypes.string,
    user: React.PropTypes.object,
    approver:React.PropTypes.object,
    status:React.PropTypes.string,
    created_at:React.PropTypes.string
  },
  render: function() {
    var formIconClasses = "circle-icon " + this.props.icon;
    var statusClass= "label label-" + this.props.status;
    return(
      <div className="heading">
        <span className={formIconClasses}></span>
        <a href="javascript:void(0)" className="link-gray pull-right link-edit" onClick={this.props.toggleEdit}><i className='pi pi-edit'></i>Edit</a>
        <h2>Expense Task</h2>
        <span className="task-detail-description">Task Description</span><br />
        <span className="task-detail-duration">01/01/2015 to 01/01/2015</span><br />
        <h4 className="status-label"><span className={statusClass}>{this.props.status.toUpperCase()}</span></h4>
        <div className="attributes">
          <span><i className="pi pi-clock"></i>{this.props.created_at}</span>
          <span><i className="pi pi-person"></i> Submitted by <a href="#">{this.props.user.name}</a></span>
          <span><i className="pi pi-person"></i> Assigned to <a href="#">{this.props.approver.name}</a></span>
          <span><i className="pi pi-person"></i> Request for <a href="#"></a></span>
        </div>
      </div>
    )
  }

});

var FormEditHeader = React.createClass({
  propTypes: {
    user: React.PropTypes.object,
    approver:React.PropTypes.object,
    status:React.PropTypes.string,
    created_at:React.PropTypes.string
  },
  render: function() {
    return(
      <div className="block block-secondary">
        <div className="block-body">
          <div className="form-group">
            <div className="col-xs-6">
              <label>Title</label>
              <input type="text" className="form-control" />
            </div>
            <div className="col-xs-6">
              <label>Description</label>
              <input type="text" className="form-control" />
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-6">
              <div className="row">
                <div className="col-xs-6">
                  <label>From</label>
                  <div className="input-group">
                    <div className="input-icon">
                      <span className="input-group-addon"><i className="pi pi-calendar"></i></span>
                      <input type="text" className="form-control" />
                    </div>
                  </div>
                </div>
                <div className="col-xs-6">
                  <label>To</label>
                  <div className="input-group">
                    <div className="input-icon">
                      <span className="input-group-addon"><i className="pi pi-calendar"></i></span>
                      <input type="text" className="form-control" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-6">
              <label>Who is approving this?</label>
              <select className="form-control selectpicker">
                <option>Select</option>
                <option>Ziwei Zhou</option>
                <option>Takashi Obatake</option>
                <option>Audrey Pang</option>
              </select>
            </div>
            <div className="col-xs-6">
              <label>Who is this request for?</label>
              <select className="form-control selectpicker">
                <option>Select</option>
                <option>Ziwei Zhou</option>
                <option>Takashi Obatake</option>
                <option>Audrey Pang</option>
              </select>
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-12">
              <div className="pull-left">
                <a href="javascript:void(0);" className="btn btn-sm btn-primary">Submit for Approval</a>
                <a href="javascript:void(0);" className="btn btn-sm btn-orange">Save as Draft</a>
                <a href="javascript:void(0);" className="btn btn-link link-gray" onClick={this.props.toggleEdit}>Cancel</a>
              </div>
              <div className="pull-right">
                <a href="javascript:void(0);" className="btn btn-link link-gray"><i className="pi pi-trash-o text-danger"></i>Delete Request</a>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-12">
              <div className="confirm-submission">
                <div className="row">
                  <div className="col-xs-6">
                    <input type="text" className="form-control" placeholder="Enter Your Full Name as Signature" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-12">
              <label className="checkbox-inline"><input type="checkbox" className="pull-left" />
                I declare that the above expenses have been reasonably and actually
                incurred by me in fulfilling my responsibilities as an employee of Fitch.
              </label>
            </div>
          </div>
          <div className="form-group">
            <div className="col-xs-12">
              <button type="submit" className="btn btn-sm btn-green">Confirm and Submit</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

});

var fields = {
   "expense": ExpenseList
};
module.exports = FormApp;
// export default FormApp;
