var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var SimpleFormMixin = require('../mixins/simple-form-mixin.js');

let RatingField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){
  	var properties = this.props.fields.properties;
  	var RatingStruct = {};
  	var icons = [];

  	for (var i = 1; i <= properties.max_rating; i++ ){
  		switch (properties.symbol){
  			case 'number':
  				RatingStruct[i] = i;
  				break;
  			default:
  				icons.push(<i className={'fa '+properties.symbol}></i>);
  				RatingStruct[i] = (<span>{icons.slice(0)}</span>);
  		}
  	}

  	var RatingType = t.enums(RatingStruct);
  	var RatingForm = t.struct({
			value: (this.props.fields.required?RatingType:t.maybe(RatingType))
		});

		var options = {
		  fields: {
		    value: {
		    	label: properties.label + (this.props.fields.required?' *':''),
		      help: properties.hint,
		      factory: t.form.Radio,
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
      	<Form type={RatingForm} value={values} options={options} ref="form"></Form>
    );
  }
});
export default RatingField;
