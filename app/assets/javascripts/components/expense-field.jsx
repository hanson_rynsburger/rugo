var React = require('react');
var Reflux = require('reflux');
var ExpenseEdit = require('./fields/expense/expense-edit.jsx');
var ExpenseRow = require('./fields/expense/expense-row.jsx');
var ExpenseChart = require('./fields/expense/expense-chart.jsx');
var FormActions = require("../actions/form-action.js");
var Helper = require('../helper.js');

let ExpenseField = React.createClass({
	getInitialState: function(){
		return {
			visibleRow: -1, //-1 is used to hide all
			newEntry: false,
			view: 'table'
		};
	},
	getDefaultProps: function(){ //solution for first answer
		return {
			value: []
		};
	},
	
	onToggleNewManualEntry: function(){
		this.setState({
			newEntry: !this.state.newEntry
		});
	},
	onToggleEdit: function(id){
		if (id != this.state.visibleRow){
			this.setState({
				visibleRow: id
			});
		}
	},
	onAnswerChange: function(id, answer){
		if (id == 'new'){
			this.props.value.push(answer);
		}else{
			this.props.value[id] = answer;
		}
		FormActions.updateAnswer(this.props.id, this.props.value);
		this.setState({
			visibleRow: -1,
			newEntry: false
		});
	},
	onDeleteAnswer: function(id){
		if (!confirm('Are you sure you want to delete this answer?')){
			return;
		}
		if (this.props.value[id]){
			var value = this.props.value;
			value.splice(id, 1);
		}
		FormActions.updateAnswer(this.props.id, value);
		this.setState({
			visibleRow: -1,
			newEntry: false
		});
	},
	onViewChange: function(view){
		this.setState({
			view: view
		});
	},
	render: function(){
		var tableRows = [];
		for (var i in this.props.value){
			var id = i;
			tableRows.push(<ExpenseRow fields={this.props.fields} value={this.props.value[i]} id={id} onToggleEdit={this.onToggleEdit.bind(this, id)}></ExpenseRow>);
			if (this.state.visibleRow == id){
				tableRows.push(<ExpenseEdit
					fields={this.props.fields}
					value={this.props.value[i]}
					id={id}
					onSave={this.onAnswerChange}
					onToggleEdit={this.onToggleEdit.bind(this, -1)}
					onDelete={this.onDeleteAnswer}
					></ExpenseEdit>);
			}
		}

		var categories = Helper.convertToAssociativeArray(this.props.fields.properties.categories);

		//TODO: implement filter menu
		return (
			<div className="expense-detail">
				<div className="action-menu">
					<div className="pull-left">
						<form className="form-inline">
							<div className="form-group mb0">
								<label>Filter By</label>
								<select className="form-control">
								</select>
							</div>
						</form>
					</div>
					<div className="btn-group pull-right">
						<button className={"btn btn-default pi pi-table " + (this.state.view=='table'?'active':'')} type="button" onClick={this.onViewChange.bind(this, 'table')}></button>
						<button className={"btn btn-default pi pi-spinner "  + (this.state.view=='chart'?'active':'')} type="button" onClick={this.onViewChange.bind(this, 'chart')}></button>
					</div>
				</div>
				{
					this.state.view == 'table'?
					(<div>
						<div className="table-view no-border">
							<table className="table table-bordered">
								<thead>
									<tr>
										<th><span>Status</span></th>
										<th><span>Date</span></th>
										<th><span>Category</span></th>
										<th><span>Description</span></th>
										<th><span>Amount</span></th>
									</tr>
								</thead>
								<tbody>
									{tableRows}
								</tbody>
							</table>
						</div>
						<div className="dropdown">
							<a className="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
								<i className='pi pi-plus'></i>
								Add New Entry <i className='pi pi-arrow-drop-down'></i>
							</a>
						    <div className="dropdown-menu">
						      	<div className="arrow"></div>
						      	<ul>
							        <li><a href="javascript:void(0)" onClick={this.onToggleNewManualEntry}>Manual Entry</a></li>
							        <li><a href="javascript:void(0)">Import</a></li>
						      	</ul>
						    </div>
						</div>
						<div>
						{
							this.state.newEntry? <ExpenseEdit fields={this.props.fields} id='new' onSave={this.onAnswerChange}  onToggleEdit={this.onToggleNewManualEntry}></ExpenseEdit> : null
						}
						</div>
					</div>)
					:(<div className="chart-view no-border">
						<ExpenseChart data={this.props.value} categories={categories}></ExpenseChart>
					</div>)
				}
			</div>
		);
	}
});

export default ExpenseField;
