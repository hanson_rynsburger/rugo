var React = require('react');
var TravelEdit = require('./fields/travel/travel-edit.jsx');
var TravelRow = require('./fields/travel/travel-row.jsx');
var FormActions = require("../actions/form-action.js");
let TravelField = React.createClass({
	getInitialState: function(){
		return {
			visibleRow: -1,
			newEntry: false
		};
	},
	getDefaultProps: function(){ //solution for first answer
		return {
			value: []
		};
	},
	onToggleNew: function(){
		this.setState({
			newEntry: !this.state.newEntry
		});
	},
	onToggleEdit: function(id){
		if (id != this.state.visibleRow){
			this.replaceState({
				visibleRow: id
			});
		}
	},
	onAnswerChange: function(id, answer){
		if (id == 'new'){
			this.props.value.push(answer);
		}else{
			this.props.value[id] = answer;
		}
		FormActions.updateAnswer(this.props.id, this.props.value);
		this.setState({
			visibleRow: -1,
			newEntry: false
		});
	},
	onDeleteAnswer: function(id){
		if (!confirm('Are you sure you want to delete this answer?')){
			return;
		}
		if (this.props.value[id]){
			var value = this.props.value;
			value.splice(id, 1);
		}
		FormActions.updateAnswer(this.props.id, value);
		this.setState({
			visibleRow: -1,
			newEntry: false
		});
	},
	render: function(){
		var tableRows = [];
		for (var i in this.props.value){
			var id = i;
			tableRows.push(<TravelRow fields={this.props.fields} value={this.props.value[i]} id={id} onToggleEdit={this.onToggleEdit.bind(this, id)}></TravelRow>);
			if (this.state.visibleRow == id){
				tableRows.push(<TravelEdit
					fields={this.props.fields}
					value={this.props.value[i]}
					id={id}
					onSave={this.onAnswerChange}
					onToggleEdit={this.onToggleEdit.bind(this, -1)}
					onDelete={this.onDeleteAnswer}
					></TravelEdit>);
			}
		}

		return (
			<div className="travel-detail">
				<div className="row">
				    <div className="col-xs-12">
				      	<h2>Travel Details</h2>
				    </div>
				</div>
				<div className="list-group">
					{tableRows}
				</div>
				<div className="mt10">
				    <a href="javascript:void(0)" onClick={this.onToggleNew}><i className="pi pi-plus"></i> Add a Destination</a>
			  	</div>
			  	<div>
				{
					this.state.newEntry? <TravelEdit fields={this.props.fields} id='new' onToggleEdit={this.onToggleNew} onSave={this.onAnswerChange}></TravelEdit> : null
				}
				</div>
			</div>
		);
	}
});

export default TravelField;
