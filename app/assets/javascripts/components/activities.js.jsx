var React = require("react");
var Activities = React.createClass({
  propTypes: {
    activities: React.PropTypes.array
  },
  render: function() {
    var nodes = this.props.activities.map(function ( activity ) {
      return <Activity { ...activity }/>
    });
    return (
      <div className="page-activities">
        <h2>Activities</h2>
        <div className="row">
          <div className="col-xs-10 col-xs-offset-2">
            <div className="activities">
              <form className="comment-input">
                <div className="form-group">
                  <textarea type="text" className="form-control" rows= "4" ng-model="comment"></textarea>
                </div>
                <div className="clearfix">
                  <div className="btn btn-primary pull-left">Send</div>
                </div>
              </form>
              { nodes }
              <div className="text-center">
                <button className="btn btn-md btn-secondary show-more" type="button">Show more</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var Activity = React.createClass({
  propTypes: {
    activity: React.PropTypes.object
  },
  render: function() {
    return (
      <div className="activities-msg">
        <div className="activity">
          <div className="avatar">
            <span className="timestamp">2 minutes ago</span>
            <div className="avatar-img">
              <img alt="Img avatar default" className="img-circle" src="/assets/img-avatar-default.png" />
              <div className="avatar-initials">
                JD
              </div>
            </div>
          </div>
          <div className="block block-default no-border">
            <span>
              Test Comments.
            </span>
          </div>
        </div>
      </div>
    );
  }
});
module.exports = Activities;
