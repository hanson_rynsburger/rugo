var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var DatePickerTemplate = require('./fields/date-picker-template.js');
var Select2Template = require('./fields/select2-template.js');
var AlertActions = require("../actions/alert-action.js");
var FormActions = require("../actions/form-action.js");

var FormHeaderNew = React.createClass({
	onSave: function(){
		var error = this.refs.form.validate();
		if (error.errors.length){
			//TODO make multiple alert div
			AlertActions.error(error.errors[0].message);
		}else{
			var value = this.refs.form.getValue();
		    if (value) {
		      	FormActions.create(this.props.id, value);
		    }
		}
	},
	onCancel: function(e){
		e.preventDefault();
		this.props.onCancel();
	},
	render: function(){
		var _this = this;
		var UserType = t.enums(this.props.employees);
		var newStruct = {
			description: t.Str,
			user_id: UserType
		};

		if (this.props.show_on_calendar){
			newStruct = _.extend(newStruct, {
				start_at: t.Str,
				end_at: t.Str
			});
		}

		var FormHeaderNewType = t.struct(newStruct);

		var FormHeaderNewTemplate = function(locals){
			return (
				<div>
					<div className="form-group">
						<div className="col-xs-6">
							<label className="control-label">Title</label>
							<span className="form-control" disabled>{_this.props.name}</span>
						</div>
						<div className="col-xs-6">
							{locals.inputs.user_id}
						</div>
					</div>
					{
						_this.props.show_on_calendar?
						(<div className="form-group">
							<div className="col-xs-6">
								<div className="row">
									<div className="col-xs-6">
										{locals.inputs.start_at}
									</div>
									<div className="col-xs-6">
										{locals.inputs.end_at}
									</div>
								</div>
							</div>
						</div>):null
					}
					<div className="form-group">
						<div className="col-xs-12">
							{locals.inputs.description}
						</div>
					</div>
				</div>
			);
		};

		var formHeaderNewOptions = {
			template: FormHeaderNewTemplate,
			fields: {
				description: {
					label: 'Description',
					type: 'textarea',
					config: {
						rows: 2
					}
				},
				start_at: {
					label: 'From',
					template: DatePickerTemplate
				},
				end_at: {
					label: 'To',
					template: DatePickerTemplate
				},
				approver_id: {
					label: 'Who is approving this?'
				},
				user_id: {
					label: 'Who is this request for?',
					template: Select2Template
				}
			}
		};
		return (
			<div>
				
				<div className="block block-secondary">
					<div className="block-body">
						<Form ref="form" type={FormHeaderNewType} options={formHeaderNewOptions} value={ {user_id: current_user_id.toString()} }/>
						<div className="form-group">
							<div className="col-xs-12">
								<button type="submit" className="btn btn-sm btn-green" onClick={this.onSave}>Save</button>
								<a href="#" onClick={this.onCancel} className="btn btn-link link-gray">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

export default FormHeaderNew;
