var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var SimpleFormMixin = require('../mixins/simple-form-mixin.js');

let BooleanField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){
  	var BooleanType = t.enums({
  		'Yes':  this.props.fields.properties.true_label,
  		'No': this.props.fields.properties.false_label
  	});

  	var BooleanForm = t.struct({
			value: (this.props.fields.required?BooleanType:t.maybe(BooleanType))
		});

		var options = {
		  fields: {
		    value: {
		    	label: this.props.fields.properties.label + (this.props.fields.required?' *':''),
		      help: this.props.fields.properties.hint,
		      factory: t.form.Radio,
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
      	<Form type={BooleanForm} value={values} options={options} ref="form"></Form>
    );
  }
});
export default BooleanField;
