var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var SimpleFormMixin = require('../mixins/simple-form-mixin.js');
var DatePickerTemplate = require('./fields/date-picker-template.js');

let DateField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){
  	var DateForm = t.struct({
			value: (this.props.fields.required?t.Str:t.maybe(t.Str))
		});

		var options = {
		  fields: {
		    value: {
		    	label: this.props.fields.properties.label + (this.props.fields.required?' *':''),
		      help: this.props.fields.properties.hint,
	      	placeholder: this.props.fields.properties.placeholder,
		      template: DatePickerTemplate
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
    	<Form type={DateForm} value={values} options={options} ref="form"></Form>
    );
  }
});
export default DateField;
