var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var SimpleFormMixin = require('../mixins/simple-form-mixin.js');

let CheckboxField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){

  	var CheckboxForm = t.struct({
			value: t.list(t.Bool)
		});

		var options = {
		  fields: {
		    value: {
		    	label: this.props.fields.properties.label + (this.props.fields.required?' *':''),
		      help: this.props.fields.properties.hint
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
      	<Form type={CheckboxForm} value={values} options={options} ref="form"></Form>
    );
  }
});
export default CheckboxField;
