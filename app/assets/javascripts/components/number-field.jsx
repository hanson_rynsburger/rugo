var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var subtype = t.subtype;

var SimpleFormMixin = require('../mixins/simple-form-mixin.js');

let NumberField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){
  	var _this = this;
  	var predicate = function (v){
			return v >= _this.props.fields.properties.from_number && v <= _this.props.fields.properties.to_number;
		};

  	var NumberType = t.subtype(t.Num, predicate, 'Between '+this.props.fields.properties.from_number+' and '+this.props.fields.properties.to_number);
  	var NumberForm = t.struct({
			value: (this.props.fields.required?NumberType:t.maybe(NumberType))
		});

		var options = {
		  fields: {
		    value: {
		    	label: this.props.fields.properties.label + (this.props.fields.required?' *':''),
		      help: this.props.fields.properties.hint
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
      <div>
      	<Form type={NumberForm} value={values} options={options} ref="form"></Form>
      </div>
    );
  }
});
export default NumberField;
