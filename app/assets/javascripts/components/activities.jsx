var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;

var ActivityMsg = React.createClass({
	getInitials: function(name){
		var part = name.split(' ');
		if (part.length>1){
			return part[0].substr(0,1).toUpperCase() + part[1].substr(0,1).toUpperCase();
		}else{
			return part[0].substr(0,2).toUpperCase();
		}
	},
	render: function(){
		var avatar;
		if (typeof this.props.avatar_url !== 'undefined'){
			avatar = <img alt="img avatar" className="img-circle" src={this.props.avatar_url} />;
		}else{
			avatar = <div className="avatar-initials">{this.getInitials(this.props.name)}</div>
		}

		return (
			<div className="acitivity">
				<div className="avatar">
					<span className="timestamp">{moment(this.props.created_at).fromNow()}</span>
					<div className="avatar-img">
						{avatar}
					</div>
				</div>
				<div className="block block-default no-border">
					<span>{this.props.name}</span>
					<span>{this.props.status}</span>
					<span>{this.props.comment}</span>
				</div>
			</div>
		);
	}
});

var ActivityMsgs = React.createClass({
	getInitialState: function(){
		//TODO load activities by using AJAX calls
		return {
			activities: [{
				created_at:"2015-05-05T14:47:37.342+08:00",
				name: 'John Smith',
				status: 'approved',
				comment: 'comment goes here'
			}]
		};
	},
	render: function(){
		var msgs = [];
		for (var i in this.state.activities){
			var activity = this.state.activities[i];
			msgs.push(
				<ActivityMsg {...activity} />
			);
		}
		return (
			<div>
				<div className="activities-msg">
					{msgs}
				</div>
				<div className="text-center">
					<button className="btn btn-md btn-secondary show-more">
						Show All Activities
					</button>
				</div>
			</div>
		);
	}
});


let Activities = React.createClass({
	render: function(){
		//TODO use ajax to send activity
		//define activity t.comb form
		var Activity = t.struct({
			comment: t.Str
		});
		var ActivityTemplate = function(locals){
			return (
				<div className="form-group">
                	{locals.inputs.comment}
              	</div>
			);
		};
		var ActivityOptions = {
			template: ActivityTemplate,
			fields: {
				comment: {
					label: ' ',
					type: 'textarea',
					config: {
						rows: 4
					}
				}
			}
		};

		return (
			<div className="page-activities">
				<h2>Activities</h2>
				<div className="row">
					<div className="col-xs-10 col-xs-offset-2">
						<div className="activities">
							<form className="comment-input">
								<Form type={Activity} options={ActivityOptions}>
								</Form>
								<div className="clearfix">
									<a href="javscript:void(0)" className="btn btn-primary pull-left">Send</a>
									<div className="input-file input-file-link pull-right">
										<a href="javscript:void(0);"><i className="pi pi-paperclip"></i> Attach File</a>
									</div>
								</div>
							</form>
							<ActivityMsgs />
						</div>
					</div>
				</div>
			</div>
		);
	}
});

export default Activities;