var React = require("react");
var Task = require("./task.jsx");
var TaskFilter = require("./task-filter.jsx");
var TaskSort = require("./task-sort.jsx");
var t = require('tcomb-form');
var Form = t.form.Form;

let TaskList = React.createClass({
  getInitialState: function(){
    return {
      filter:{
        'author_id' : 0,
        'assignee_id': 0,
        'status': 0,
        'user_id': 0
      },
      sortby: 'id',
      sortdir: 'asc',
      activeTab: 0
    };
  },

  onClickFilter: function(filterby, value){
    var filter = this.state.filter;
    filter[filterby] = value;

    this.setState({
      activeTab: 0,
      filter: filter
    });
  },

  onClickSort: function(sortby){
    this.setState({
      sortby: sortby,
      sortdir: (this.state.sortdir == 'asc' && this.state.sortby == sortby)?'desc':'asc'
    });
  },

  onTabSelect: function(tab, e){
    e.preventDefault();
    e.stopPropagation();

    var filter = {
      author_id: 0,
      assignee_id: 0,
      status: 0,
      user_id: 0
    };

    switch (tab){
      case 1:
        filter.author_id = current_user_id;
        break;
      case 2:
        filter.assignee_id = current_user_id;
        break;
      case 3:
        filter.user_id = current_user_id;
        break;
    }

    this.setState({
      activeTab: tab,
      filter: filter
    });
  },

  propTypes: {
    tasks: React.PropTypes.array
  },

  render: function() {
    var _this = this;
    var total = {};
    total[0] = this.props.tasks.length; //all
    total[1] = _.filter(this.props.tasks, function(d){ 
      return d.author_id == current_user_id;
    }).length; //created by you
    total[2] = _.filter(this.props.tasks, function(d){ 
      return d.approver_id == current_user_id;
    }).length; //needs approval by you
    total[3] = _.filter(this.props.tasks, function(d){ 
      return d.user_id == current_user_id;
    }).length; //assigned to you

    //filtering and sorting 
    var taskNodes = _.chain(this.props.tasks)
      .filter(function(d){
        var result = true;
        _.each(_this.state.filter, function(f, key){
          if (f!=0 && d[key]!=f){
            result = false;
          }
        });
        return result;
      })
      .sortBy(function(d){
        return d[_this.state.sortby];
      })
      .map(function(task){
        return <Task { ...task } employees={_this.props.employees} />;
      })
      .value();

    if (this.state.sortdir == 'desc'){
      taskNodes.reverse();
    }

    var userList = _.extend({
      0: 'All'
    }, this.props.employees);

    var filterSettings = [
      {
        name: 'Author',
        key: 'author_id',
        filters: userList
      },
      {
        name: 'Assignee',
        key: 'assignee_id',
        filters: userList
      },
      {
        name: 'Status',
        key: 'status',
        filters: {
          '0': 'All',
          'draft': 'Draft',
          'pending': 'Pending',
          'submitted': 'Submitted'
        }
      },
    ];

    var sortList = {
      'id': 'ID',
      'author': 'Author',
      'assignee': 'Assignee',
      'updated_at': 'Date',
      'status': 'Status',
      'title': 'Title'
    };

    var filterComponents = _.map(filterSettings, function(d){
      return (<TaskFilter {...d} value={_this.state.filter[d.key]} onFilter={_this.onClickFilter.bind(_this, d.key)} />);
    });
    return (
      <div>
        <div className="action-menu">
          <ul className="pull-left">
            <li className={this.state.activeTab==0?'active':''}><a href="#" onClick={this.onTabSelect.bind(this, 0)}>All <span className="badge badge-default">{total[0]}</span></a></li>
            <li className={this.state.activeTab==1?'active':''}><a href="#" onClick={this.onTabSelect.bind(this, 1)}>Created by you <span className="badge badge-default">{total[1]}</span></a></li>
            <li className={this.state.activeTab==2?'active':''}><a href="#" onClick={this.onTabSelect.bind(this, 2)}>Needs your approval <span className="badge badge-default">{total[2]}</span></a></li>
            <li className={this.state.activeTab==3?'active':''}><a href="#" onClick={this.onTabSelect.bind(this, 3)}>Your tasks <span className="badge badge-default">{total[3]}</span></a></li>
          </ul>
          {
            <TaskSort name="Sort" value={this.state.sortby} sorts={sortList} sortdir={this.state.sortdir} onSort={this.onClickSort} />
          }
          {
            filterComponents
          }
        </div>
        <div className="row">
          <div className="col-md-12">
            { taskNodes }
          </div>
        </div>
      </div>
    );
  }
});

export default TaskList;
