var React = require("react");

let TaskSort = React.createClass({
  onClickSort: function(value, e){
    e.preventDefault();
    e.stopPropagation();
    return this.props.onSort(value);
  },
  render: function() {
    var _this = this;
    var valueList = _.map(this.props.sorts, function(d, key){
      return (
        <li><a href="#" onClick={_this.onClickSort.bind(_this, key)}>{d} {key==_this.props.value?(<i className={'fa fa-sort-'+_this.props.sortdir}></i>):null}</a></li>
      );
    });
    return (
      <ul className="pull-right">
        <li className="dropdown">
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">{this.props.name} <span className="caret"></span></a>
          <div className="dropdown-menu">
            <div className="arrow"></div>
            <ul>
              {valueList}
            </ul>
          </div>
        </li>
      </ul>
    );
  }
});

export default TaskSort;