var React = require('react');
var Reflux = require('reflux');
var AlertStore = require("../stores/alert-store.js");
let AlertBlock = React.createClass({
	//TODO use target for displaying error
	mixins: [Reflux.listenTo(AlertStore, 'onAlert')],
	getInitialState: function (){
		return {
			type: this.props.type,
			target: this.props.target,
			msg: this.props.msg,
			visible: this.props.visible?this.props.visible:false
		};
	},
	onAlert: function(msgObj){
		var _this = this;
		msgObj.visible = true;
		this.setState(msgObj);
		if (this.timeoutInstance){
			clearTimeout(this.timeoutInstance);
			this.timeoutInstance = null;
		}
		this.timeoutInstance = setTimeout(function(){
			_this.setState({
				visible: false
			});
		}, 3000);
	},
	render: function(){
		var className = 'alert alert-' + this.state.type;
		if (!this.state.visible){
			className += ' hidden';
		}
		return (
			<div className={className}>
    			<span className="alert-message">
      				{this.state.msg}
    			</span>
  			</div>
		);
	}
});

export default AlertBlock;
