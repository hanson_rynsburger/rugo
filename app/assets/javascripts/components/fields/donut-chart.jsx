var React = require('react');
var d3 = require('d3');
var ColorStore = require('../../stores/color-store.js');
var PI = 3.14159265358979;

var Chart = React.createClass({
	render: function(){
		return (
			<svg width={this.props.width} height={this.props.height}>
				{this.props.children}
			</svg>
		);
	}
});

var Arc = React.createClass({
	getDefaultProps: function(){
		return{
			color: 'black',
			outerRadius: 70,
			innerRadius: 50,
			text: ''
		}
	},
	arcTween: function(transition, newAngle){
		var _this = this;
		transition.attrTween('d', function(d){
			var interpolate = d3.interpolate(d.endAngle, newAngle);
			return function(t){
				d.endAngle = interpolate(t);
				return _this.arc(d);
			};
		});
	},
	componentDidMount: function(){
		d3.select(this.getDOMNode())
			.select('path')
			.datum({endAngle: this.props.startAngle})
			.transition()
			.duration(500)
			.call(this.arcTween, this.props.endAngle);	
	},
	render: function(){
		var _this = this;
		this.arc = d3.svg.arc()
			.outerRadius(this.props.outerRadius)
			.innerRadius(this.props.innerRadius)
			.startAngle(this.props.startAngle);

		var arc2 = d3.svg.arc()
			.outerRadius(this.props.outerRadius)
			.innerRadius(this.props.innerRadius)
			.startAngle(this.props.startAngle)
			.endAngle(this.props.endAngle);

		var showText = false;
		if ((this.props.endAngle-this.props.startAngle)>2*PI/20){ //showtext if angle is more than 5%
			showText = true;
		}

		return (
			<g className="chart-arc">
				<path d={this.arc(this.props.data)} style={this.props.style} fill={this.props.color}></path>
				{
					showText?(<text className="chart-text" textAnchor="middle" transform={'translate('+arc2.centroid()+')'}>{this.props.text}</text>):null
				}
			</g>
		);
	}
});

var DataSeries = React.createClass({
	getDefaultProps: function(){
		return {
			size: [70, 70]
		};
	},
	render: function(){
		var _this = this;
		var pie = d3.layout.pie();
		var colors = ColorStore.colors;

		var pieData = pie(_.map(this.props.data, function(d){
			return d.value;
		}));

		var sum = _.reduce(this.props.data, function(sum, d){
			return sum + d.value;
		}, 0);

		var cumulativeSum = 0;

		var arcs = _.map(this.props.data, function(point, i){
			var startAngle = cumulativeSum/sum*PI*2;
			cumulativeSum += point.value;
			var endAngle = cumulativeSum/sum*PI*2;
			return (
				<Arc
					style={point.style}
					data={pieData[i]}
					color={colors(i)} 
					key={i} 
					outerRadius={_this.props.size[0]} 
					innerRadius={_this.props.size[1]} 
					text={point.text}
					startAngle = {startAngle}
					endAngle = {endAngle}
				></Arc>
			);
		});

		return (
			<g transform={'translate('+this.props.size[0]+','+this.props.size[0]+')'}>
				{arcs}
				{
					(this.props.text && this.props.text[0])?(<text textAnchor="middle" className="chart-primary-text">{this.props.text[0]}</text>):null
				}
				{
					(this.props.text && this.props.text[1])?(<text textAnchor="middle" dy="20" className="chart-secondary-text">{this.props.text[1]}</text>):null
				}
			</g>
		);
	}
});

let DonutChart = React.createClass({
	getDefaultProps: function() {
		return {
			width: 300,
			height: 300
		};
	},
	render: function(){
		var _this = this;
		return (
			<Chart width={this.props.width} height={this.props.height}>
              	<DataSeries data={this.props.data} size={[150, 90]} text={this.props.text}/>
            </Chart>
		);
	}
});

export default DonutChart;
