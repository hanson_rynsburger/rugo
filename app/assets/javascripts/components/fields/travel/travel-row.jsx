var React = require('react');
var Helper = require('../../../helper.js');

let TravelRow = React.createClass({
	render: function(){
		var data = {
			value: this.props.value,
			fields: this.props.fields,
			classes: Helper.convertToAssociativeArray(this.props.fields.properties.classes)
		};
		var value = this.props.value;
		
		return (
			<div className="block block-default list-block">
				<div className="block-body">
					<div className="task-detail-small">
						<span className={'circle-icon icon-'+ value.transportation +' icon-black'}></span>
						<h4 className="block-title">
							{value.from} <i className="pi pi-long-arrow-right"></i>
							{value.to} { value.status?<span className={'label label-'+value.status}>{value.status}</span>:null}
						</h4>
						<span className="list-desc">{value.from_date} to {value.to_date}</span>
						<div className="attributes">
							<span className="dot-separator">{data.classes[value.transportation_class]} Class</span>
							<span className="mr10">{value.round_trip?'Round Trip':'One Way'}</span>
							<div className="icons">
								<span>
				                	<i className={'pi pi-' + value.prefer_time_from}></i><span className="slash">/</span><i className={'pi pi-' + value.prefer_time_to}></i>
				              	</span>
				              	{value.car_rental?<i className="pi pi-car"></i>:null}
				              	{value.hotel?<i className="pi pi-hotel"></i>:null}
							</div>
						</div>
						<a href="javascript:void(0);" className="link-gray pull-right edit-label" onClick={this.props.onToggleEdit}><span>Edit</span></a>
					</div>
				</div>
			</div>
		);
	}
});

export default TravelRow;