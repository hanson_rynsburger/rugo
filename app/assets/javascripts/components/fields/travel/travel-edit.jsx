var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var Helper = require('../../../helper.js');
var DatePickerTemplate = require('../date-picker-template.js');
var SwitchControlTemplate = require('../switch-control-template.js');
var AlertActions = require("../../../actions/alert-action.js");

let TravelEdit = React.createClass({
	getInitialState: function(){
		return {
			visible: this.props.visible,
			status: this.props.value?this.props.value.status:''
		};
	},
	onBooked: function(){
		this.state.status = 'booked';
		this.onSave();
	},
	onSave: function(e){
		//validation
		var error = this.refs.form.validate();
		if (error.errors.length){
			//TODO make multiple alert div
			AlertActions.error(error.errors[0].message);
		}else{
			var value = this.refs.form.getValue();
			console.log(value);
			var obj = {
				from: value.from,
				to: value.to,
				from_date: value.from_date,
				prefer_time_from: value.prefer_time_from,
				to_date: value.to_date,
				prefer_time_to: value.prefer_time_to,
				round_trip: value.round_trip,
				car_rental: value.car_rental,
				hotel: value.hotel,
				transportation_class: value.transportation_class,
				transportation: value.transportation,
				memo: value.memo,
				status: this.state.status
			};
		    if (value) {
		      	this.props.onSave(this.props.id, obj);
		    }
		}
	},
	onDelete: function(e){
		this.props.onDelete(this.props.id);
	},
	render: function(){
		var _this = this;

		//define field types
		var TimeType = t.enums(
			{
				'morning' : 'Morning',
				'night' : 'Night'
			}
		);

		var ClassType = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.classes)
		);

		var TransportationType = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.transportations)
		);

		var Travel = t.struct({
			from: t.Str,
			from_date: t.Str,
			prefer_time_from: TimeType,
			to: t.Str,
			to_date: t.Str,
			prefer_time_to: TimeType,
			round_trip: t.maybe(t.Str),
			car_rental: t.maybe(t.Str),
			hotel: t.maybe(t.Str),
			memo: t.Str,
			transportation: TransportationType,
			transportation_class: ClassType
		});

		//layout template
		var TravelLayout = function(locals){
			return (
				<div>
					<div className="row">
						<div className="col-xs-3">
							{locals.inputs.from}
						</div>
						<div className="col-xs-3">
							{locals.inputs.from_date}
						</div>
						<div className="col-xs-3">
							{locals.inputs.prefer_time_from}
						</div>
					</div>
					<div className="row">
						<div className="col-xs-3">
							{locals.inputs.to}
						</div>
						<div className="col-xs-3">
							{locals.inputs.to_date}
						</div>
						<div className="col-xs-3">
							{locals.inputs.prefer_time_to}
						</div>
						<div className="col-xs-3 custom-checkbox">
							{locals.inputs.round_trip}
						</div>
					</div>
					<div className="row">
						<div className="col-xs-3">
							{locals.inputs.transportation}
						</div>
						<div className="col-xs-3">
							{locals.inputs.transportation_class}
						</div>
						<div className="col-xs-6 custom-checkbox">
							{locals.inputs.car_rental}
							{locals.inputs.hotel}
						</div>
					</div>
					<div className="row">
						<div className="col-xs-12">
							{locals.inputs.memo}
						</div>
					</div>
				</div>
			);
		};

		//t.comb options
		var options = {
			template: TravelLayout,
			fields: {
				from:{
					label: 'From'
				},
				to:{
					label: 'To'
				},
				prefer_time_from:{
					label: 'Time'
				},
				prefer_time_to: {
					label: 'To'
				},
				from_date: {
					label: 'Date',
					template: DatePickerTemplate
				},
				to_date: {
					label: 'Date',
					template: DatePickerTemplate
				},
				round_trip: {
					label: 'Round Trip',
					template: SwitchControlTemplate
				},
				transportation: {
					label: 'Transportation'
				},
				transportation_class: {
					label: 'Class'
				},
				car_rental: {
					label: 'Car Rental',
					config: {
						icon: <i className="pi pi-car"></i>
					},
					template: SwitchControlTemplate
				},
				hotel: {
					label:  'Hotel Rental',
					config: {
						icon: <i className="pi pi-hotel"></i>,
						className: 'ml10'
					},
					template: SwitchControlTemplate
				},
				memo: {
					label: 'Memo',
					type: 'textarea',
					config: {
						rows: 3
					}
				}
			}
		};

		return (
			<div className="block block-secondary">
				<div className="block-body">
					<form>
						<Form type={Travel} value={this.props.value} options={options} ref="form">
						</Form>
						<div className="form-group mb0">
							<div className="row">
								<div className="col-xs-12">
									<div className="pull-left">
										<a href="javascript:;" className="btn btn-sm btn-primary" onClick={this.onSave}>Save</a>
										{this.props.id != 'new'? <a href="javascript:;" className="btn btn-sm btn-success" onClick={this.onBooked}>Booked</a>: null}
										<a href="javascript:;" onClick={this.props.onToggleEdit} className="btn link-gray btn-link link-cancel">Cancel</a>
									</div>
									{this.props.id != 'new'? <div className="pull-right">
										<a className="btn btn-link link-gray btn-delete-entry" href="javascript:;" onClick={this.onDelete}>
											<i className="pi pi-trash-o text-danger"></i>Delete
										</a>
									</div>: null}
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
});

export default TravelEdit;
