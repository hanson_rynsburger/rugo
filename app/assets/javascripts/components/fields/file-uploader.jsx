var React = require('react');
var FileUploader = React.createClass({
	getInitialState: function(){
		return {
			filename: this.props.defaultText	
		};
	},
	onChange: function(e){
		var files = e.target.files;
		var filename = '';
		filename = _.chain(files)
			.map(function(f){
				return f.name;
			})
			.value()
			.join(', ');

		if (this.props.onFileChange){
			this.props.onFileChange(files);
		}

		this.setState({
			filename: filename
		});
	},
	render: function(){
		return (
			<div className="input-file">
				<span className="custom-file-name">{this.state.filename}</span>
				<span className="btn btn-sm btn-upload">Upload
					<input type="file" className="upload" onChange={this.onChange} />
				</span>
			</div>
		);
	}
});

export default FileUploader;