var React = require('react');
var DatePicker = require('./date-picker.jsx');

var DatePickerTemplate = function datePickerTemplate(locals) {
	function onChangeDate(date) {
		locals.onChange(date);
	}
	console.log(locals.placeholder);
	return (
		<div className="form-group">
			<label className="control-label">{locals.label}</label>
			<DatePicker value={locals.value} onChange={onChangeDate} placeholder={locals.placeholder} />
			{locals.help?(<span className="help-block" role="tooltip">{locals.help}</span>):null}
		</div>
	);
};

export default DatePickerTemplate;