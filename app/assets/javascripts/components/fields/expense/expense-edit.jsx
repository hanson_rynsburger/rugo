var React = require('react');
var Reflux = require('reflux');
var t = require('tcomb-form');
var Form = t.form.Form;
var Helper = require('../../../helper.js');
var DatePickerTemplate = require('../date-picker-template.js');
var FileUploader = require('../file-uploader.jsx');
var AlertActions = require("../../../actions/alert-action.js");
var CurrencyRateStore = require('../../../stores/currency-rate-store.js');
var CurrencyRateActions = require('../../../actions/currency-rate-action.js');

let ExpenseEdit = React.createClass({
	mixins: [Reflux.listenTo(CurrencyRateStore, 'onGetRate')],
	files: [],
	getInitialState: function(){
		return {
			value: this.props.value,
			rates: {}
		};
	},
	getDefaultProps: function(){
		return {
			value: {}
		};
	},
	onGetRate: function(from, to, rate){
		if (from == to){
			this.state.rates[from + '_' + to] = 1.0000;
		}else{
			this.state.rates[from + '_' + to] = rate;	
		}
		this.setCurrencyRate(this.state.value.currency);
	},
	onFileChange: function(files){
		this.files = files;
	},
	onSave: function(e){
		//validation
		var error = this.refs.form.validate();
		if (error.errors.length){
			//TODO make multiple alert div
			AlertActions.error(error.errors[0].message);
		}else{
			var value = this.refs.form.getValue();
			var obj = {
				expense_type: value.expense_type,
				purpose: value.purpose,
				payment: value.payment,
				currency: value.currency,
				date: value.date,
				memo: value.memo,
				rate: value.rate,
				amount: value.amount,
				status: '',
				files: this.files
			};
		    if (value) {
		      	this.props.onSave(this.props.id, obj);
		    }
		}
	},
	onDelete: function(e){
		this.props.onDelete(this.props.id);
	},
	setCurrencyRate: function(currency){
		if (!this.state.rates[currency + '_sgd']){
			CurrencyRateActions.get(currency, 'sgd');
		}else{
			this.state.value.rate = this.state.rates[currency + '_sgd']; //we assume default currency as sgd
			//TODO use sgd as global variable
			this.setState({
				value: this.state.value
			});
		}
		
	},
	onChangeForm: function(value){
		if (value.currency && value.currency!=this.state.value.currency){
			this.state.value = value;
			this.setCurrencyRate(value.currency);
		}
		return true;
	},
	render: function(){
		var _this = this;
		//TODO get rid of convert to associative array by doing it at backend
		//TODO make file attachement field (ajax? or form submit?)
		//define field types
		var ExpenseType = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.categories)
		);

		var Purpose = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.purposes)
		);

		var PaymentType = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.payments)
		);

		var Currency = t.enums(
			Helper.convertToAssociativeArray(this.props.fields.properties.currencies)
		);

		var Expense = t.struct({
			expense_type: ExpenseType,
			purpose: Purpose,
			payment: PaymentType,
			currency: Currency,
			date: t.Str,
			memo: t.Str,
			rate: t.Num,
			amount: t.Num
		});

		var fileText = _.map(this.props.value.file, function(d){
			return d.original_filename;
		}).join(',');

		//layout template
		var ExpenseLayout = function(locals){
			return (
				<div>
					<div className="col-xs-3">
						{locals.inputs.date}
					</div>
					<div className="col-xs-3">
						{locals.inputs.expense_type}
					</div>
					<div className="col-xs-3">
						{locals.inputs.purpose}
					</div>
					<div className="col-xs-3">
						{locals.inputs.payment}
					</div>
					<div className="col-xs-2">
						{locals.inputs.currency}
					</div>
					<div className="col-xs-2">
						{locals.inputs.rate}
					</div>
					<div className="col-xs-2">
						{locals.inputs.amount}
					</div>
					<div className="col-xs-6">
						<div className="form-group">
							<label>Attachment</label>
							<FileUploader onFileChange={_this.onFileChange} defaultText={fileText} />
						</div>
					</div>
					<div className="col-xs-12">
						{locals.inputs.memo}
					</div>
				</div>
			);
		};

		//t.comb options
		var options = {
			template: ExpenseLayout,
			fields: {
				expense_type:{
					label: 'Expense Type'
				},
				purpose: {
					label: 'Purpose'
				},
				payment: {
					label: 'Payment Type'
				},
				currency: {
					label: 'Currency'
				},
				date: {
					label: 'Date',
					template: DatePickerTemplate
				},
				rate: {
					label: 'Rate'
				},
				amount: {
					label: 'Amount'
				},
				memo: {
					label: 'Memo',
					type: 'textarea',
					config: {
						rows: 2
					}
				}
			}
		};

		return (
			<tr className="form-row-edit">
				<td colSpan="5">
					<div className="block block-secondary">
						<form>
							<Form type={Expense} value={this.state.value} options={options} ref="form" onChange={this.onChangeForm}>
							</Form>
							<div className="form-group mb0">
								<div className="col-xs-12">
									<div className="pull-left">
										<a href="javascript:;" className="btn btn-sm btn-primary" onClick={this.onSave}>Save</a>
										{this.props.id != 'new'? <button className="btn btn-sm btn-success">Paid</button>: null}
										<a href="javascript:;" onClick={this.props.onToggleEdit} className="btn link-gray btn-link link-cancel">Cancel</a>
									</div>
									{this.props.id != 'new'? <div className="pull-right">
										<a className="btn btn-link link-gray btn-delete-entry" href="javascript:;" onClick={this.onDelete}>
											<i className="pi pi-trash-o text-danger"></i>Delete Entry
										</a>
									</div>: null}

								</div>
							</div>
						</form>
					</div>
				</td>
			</tr>
		);
	}
});

export default ExpenseEdit;
