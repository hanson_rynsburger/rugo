var React = require('react');
var Helper = require('../../../helper.js');

let ExpenseRow = React.createClass({
	formatData: function(value){
		if (typeof value.amount !== 'undefined'){
			value.amount = Number(value.amount).toFixed(2);
		}
		if (typeof value.rate !== 'undefined'){
			value.rate = Number(value.rate).toFixed(5);
		}
		return value;
	},
	componentDidMount: function(state){
		// this.state.value = state.value;
	},
	render: function(){
		var data = {
			value: this.formatData(this.props.value),
			fields: this.props.fields,
			categories: Helper.convertToAssociativeArray(this.props.fields.properties.categories)
		};
		//TODO use proper icon for status
		return (
			<tr className="edit-task" onClick={this.props.onToggleEdit}>
				<td><i className="pi pi-check-circle-o"></i></td>
				<td>{data.value.date}</td>
				<td>{data.categories[data.value.expense_type]}</td>
				<td>{data.value.memo}</td>
				<td className="amount">
					<span>${(data.value.amount * data.value.rate).toFixed(2)}</span><br/>
					<span className="text-muted">${data.value.amount}@{data.value.rate}</span>
					<a className="td-edit" href="javascript:void(0)"></a>
				</td>
			</tr>
		);
	}
});

export default ExpenseRow;