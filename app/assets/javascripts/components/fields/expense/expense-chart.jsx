var React = require('react');
var DonutChart = require('../donut-chart.jsx');
var ColorStore = require('../../../stores/color-store.js');
var Helper = require('../../../helper.js');

let ExpenseChart = React.createClass({
	render: function(){
		var _this = this;
		var colors = ColorStore.colors;

		//categorize data
		var data = _.map(_.groupBy(this.props.data, 'expense_type'), function(c, key){
			var sum = _.reduce(c, function(s, d){
				return s + d.rate*d.amount;
			}, 0);
			return {
				value: sum,
				key: key
			}
		});

		var total = _.reduce(data, function(sum, d){
			return sum + d.value;
		}, 0).toFixed(2);


		//forming up data for donut chart
		var donutData = _.map(data, function(d, i){
			var p = d.value/total*100;
			return {
				value: p,
				text: p.toFixed(2) + '%'
			};
		});

		var tbody = _.map(data, function(d, i){
			return (
				<tr>
					<td>
						<span className="color-circle" style={ {'background-color': colors(i)} }></span>
						<span>{_this.props.categories[d.key]}</span>
					</td>
					<td>
						{d.value.toFixed(2)}
					</td>
				</tr>
			)
		});	

		return (
			<div>
				<div className="chart-view-left">
					<table className="table table-bordered">
						<thead>
							<tr>
								<th rowSpan="2">Expense Type Summary</th>
								<th>Total Spent</th>
							</tr>
							<tr>
								<th>{total}</th>
							</tr>
						</thead>
						<tbody>
							{tbody}
						</tbody>
					</table>
				</div>
				<div className="chart-view-right">
					<DonutChart data={donutData} width="300" height="300" text={[total, 'TOTAL SPENT']} />
				</div>
				<div className="clearfix">
				</div>
			</div>
		);
	}
});

export default ExpenseChart;
