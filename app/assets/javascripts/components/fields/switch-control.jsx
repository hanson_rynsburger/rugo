var React = require('react');
var SwitchControl = React.createClass({
	getInitialState: function(){
 		return {
 			checked: (this.props.value == 1)
 		};
	},
	onChange: function(e){
		this.setState({
			checked: !this.state.checked
		});
		if (this.props.onChange){
			this.props.onChange(this.state.checked?'':'1');
		}
	},
	render: function(){
		return (
			<label className={this.props.className}>
                <input type="checkbox" ref="input" value="1" onChange={this.onChange} checked={this.state.checked} />
                <span className="input-checkbox"></span>
                {this.props.icon}
                {this.props.label}
            </label>
		);
	}
});

export default SwitchControl;