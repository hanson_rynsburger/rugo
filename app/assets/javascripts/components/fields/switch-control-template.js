var React = require('react');
var SwitchControl = require('./switch-control.jsx');

var SwitchControlTemplate = function (locals) {
	function onChange(val) {
		locals.onChange(val);
	}
	return (
		<SwitchControl value={locals.value} label={locals.label} className={locals.config.className} icon={locals.config.icon} onChange={onChange} />
	);
};

export default SwitchControlTemplate;