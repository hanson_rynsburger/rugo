var React = require('react');
var Select2 = require('./select2.jsx');

var Select2Template = function (locals) {
	function onChange(val) {
		locals.onChange(val);
	}
	return (
		<Select2 options={locals.options} value={locals.value} label={locals.label} onChange={onChange}></Select2>
	);
};

export default Select2Template;