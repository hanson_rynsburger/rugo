var React = require('react');
var DatePicker = React.createClass({
	getInitialState: function(){
		return {
			format: this.props.format?this.props.format:'MM/DD/YYYY'
		}
	},
	componentDidMount: function(){
		var $el = $(this.getDOMNode());
		var _this = this;
		$el.datetimepicker({
			format: this.state.format
			// debug: true
		}).on("dp.change", function (e){
			_this.props.onChange(e.date.format(_this.state.format));
		});

		//show datetimepicker on focus
		$(this.refs.input.getDOMNode()).on('focus', function(e){
			$el.data('DateTimePicker').show();
		});
	},
	render: function(){
		return (
			<div className="input-group date">
				<div className="input-icon">
					<span className="input-group-addon"><i className="pi pi-calendar"></i></span> <input ref="input" type="text" className="form-control" placeholder={this.props.placeholder} defaultValue={this.props.value} />
				</div>
			</div>
		);
	}
});

export default DatePicker;