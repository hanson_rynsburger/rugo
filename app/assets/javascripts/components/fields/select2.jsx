var React = require('react');
var Select2 = React.createClass({
	componentDidMount: function(){
		var _this = this;
		$(this.refs.select.getDOMNode())
			.select2()
			.on('change', function(e){
				_this.props.onChange($(this).val());
			});
	},
	render: function(){
		var options = _.map(this.props.options, function(d){
			return (<option value={d.value}>{d.text}</option>);
		});
		return (
			<div>
				<label className="control-label">{this.props.label}</label>
				<select ref="select" value={this.props.value}>
					{options}
				</select>
			</div>
		);
	}
});

export default Select2;