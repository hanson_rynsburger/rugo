var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var DatePickerTemplate = require('./fields/date-picker-template.js');
var AlertActions = require("../actions/alert-action.js");
var FormActions = require("../actions/form-action.js");

var FormHeaderEdit = React.createClass({
	getInitialState: function(){
		return {
			value: {
				id: this.props.data.id,
				status: this.props.data.status,
				description: this.props.data.description,
				start_at: this.props.data.start_at,
				end_at: this.props.data.end_at,
				acceptance_check: false
			}
		}
	},
	onDelete: function(){
		if (confirm('Do you really want to delete this form?')){
			FormActions.delete(this.state.value.id);
		}
	},
	onApproval: function(){
		this.state.value.status = 'pending';
		this.onSave();
	},
	onDraft: function(){
		this.state.value.status = 'draft';
		this.onSave();
	},
	onSave: function(){
		var error = this.refs.form.validate();
		if (error.errors.length){
			//TODO make multiple alert div
			AlertActions.error(error.errors[0].message);
		}else{
			var value = this.refs.form.getValue();
		    if (value) {
		    	var vObj = {};
		    	for (var i in value){
		    		this.state.value[i] = vObj[i] = value[i];
		    	}
		    	vObj['id'] = this.state.value.id;
		    	vObj['status'] = this.state.value.status;
		      	FormActions.update(vObj);
		    }
		}
	},
	render: function(){
		var _this = this;

		//TODO: remove userlist below and implement ajax for fetching
		var UserType = t.enums(this.props.employees);

		var editStruct = {
			description: t.Str,
			acceptance_check: t.Bool
		};

		if (this.props.show_on_calendar){
			editStruct = _.extend(editStruct, {
				start_at: t.Str,
				end_at: t.Str
			});
		}

		var FormHeaderEditType = t.struct(editStruct);

		var FormHeaderEditTemplate = function(locals){
			return (
				<div>
					<div className="form-group">
						<div className="col-xs-12">
							{locals.inputs.description}
						</div>
					</div>
					{_this.props.show_on_calendar?
						(
						<div className="form-group">
							<div className="col-xs-6">
								<div className="row">
									<div className="col-xs-6">
										{locals.inputs.start_at}
									</div>
									<div className="col-xs-6">
										{locals.inputs.end_at}
									</div>
								</div>
							</div>
						</div>
						)
						:null
					}
					<div className="form-group">
						<div className="col-xs-12">
							{locals.inputs.acceptance_check}
						</div>
					</div>
					<div className="form-group">
						<div className="col-xs-12">
							<div className="pull-left">
								<a href="javascript:void(0);" className="btn btn-sm btn-primary" onClick={_this.onApproval}>Submit for Approval</a>
								<a href="javascript:void(0);" className="btn btn-sm btn-orange"  onClick={_this.onDraft}>Save as Draft</a>
								<a href="javascript:void(0);" className="btn btn-link link-gray" onClick={_this.props.hideEdit}>Cancel</a>
							</div>
							<div className="pull-right">
								<a href="javascript:void(0);" className="btn btn-link link-gray" onClick={_this.onDelete}><i className="pi pi-trash-o text-danger"></i>Delete Request</a>
							</div>
						</div>
					</div>
				</div>
			);
		};

		var formHeaderEditOptions = {
			template: FormHeaderEditTemplate,
			fields: {
				description: {
					label: 'Description',
					type: 'textarea',
					config: {
						rows: 2
					}
				},
				start_at: {
					label: 'From',
					template: DatePickerTemplate
				},
				end_at: {
					label: 'To',
					template: DatePickerTemplate
				},
				acceptance_check: {
					label: 'I declare that the above expenses have been reasonably and actually incurred by me in fulfilling my responsibilities as an employee of Fitch.'
				}
			}
		};
		return (
			<div className="block block-secondary">
				<div className="block-body">
					<Form ref="form" type={FormHeaderEditType} value={this.state.value} options={formHeaderEditOptions} />
				</div>
			</div>
		);
	}
});

let FormHeader = React.createClass({
	getInitialState: function(){
		return {
			editVisible: false
		};
	},
	showEdit: function(e){
		this.setState({
			editVisible: true
		});
	},
	hideEdit: function(e){
		this.setState({
			editVisible: false
		});
	},
	render: function(){
		var author, assignee, user, edit;
		if (typeof this.props.data.author !== 'undefined'){
			author = <span><i class="pi pi-person"></i> <span>Submitted By</span> <a href="#">{this.props.data.author.name}</a></span>;
		}
		if (typeof this.props.data.assignee !== 'undefined'){
			assignee = <span><i class="pi pi-person"></i> <span>Assigned By</span> <a href="#">{this.props.data.assignee.name}</a></span>;
		}
		if (typeof this.props.data.user !== 'undefined'){
			user = <span><i class="pi pi-person"></i> <span>Request For</span> <a href="#">{this.props.data.user.name}</a></span>;
		}
		if (this.state.editVisible){
			edit = <FormHeaderEdit hideEdit={this.hideEdit} data={this.props.data} employees={this.props.employees} />
		}
		return (
			<div className="pages tasks">
				<div className="heading">
					<span className={"circle-icon fa "+this.props.data.form.icon}></span>
					<a className="link-gray pull-right link-edit" href="javascript:void(0)" onClick={this.showEdit}><i className="pi pi-edit"></i> <span>Edit</span></a>
					<h2>{this.props.data.form.name}</h2>
					<span className="task-detail-description">{this.props.data.description}</span><br/>
					{
						this.props.show_on_calendar?
						(<span className="task-detail-duration">{moment(this.props.data.start_at).format('MM/DD/YYYY')} <span>To</span> {moment(this.props.data.end_at).format('MM/DD/YYYY')}</span>)
						:null
					}
      				<br/>
      				<h4 className="status-label"><span className={"label label-" + this.props.data.status}>{this.props.data.status}</span></h4>

      				<div className="attributes">
      					<span><i className="pi pi-clock"></i> {moment(this.props.data.updated_at).fromNow()}</span>
      					{author}
      					{assignee}
      					{user}
      				</div>
				</div>
				{edit}
			</div>
		);
	}
});

export default FormHeader;
