var React = require('react');
var t = require('tcomb-form');
var Form = t.form.Form;
var SimpleFormMixin = require('../mixins/simple-form-mixin.js');

let DropdownField = React.createClass({
	mixins: [SimpleFormMixin],
  render: function(){

  	var struct = _.object(
  		_.map(this.props.fields.properties.select_options, function(d){
  			return d.value;
  		}),
  		_.map(this.props.fields.properties.select_options, function(d){
  			return d.label;
  		})
  	);

  	var DropdownType = t.enums(struct);

  	var DropdownForm = t.struct({
			value: (this.props.fields.required?DropdownType:t.maybe(DropdownType))
		});

		var options = {
		  fields: {
		    value: {
		    	label: this.props.fields.properties.label + (this.props.fields.required?' *':''),
		      help: this.props.fields.properties.hint,
		      nullOption: {value: '', text: this.props.fields.properties.prompt}
		    }
		  }
		};

		var values = {
			value: this.props.value
		};
		
    return (
      	<Form type={DropdownForm} value={values} options={options} ref="form"></Form>
    );
  }
});
export default DropdownField;
