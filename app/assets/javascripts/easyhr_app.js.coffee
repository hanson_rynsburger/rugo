@app = angular.module("easyhrApp", [
  "restangular",
  "ngSanitize",
  "ngResource",
  "angularMoment",
  "mgcrea.ngStrap",
  "ngTable",
  "ngRoute",
  "templates",
  # "monospaced.elastic",
  "easyhr.taskController",
  "easyhr.timeoffController",
  "easyhr.userController",
  "easyhr.policyController",
  "easyhr.formBuildersController",
  "easyhr.formsController",
  "easyhr.teamController",
  "easyhr.companyController",
  "easyhr.employeeController",
  "easyhr.locationController",
  "easyhr.calendarController",
  "easyhr.planAndBillingController",
  "angular-flash.service",
  "angular-flash.flash-alert-directive"
])

app.config ([
  "$httpProvider",
  "RestangularProvider",
  "flashProvider"
  ($httpProvider, $RestangularProvider, $flashProvider) ->

    $flashProvider.successClassnames.push('alert-success');
    $flashProvider.errorClassnames.push('alert-danger');

    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = angular.element("meta[name=\"csrf-token\"]").attr("content")
    $RestangularProvider.setRequestSuffix ".json"
    $RestangularProvider.setErrorInterceptor (error) ->
      console.log error.status + ":" + error.data.detail
      return
])

app.filter 'to_trusted', [
  '$sce'
  ($sce) ->
    (text) ->
      $sce.trustAsHtml text
]

app.run ['$rootScope', ($rootScope) ->
  $rootScope.translate = (s, params = {}) ->
    return I18n.t(s, params)
  return
]

$(document).on('ready page:load', ->
  angular.bootstrap(document.body, ['easyhrApp'])
)



app.filter "overdue", ->
  (input) ->
    out = ""
    d = new Date(input)
    out = "overdue"  if d < Date.now()
    out
