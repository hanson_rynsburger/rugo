# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

policyControllers = angular.module("easyhr.policyController", [
  "restangular"
])

policyControllers.controller "PolicyDetailCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http",
  "$location",
  "ComboboxService",
  "$compile"
  ($scope, Restangular, $timeout, $http, $location, ComboboxService, $compile) ->
    $scope.I18n = I18n
    $scope.timeoff_policy = timeoff_policy

    # tabs
    $scope.activeTab ||= 0

    $scope.accrue_types = TIMEOFF_POLICY_TYPES
    $scope.accrue_rates = TIMEOFF_POLICY_RATES
    $scope.anniversaries = TIMEOFF_POLICY_ANNIVERSARIES
    $scope.is_ptos = TIMEOFF_POLICY_PTOS
    $scope.hours_per_day = HOURS_PER_DAY
    $scope.command =
      showExpireDay: $scope.timeoff_policy.carry_over && $scope.timeoff_policy.carry_over_expire_on

    $scope.carryoverCheck = (val)->
      $scope.timeoff_policy.carry_over = val
    $scope.showExpire = (val)->
      $scope.command.showExpireDay = val
    $scope.showNegative = (val) ->
      $scope.timeoff_policy.allow_negative = val


    $scope.addNewTimeoffType = ()->
      partial = timeoff_type_partial
      unique_id = new Date().valueOf()
      partial = partial.replace(/TIMEOFF_TYPE_IDX/g,unique_id)
      $('#timeoff-types-form').append(partial)
      $compile(angular.element('#timeoff-type-item-' + unique_id))($scope)
      return

    $scope.removeTimeoffType = (idx)->
      $('#timeoff-type-item-' + idx).remove()
      return

    $scope.addTenture = ()->
      partial = tenture_partial
      unique_id = new Date().valueOf()
      partial = partial.replace(/TENTURE_IDX/g,unique_id)
      $('#tentures-form').append(partial)
      $compile(angular.element('#tenture-item-' + unique_id))($scope)

      # trigger calculate tentures
      $timeout ->
        $scope.calculateTentures()

      return

    $scope.removeTenture = (idx)->
      $('#tenture-item-' + idx).remove()
      return

    $scope.updateChanges = ()->
      $timeout ->
        $scope.$digest() unless $scope.$$phase is '$digest'
      , 250

    $scope.calculateTentures = ()->
      extra_days = $scope.timeoff_policy.base_vacation_days

      $('#tentures-form').find('.tenture-item').each (idx, item)->
        $tenture    = $(item)
        extra_days  = extra_days + parseInt($tenture.find('.extra-days').val())
        $tenture.find('.form-control-static').html( I18n.t('js.common.days', { count: extra_days }) )

      $scope.updateChanges()
      return


    # will running when page has been loaded 100%
    #
    $timeout ->
      $('.time-select').change ->
        day = $(this).attr('day')
        startHour = parseInt($("select[name='timeoff_policy[work_hours]["+day+"][start_at][(4i)]']").val())
        startMinute = parseInt($("select[name='timeoff_policy[work_hours]["+day+"][start_at][(5i)]']").val())

        endHour = parseInt($("select[name='timeoff_policy[work_hours]["+day+"][end_at][(4i)]']").val())
        endMinute = parseInt($("select[name='timeoff_policy[work_hours]["+day+"][end_at][(5i)]']").val())
        hours = (((endHour - startHour) * 60 + (endMinute - startMinute)) / 60).toFixed(2)
        hours = parseInt(hours) if (hours % 1 is 0)
        $('#' + day + '-hours').text(hours)

      $('.time-select').trigger 'change'

    , 500

    return
]