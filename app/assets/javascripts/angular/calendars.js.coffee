# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

calendarControllers = angular.module("easyhr.calendarController", ["restangular", "ui.calendar"])

calendarControllers.controller "CalendarIndexCtrl", ["$scope", "Restangular", "$compile", "uiCalendarConfig", "$timeout", ($scope, Restangular,$compile,uiCalendarConfig, $timeout) ->
  date = new Date()
  d = date.getDate()
  m = date.getMonth()
  y = date.getFullYear()
  $scope.changeTo = "English"

  $scope.allWorkdates   ||= []
  $scope.allHolidates   ||= []
  $scope.allTimeoffs    ||= []
  $scope.allTasks       ||= []

  $scope.restHrdates = Restangular.all('calendars')
  $scope.restTimeoffs = Restangular.all('timeoffs')
  $scope.restTasks =  Restangular.all('tasks')
  $scope.alertMessage
  $scope.keyword

  # alert on eventClick
  $scope.alertOnEventClick = (hrdate, jsEvent, view) ->
    $scope.alertMessage = (hrdate.title + " was clicked ")
    console.log $scope.alertMessage
    return


  # alert on Drop
  $scope.alertOnDrop = (event, delta, revertFunc, jsEvent, ui, view) ->
    $scope.alertMessage = ("Event Droped to make dayDelta " + delta)
    return


  # alert on Resize
  $scope.alertOnResize = (event, delta, revertFunc, jsEvent, ui, view) ->
    $scope.alertMessage = ("Event Resized to make dayDelta " + delta)
    return


  # add and removes an event source of choice
  $scope.addRemoveEventSource = (sources, source) ->
    canAdd = 0
    angular.forEach sources, (value, key) ->
      if sources[key] is source
        sources.splice key, 1
        canAdd = 1
      return

    sources.push source  if canAdd is 0
    return


  # add custom event
  $scope.addEvent = ->
    $scope.events.push
      title: "Open Sesame"
      start: new Date(y, m, 28)
      end: new Date(y, m, 29)
      className: ["openSesame"]

    return


  # remove event
  $scope.remove = (index) ->
    $scope.events.splice index, 1


  # Change View
  $scope.changeView = (view, calendar) ->
    uiCalendarConfig.calendars[calendar].fullCalendar "changeView", view


  # Change View
  $scope.renderCalender = (calendar) ->
    uiCalendarConfig.calendars[calendar].fullCalendar "render"  if uiCalendarConfig.calendars[calendar]


  # Render Tooltip
  $scope.eventRender = (item, element, view) ->
    if item.type == 'hrdate'
      element.attr
        tooltip: item.title
        "tooltip-append-to-body": true
        "bs-modal": "modal"
        "data-container": "body"
        "data-animation": "am-fade-and-slide-top"
        "data-content-template": Routes.edit_company_calendar_path(item._id, modal: true)

    else if item.type == 'timeoff'
      element.attr
        tooltip: item.title
        "tooltip-append-to-body": true
        href: Routes.company_timeoff_path(item.id)

    $compile(element) $scope


  $scope.refetchEvents = ()->
    uiCalendarConfig.calendars.calendar.fullCalendar('refetchEvents')


  $scope.hrdateSources = [
    {
      events: (start, end, timezone, callback)->
        $scope.allWorkdates = []
        $scope.restHrdates.getList(day_type: 'workday', keyword: $scope.keyword).then((data)->
          angular.forEach data, (value, key)->
            start_date = new Date(value.start + " 00:00")
            end_date   =  if value.end != undefined && (value.end != null)
                            new Date(value.end + " 00:00")

            $scope.allWorkdates.push({ type: 'hrdate', id: value.id, title: value.name, start: start_date, end: end_date, allDay: true, repeat: value.repeat, day_type: value.day_type })

          # run callback
          callback($scope.allWorkdates)
        )
      color: '#3a87ad'
      textColor: 'black'
    }
    {
      events: (start, end, timezone, callback)->
        $scope.allHolidates = []
        $scope.restHrdates.getList(day_type: 'holiday', keyword: $scope.keyword).then((data)->
          angular.forEach data, (value, key)->
            start_date = new Date(value.start + " 00:00")
            end_date   =  if value.end != undefined && (value.end != null)
                            new Date(value.end + " 00:00")

            $scope.allHolidates.push({ type: 'hrdate', id: value.id, title: value.name, start: start_date, end: end_date, allDay: true, repeat: value.repeat, day_type: value.day_type })

          # run callback
          callback($scope.allHolidates)
        )
      color: '#1493d9'
      textColor: 'black'
    }
    {
      events: (start, end, timezone, callback)->
        $scope.allTimeoffs = []
        $scope.restTimeoffs.getList().then((data)->
          angular.forEach data, (value, key)->
            start_date = new Date(value.start_at)
            end_date   = new Date(value.end_at)

            $scope.allTimeoffs.push({ type: 'timeoff', id: value.id, title: value.name, start: start_date, end: end_date, allDay: false, repeat: false, status: value.status })

          # run callback
          callback($scope.allTimeoffs)
        )
      color: '#4b627c'
      textColor: 'black'
    }
    {
      events: (start, end, timezone, callback)->
        $scope.allTasks = []
        $scope.restTasks.customGET("all", { show_on_calendar: true }).then((data)->
          angular.forEach data, (value, key)->

            # start and end date
            start_time  = value.start_at
            end_time    = value.end_at

            $scope.allTasks.push({ type: 'task', id: value.id, title: value.form.name, start: start_time, end: end_time, allDay: false, repeat: false, status: value.status })

          # run callback
          callback($scope.allTasks)
        )
      color: '#cc9900'
      textColor: 'black'
    }
  ]


  # config object
  $scope.uiConfig = calendar:
    editable: true
    header:
      left: "prev,next today"
      center: "title"
      right: "month,agendaWeek,agendaDay"
    businessHours:
      start: '09:00'
      end: '17:00'
      dow: [1, 2, 3, 4, 5]
    eventClick:   $scope.alertOnEventClick
    eventDrop:    $scope.alertOnDrop
    eventResize:  $scope.alertOnResize
    eventRender:  $scope.eventRender


  # end angular controller
  return
]




calendarControllers.controller "CalendarNewCtrl", ["$scope", "$timeout", ($scope, $timeout) ->
  $scope.hrdate

  $timeout(->
    console.log $scope.hrdate
    )
]
