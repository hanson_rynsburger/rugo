# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
taskControllers = angular.module("easyhr.taskController", [
  "restangular",
  "chart.js"
])

taskControllers.controller "TaskIndexCtrl", [
  "$scope"
  "Restangular"
  "ngTableParams"
  "$http"
  "TaskService"
  "flash"
  "$upload"
  ($scope, Restangular, ngTableParams, $http, TaskService, flash, $upload) ->
    $scope.I18n = I18n
    $scope.paginateArray = PAGINATE_ARRAY
    $scope.perPage = $scope.paginateArray[0]
    $scope.sortBy = ""
    $scope.typeList = null
    $scope.defaultType = "All"
    $scope.totalCount = 0
    $scope.countTimeoffTask = 0
    $scope.countTravelTask = 0
    $scope.countExpenseTask = 0
    $scope.fileUpload = []
    $scope.currentUser = currentUser

    $scope.activeTaskList = (param) ->
      $scope.activeAll = ''
      $scope.activeTimeoff = ''
      $scope.activeTravel = ''
      $scope.activeExpense = ''

      if param == TASKS_TYPE[0]
        $scope.activeAll = 'active'
      else if param == TASKS_TYPE[1]
        $scope.activeTimeoff = 'active'
      else if param == TASKS_TYPE[2]
        $scope.activeTravel = 'active'
      else
        $scope.activeExpense = 'active'

    $scope.getListTask = (type) ->
      $scope.defaultType = type

      $scope.activeTaskList(type)
      $scope.tableParamsAll = new ngTableParams(
        page: START_PAGE # show first page
        count: $scope.perPage # count per page
        sort_by: $scope.sortBy
        type: $scope.defaultType
      ,
        total: 0 # length of data
        getData: ($defer, params) ->
          TaskService.getListTask($scope, params, $defer)
      )

    $scope.getListTask($scope.defaultType)

    $scope.sortTask = (typeSort)->
      $scope.sortBy = typeSort
      $scope.tableParamsAll.reload()

    $scope.sub_types = listTitleTimeoff

    $scope.task = {}

    $scope.resetDirty = ->
      $scope.newTaskForm["start_at"].$dirty = false
      $scope.newTaskForm["end_at"].$dirty = false

    $scope.newTimeoffTask = ->
      $scope.task = {}
      $scope.taskType = 'TimeoffTask'
      $scope.defaultAssigneeAndUser()
      $scope.task.start_at = new Date(new Date().setHours(9, 0, 0))
      $scope.task.end_at = new Date(new Date().setHours(17, 0, 0))
      $scope.task.name = listTitleTimeoff[0]
      $scope.resetDirty()
      return

    $scope.newTravelTask = ->
      $scope.task = {}
      $scope.taskType = 'TravelTask'
      $scope.resetDirty()
      $scope.defaultAssigneeAndUser()
      return

    $scope.newExpenseTask = ->
      $scope.task = {}
      $scope.taskType = 'ExpenseTask'
      $scope.resetDirty()
      $scope.defaultAssigneeAndUser()
      return

    $scope.selectInit = (classSelect, object)->
      if $(classSelect).length
        tag = object
        $(classSelect).select2(
          ajax:
            url: Routes.list_company_employees_path()
            dataType: "json"
            data: (params) ->
              manager: params

            results: (data) ->
              results: $.map(data, (user, i) ->
                id: user.id
                text: user.name
              )
          initSelection: (element, callback) ->
            callback tag
        ).select2('data', tag)

    $scope.defaultAssigneeAndUser = ->
      $scope.selectInit(".select-assignee", { id: $scope.currentUser.id, text: $scope.currentUser.name })
      $scope.selectInit(".select-request", { id: $scope.currentUser.id, text: $scope.currentUser.name })

    $scope.defaultAssigneeAndUser()
    $scope.onFileSelect = ($files)->
      $scope.fileUpload = $files

      $scope.fileUploadName = $.map($scope.fileUpload, (file) ->
        file.name
      ).join ', '


    $scope.submitNewTask = (isValid) ->
      if isValid
        $scope.resetDirty()
        if $scope.taskType == 'TimeoffTask'
          $scope.task.assignee_id = $('#select-assignee-timeoff').val()
          $scope.task.user_id = $('#select-request-timeoff').val()
        else
          $scope.task.assignee_id = $('#select-assignee').val()
          $scope.task.user_id = $('#select-request').val()

        params = {
          task: $scope.task
          task_type: $scope.taskType
          format: 'json'
        }

        if $scope.fileUpload and $scope.fileUpload.length
          $scope.upload = $upload.upload(
            method: 'post'
            url: Routes.company_form_entries_path params
            fileFormDataName: 'files[]'
            file: $scope.fileUpload
            fields:
              parent_id: $scope.taskID
          ).progress((evt) ->
          ).success((data, status, headers, config) ->
            flash.success = data.message
            $scope.tableParamsAll.reload()
            $scope.taskType = ''
            $scope.task = {}
            $scope.fileUpload = []
            $scope.fileUploadName = null
          ).error( ->
            return
          )
        else
          $http(
            method: 'post'
            url: Routes.company_form_entries_path()
            data: params
          ).success (success) ->
            flash.success = success.message
            $scope.tableParamsAll.reload()
            $scope.taskType = ''
            $scope.task = {}
      else
        $scope.newTaskForm["start_at"].$dirty = true
        $scope.newTaskForm["end_at"].$dirty = true
      return

    $scope.cancelNewTask = ->
      $scope.taskType = ''
      $scope.task = {}
      $scope.resetDirty()
      $scope.selectAssignee()
      $scope.selectRequest()

]



taskControllers.controller "TaskDetailCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http"
  "ngTableParams"
  "TaskService"
  "flash"
  "$upload"
  ($scope, Restangular, $timeout, $http, ngTableParams, TaskService, flash, $upload) ->
    $scope.I18n = I18n
    $scope.importExpenseTask = false
    $scope.filterExpenseList = FILTER_EXPENSE_TASK
    $scope.sortBy = $scope.filterExpenseList[0].value

    $scope.paginateArray = PAGINATE_ARRAY
    $scope.perPage = $scope.paginateArray[0]
    $scope.taskID = currentTask.id
    $scope.hasEditParentTask = false
    $scope.showableActivities = []

    $scope.getListChildTask = ->
      $scope.tableTaskParams = new ngTableParams(
        page: START_PAGE # show first page
        count: $scope.perPage # count per page
        sort_by: $scope.sortBy
        id: $scope.taskID
      ,
        total: 0 # length of data
        getData: ($defer, params) ->
          TaskService.getListChildTask($scope, params, $defer)
      )

    $scope.getListChildTask()

    $scope.sortTask = ->
      $scope.tableTaskParams.reload()

    $scope.expenses = []
    $scope.expenseTypeList = EXPENSE_TYPE
    $scope.purposeList = PURPOSE
    $scope.paymentTypeList = PAYMENT_TYPE
    $scope.currencyList = CURRENCY

    $scope.hasEditing = false
    $scope.hasAddNew = false

    $scope.editTask = (task) ->
      $scope.editingTask = angular.copy task
      $scope.hasEditing = true

    $scope.cancelEditing = ->
      $scope.editingTask = {}
      $scope.hasEditing = false

    $scope.onFileSelect = ($files)->
      $scope.fileUpload = $files
      $scope.fileUploadName = $.map($scope.fileUpload, (file) ->
        file.name
      ).join ', '

    refreshAfterUploading = ->
      $scope.tableTaskParams.reload()
      $scope.fileUpload = []
      $scope.getShowableActivities(1)
      $scope.fileUploadName = null

    $scope.updateEditingTask = (task_id) ->
      $scope.taskEditID = task_id

      params = {
        task: $scope.editingTask
        format: 'json'
      }

      if $scope.fileUpload and $scope.fileUpload.length
        $scope.upload = $upload.upload(
          method: 'put'
          url: Routes.company_form_entry_path $scope.taskEditID, params
          fileFormDataName: 'files[]'
          file: $scope.fileUpload
        ).progress((evt) ->
        ).success((data, status, headers, config) ->
          flash.success = data.message
          $scope.editingTask = {}
          $scope.hasEditing = false
          refreshAfterUploading()
        ).error( ->
          return
        )
      else
        $http(
          method: 'put'
          url: Routes.company_form_entry_path $scope.taskEditID, params
        ).success (success) ->
          flash.success = success.message
          $scope.editingTask = {}
          $scope.hasEditing = false
          $scope.tableTaskParams.reload()

    $scope.newEntryManual = ->
      $scope.hasAddNew = true

    $scope.submitNewDetailTask = (new_task) ->
      $scope.newDetailExpenseTask = new_task

      params = {
        parent_id: $scope.taskID
        type: 'ExpenseTask'
        task: $scope.newDetailExpenseTask
        format: 'json'
      }

      if $scope.fileUpload and $scope.fileUpload.length
        $scope.upload = $upload.upload(
          method: 'post'
          url: Routes.company_form_entries_path params
          file: $scope.fileUpload
          fileFormDataName: 'files[]'
          fields:
            parent_id: $scope.taskID
        ).progress((evt) ->
        ).success((data, status, headers, config) ->
          flash.success = data.message
          $scope.newDetailExpenseTask = {}
          $scope.hasAddNew = false
          refreshAfterUploading()
        ).error( ->
          return
        )
      else
        $http(
          method: 'post'
          url: Routes.company_form_entries_path params
        ).success (success) ->
          flash.success = success.message
          $scope.newDetailExpenseTask = {}
          $scope.hasAddNew = false
          $scope.tableTaskParams.reload()


    $scope.cancelCreate = ->
      $scope.hasAddNew = false
      $scope.newDetailExpenseTask = {}

    $scope.deleteDetailTask = (task_id) ->
      if confirm I18n.t('js.messeage_confirm.confirm_delete_task')
        params = {
          format: 'json'
        }

        $http(
          method: 'delete'
          url: Routes.company_form_entry_path task_id, params
        ).success (success) ->
          flash.success = success.message
          $scope.editingTask = {}
          $scope.hasEditing = false
          $scope.tableTaskParams.reload()

    $scope.newEntryImport = ->
      $scope.importExpenseTask = true

    $scope.cancelImportExpenseTask = ->
      $scope.importExpenseTask = false

    $scope.verifyAndConfirmSection = false
    $scope.imported_expenses = []

    $scope.uploadExpenseFile = ->
      if $scope.data
        $scope.fileValid = /((.csv)|(.xls)|(.xlsx))$/.test($scope.file[0].name)
        if !$scope.fileValid
          flash.error = I18n.t('js.messeage_confirm.wrong_format')
          return
        else
          $scope.uploadFiled = $upload.upload(
            url: Routes.company_import_expense_file_tasks_path format: 'json'
            method: 'post'
            file: $scope.file
          ).success (data, status, header, config) ->
            if !data.success
              flash.error = data.message
              $scope.verifyAndConfirmSection = false
              $scope.importExpenseTask = true
            else
              $scope.verifyAndConfirmSection = true
              $scope.importExpenseTask = false
              $scope.imported_expenses = data.import_expense_list


    $scope.backUploadExpenseSection = ->
      $scope.imported_expenses = []
      $scope.verifyAndConfirmSection = false
      $scope.importExpenseTask = true

    $scope.cancelVerifyAndUploadSection = ->
      $scope.imported_expenses = []
      $scope.verifyAndConfirmSection = false

    $scope.confirmAndcreateExpenses = ->
      $http(
        method: 'post'
        url: Routes.company_save_imported_expenses_tasks_path format: 'json'
        data:
          expenses: $scope.imported_expenses
          parent_expense: $scope.taskID
      ).success (response) ->
        if !response.success
          flash.error = response.message
        else
          $scope.imported_expenses = []
          $scope.verifyAndConfirmSection = false
          $scope.importExpenseTask = false
          $scope.file = null
          flash.success = response.message
          $scope.tableTaskParams.reload()

    $scope.showEditDetail = false
    $scope.formNewTravelTask = false
    $scope.travelPreferTimes = TIME_TYPE
    $scope.transportationType = TRANSPORTATION_TYPE
    $scope.bookingClass = BOOKING_CLASS
    $scope.newChildTaskTravel = {}
    $scope.editChildTaskTravel = {}

    $scope.buttonNewChildTask = ->
      $scope.formNewTravelTask = true

    $scope.cancelCreateChildTravel = ->
      $scope.formNewTravelTask = false
      $scope.newChildTaskTravel = {}

    $scope.submitCreateNewChildTaskTravel = ->
      params = {
        parent_id: $scope.taskID
        type: 'TravelTask'
        format: 'json'
      }
      $http(
        method: 'post'
        url: Routes.company_form_entries_path params
        data:
          task: $scope.newChildTaskTravel
      ).success (success) ->
        $scope.newChildTaskTravel = {}
        flash.success = success.message
        $scope.formNewTravelTask = false
        $scope.tableTaskParams.reload()

    $scope.showEditChildTaskTravelDetail = (task) ->
      $scope.editingTravelTask = angular.copy task
      $scope.showEditDetail = task.id

    $scope.cancelEditingTravel = ->
      $scope.editingTravelTask = {}
      $scope.showEditDetail = false

    $scope.submitEditChildTaskTravel = (task) ->
      $scope.taskEditTravelID = task.id
      $scope.editChildTaskTravel = task
      params = {
        format: 'json'
      }

      $http(
        method: 'put'
        data:
          task: $scope.editChildTaskTravel
        url: Routes.company_form_entry_path $scope.taskEditTravelID, params
      ).success (success) ->
        flash.success = success.message
        $scope.editChildTaskTravel = {}
        $scope.showEditDetail = false
        $scope.tableTaskParams.reload()

    $scope.deleteTravelDetailTask = (task_id) ->
      if confirm I18n.t('js.messeage_confirm.confirm_delete_task')
        params = {
          format: 'json'
        }

        $http(
          method: 'delete'
          url: Routes.company_form_entry_path task_id, params
        ).success (success) ->
          flash.success = success.message
          $scope.editingTravelTask = {}
          $scope.showEditDetail = false
          $scope.tableTaskParams.reload()

    $scope.editParentTask = ->
      $scope.task = angular.copy currentTask
      $scope.hasEditParentTask = true

    $scope.task = currentTask
    $scope.sub_types = listTitleTimeoff

    $scope.selectInitEditTask = (classSelect, object)->
      if $(classSelect).length
        tag = object
        $(classSelect).select2(
          ajax:
            url: Routes.list_company_employees_path()
            dataType: "json"
            data: (params) ->
              manager: params

            results: (data) ->
              results: $.map(data, (user, i) ->
                id: user.id
                text: user.name
              )
          initSelection: (element, callback) ->
            callback tag
        ).select2('data', tag)

    $scope.selectInitEditTask(".select-assignee", { id: $scope.task.assignee.id, text: $scope.task.assignee.name })
    $scope.selectInitEditTask(".select-request", { id: $scope.task.user.id, text: $scope.task.user.name })

    $scope.resetEditTaskDirty = ->
      $scope.editTaskForm["start_at"].$dirty = false
      $scope.editTaskForm["end_at"].$dirty = false

    $scope.submitEditTask = (isValid)->
      if isValid
        $scope.resetEditTaskDirty()

        $scope.task.assignee_id = $('#select-assignee').val()
        $scope.task.user_id = $('#select-request').val()

        $http(
          method: 'put'
          data:
            task: $scope.task
          url: Routes.company_form_entry_path $scope.task.id, format: 'json'
        ).success (success) ->
          window.location = Routes.company_form_entries_path()
          flash.success = success.message
      else
        $scope.editTaskForm["start_at"].$dirty = true
        $scope.editTaskForm["end_at"].$dirty = true

    $scope.onFileSelect = ($files)->
      $scope.fileUpload = $files
      $scope.fileUploadName = $.map($scope.fileUpload, (file) ->
        file.name
      ).join ', '

    $scope.submitEditTimeoffTask = (isValid)->
      if isValid
        $scope.resetEditTaskDirty()
        $scope.task.assignee_id = $('#select-assignee-timeoff').val()
        $scope.task.user_id = $('#select-request-timeoff').val()

        if $scope.fileUpload and $scope.fileUpload.length
          $scope.upload = $upload.upload(
            method: 'put'
            url: Routes.company_form_entry_path $scope.task.id, { task: $scope.task, format: 'json' }
            fileFormDataName: 'files[]'
            file: $scope.fileUpload
          ).progress((evt) ->
          ).success((data, status, headers, config) ->
            $scope.fileUpload = []
            window.location = Routes.company_form_entries_path()
            flash.success = data.message
          ).error( ->
            return
          )
        else
          $http(
            method: 'put'
            data:
              task: $scope.task
            url: Routes.company_form_entry_path $scope.task.id, format: 'json'
          ).success (success) ->
            window.location = Routes.company_form_entries_path()
            flash.success = success.message

      else
        $scope.editTaskForm["start_at"].$dirty = true
        $scope.editTaskForm["end_at"].$dirty = true

    $scope.cancelEditTask = ->
      $scope.task = currentTask
      $scope.hasEditParentTask = false

    $scope.approveTask = ->
      $http(
        method: 'post'
        data:
          id: $scope.task.id
        url: Routes.approve_task_company_form_entries_path format: 'json'
      ).success (success) ->
        flash.success = success.message
        window.location = Routes.company_form_entries_path()

    $scope.paidExpenseDetail = (task_id) ->
      $http(
        method: 'post'
        data:
          id: task_id
        url: Routes.approve_task_company_form_entries_path format: 'json'
      ).success (success) ->
        flash.success = success.message
        $scope.editingTask = {}
        $scope.hasEditing = false
        $scope.tableTaskParams.reload()

    $scope.bookTravelDetail = (task_id) ->
      $http(
        method: 'post'
        data:
          id: task_id
        url: Routes.approve_task_company_form_entries_path format: 'json'
      ).success (success) ->
        flash.success = success.message
        $scope.editChildTaskTravel = {}
        $scope.showEditDetail = false
        $scope.tableTaskParams.reload()

    $scope.comment = []
    $scope.submitComment = ->
      if $scope.comment.length > 0
        $http(
          method: 'post'
          url: Routes.company_form_entry_public_activity_activities_path $scope.taskID, format: 'json'
          data:
            public_activity_activity: {
              key: 'task.newcomment'
              parameters:
                data: $scope.comment
            }
        ).success (response) ->
          if response.success
            $scope.showableActivities = [response.activity].concat $scope.showableActivities
            $scope.totalActivitiesCount += 1
            $scope.comment = ''
          else
            flash.error = response.message

    $scope.labels = [
      "Used Hours"
      "Remaining Hours"
      "This Task"
    ]
    $scope.data = [
      300
      500
      100
    ]

    $scope.getShowableActivities = (show_all = 0)->
      $http(
        method: 'get'
        url: Routes.showable_activities_company_form_entry_path $scope.taskID, {all: show_all}
      ).success (response) ->
        $scope.showableActivities = response.activities
        $scope.totalActivitiesCount = response.total_count

    $scope.uploadAttachment = (files) ->
      file = files[0]
      $scope.upload = $upload.upload(
        method: 'post'
        url: Routes.upload_attachment_company_form_entry_path $scope.taskID
        fileFormDataName: 'files[]'
        file: file
      ).progress((evt) ->
      ).success((data, status, headers, config) ->
        $scope.showableActivities = [data.activity].concat $scope.showableActivities
        $scope.totalActivitiesCount = data.total_activity_count
      ).error( ->
        return
      )

    $scope.getShowableActivities()

    return
]
