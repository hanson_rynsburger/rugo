# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
userControllers = angular.module("easyhr.userController", [
])

userControllers.controller "UserIndexCtrl", [
  "$scope"
  "Restangular"
  "ngTableParams"
  "$http"
  "$routeParams"
  "$modal"
  "UserService"
  "$upload"
  "$window",
  "flash",
  ($scope, Restangular, ngTableParams, $http, $routeParams, $modal, UserService, $upload, $window, flash) ->
    $scope.I18n = I18n
    $scope.paginateArray = PAGINATE_ARRAY
    $scope.perPage = $scope.paginateArray[0]
    $scope.command =
      listCheckAll: false

    $scope.sortBy = ""
    $scope.companyId = $('#company_name').val()
    mySetTeamModal = null
    mySetLocationModal = null
    $scope.litsUserId = []
    $scope.teams = []
    $scope.locations = []

    $scope.newLocattion = false
    $scope.newTeam = false
    $scope.locationName = ""
    $scope.teamName = ""

    $scope.getUsers = ->
      $scope.tableParams = new ngTableParams(
        page: START_PAGE # show first page
        count: $scope.perPage # count per page
        company_id: $scope.companyId
        filter_employee: $scope.searchEmployee
        sort_by: $scope.sortBy
      ,
        total: 0 # length of data
        getData: ($defer, params) ->
          UserService.getListUser($scope, params, $defer)
      )

    $scope.getUsers()

    $scope.imported_employees = []
    $scope.invalidErrors = []
    $scope.importShow = true
    $scope.fileValid = true

    $scope.filterEmployee = ->
      $scope.tableParams.reload()

    $scope.sortByEmployee = (type) ->
      $scope.sortBy = type
      $scope.tableParams.reload()

    $scope.getListUserChecked = ->
      $scope.litsUserId = []
      $($scope.tableParams.data.data.employees).each (index, item) ->
        $scope.litsUserId.push(item.id) if item.is_check

    $scope.setTerminateModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_teminate')
          show: true
        )
        return

      params = {
        uid: $scope.litsUserId
        status: TERMINATE_STATUS
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.set_terminate_company_users_path $scope.companyId, params
      ).success (response) ->
        $scope.tableParams.reload()

    $scope.openSetLocationModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_location')
          show: true
        )
        return

      $http(
        method: 'get'
        url: Routes.list_locations_company_company_path $scope.companyId, format: 'json'
      ).success (response) ->
        $scope.locations = response.locations
        if $scope.locations.length > 0
          $scope.setLocationId = $scope.locations[0].id

        mySetLocationModal = $modal(
          title: I18n.t('js.templates.users.assign_to_location')
          scope: $scope
          template: "users/set_location_modal.html"
          contentTemplate: "users/set_location_modal_content.html"
          show: true
          id: "set_location_modal"
        )

    $scope.openSetTeamModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_team')
          show: true
        )
        return

      $http(
        method: 'get'
        url: Routes.list_teams_company_company_path $scope.companyId, format: 'json'
      ).success (response) ->
        $scope.teams = response.teams

        if $scope.teams.length > 0
          $scope.setTeamId = $scope.teams[0].id

        mySetTeamModal = $modal(
          title: I18n.t('js.templates.users.assign_to_team')
          scope: $scope
          template: "users/set_team_modal.html"
          contentTemplate: "users/set_team_modal_content.html"
          show: true
          id: "set_team_modal"
        )

    $scope.updateCheckStatusAll = ->
      i = 0
      while i < $scope.tableParams.data.data.employees.length
        $scope.tableParams.data.data.employees[i].is_check = $scope.command.listCheckAll
        i++

    $scope.updateCheckStatus = ($index) ->
      if !$scope.tableParams.data.data.employees[$index].is_check && $scope.command.listCheckAll
        $scope.command.listCheckAll = false
      else if !$scope.command.listCheckAll
        i = 0
        checkAll = true
        while i < $scope.tableParams.data.data.employees.length
          if !$scope.tableParams.data.data.employees[i].is_check
            checkAll = false
          i++
        $scope.command.listCheckAll = checkAll if checkAll
      return

    $scope.submitSetLocation = () ->
      $scope.getListUserChecked()

      $scope.locationId = mySetLocationModal.$scope.setLocationId

      params = {
        uid: $scope.litsUserId
        location_id: $scope.locationId
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.set_location_company_users_path $scope.companyId, params
      ).success (response) ->
        mySetLocationModal.hide()
        $scope.tableParams.reload()


    $scope.submitSetTeam = () ->
      $scope.getListUserChecked()

      $scope.teamId = mySetTeamModal.$scope.setTeamId

      params = {
        uid: $scope.litsUserId
        team_id: $scope.teamId
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.set_team_company_users_path $scope.companyId, params
      ).success (response) ->
        mySetTeamModal.hide()
        $scope.tableParams.reload()


    $scope.addNewLocation = ->
      $scope.newLocattion = true

    $scope.saveLocation = (isValid) ->
      if isValid
        params = {
          location_name: $scope.$$childTail.locationName
          format: 'json'
        }

        $http(
          method: 'post'
          url: Routes.new_location_company_path $scope.companyId, params
        ).success (success) ->
          $scope.$$childTail.locationName = ""
          $scope.newLocattion = false
          $http(
            method: 'get'
            url: Routes.list_locations_company_company_path $scope.companyId, format: 'json'
          ).success (response) ->
            $scope.locations = response.locations
            if $scope.locations.length > 0
              $scope.setLocationId = success.location_id

    $scope.cancelAddLocation = ->
      $scope.newLocattion = false
      $scope.$$childTail.locationName = ""

    $scope.addNewTeam = ->
      $scope.newTeam = true

    $scope.saveTeam = (isValid) ->
      if isValid
        params = {
          team_name: $scope.$$childTail.teamName
          format: 'json'
        }

        $http(
          method: 'post'
          url: Routes.new_team_company_path $scope.companyId, params
        ).success (success) ->
          $scope.$$childTail.teamName = ""
          $scope.newTeam = false

          $http(
            method: 'get'
            url: Routes.list_teams_company_company_path $scope.companyId, format: 'json'
          ).success (response) ->
            $scope.teams = response.teams

            if $scope.teams.length > 0
              $scope.setTeamId = success.team_id

    $scope.cancelAddTeam = ->
      $scope.newTeam = false
      $scope.$$childTail.teamName = ""

    $scope.uploadFile = ->
      if $scope.file
        $scope.fileValid = /((.csv)|(.xls)|(.xlsx))$/.test($scope.file[0].name)
        return if !$scope.fileValid
        $scope.uploadFiled = $upload.upload(
          url: Routes.import_file_company_users_path $scope.companyId, format: 'json'
          method: 'post'
          file: $scope.file,
        ).success (data, status, headers, config) ->
          $scope.invalidErrors = data.invalid_employee_list
          $scope.importShow = false
          $scope.imported_employees = data.import_employee_list

    $scope.cancelFile = ->
      $scope.file = null
      $scope.fileValid = true

    $scope.backImport = ->
      $scope.file = null
      $scope.importShow = true

    $scope.downLoadTemplate = ->
      url = Routes.download_template_company_users_path($scope.companyId)
      $window.open(url, "_blank")
      return

    $scope.saveEmployeeWithConfirm = ->
      params = {
        users: $scope.imported_employees
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.save_employee_with_confirm_company_users_path $scope.companyId, params
      ).success (response) ->
        $scope.imported_employees = []
        $scope.importShow = true
        $scope.invalidErrors = []
        $scope.file = null
        flash.success = response.message

    $scope.cancelConfirm = ->
      $scope.imported_employees = []
      $scope.importShow = true
      $scope.invalidErrors = []
      $scope.file = null
]



userControllers.controller "UserDetailCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http"
  ($scope, Restangular, $timeout, $http) ->


    return
]

userControllers.controller "UserEditCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http"
  ($scope, Restangular, $timeout, $http) ->
    $scope.user = user
    $scope.user.employment = {} if !$scope.user.employment
    $scope.user.personal = {} if !$scope.user.personal
    $scope.user.tax = {} if !$scope.user.tax
    $scope.user.emergency = [] if !$scope.user.emergency
    $scope.user.bank = [] if !$scope.user.bank
    $scope.user.compensation = [] if !$scope.user.compensation

    $scope.temp = []
    $scope.companyId = $('#current_company').val()
    $scope.locations = []

    $scope.statuses = STATUS
    $scope.icons = ICONS

    $http(
      method: 'get'
      url: Routes.list_users_company_company_path $scope.companyId, format: 'json'
    ).success (response) ->
      $scope.manager_users = response.users
      $scope.users = response.users

      $scope.users = $.map($scope.users, (user) ->
        {value: user.id, label: user.email}
      )
      $scope.manager_users = $.map($scope.manager_users, (user) ->
        {value: user.id, label: user.email}
      )
      $scope.updateValueByArray($scope.users, "backup_id")
      $scope.updateValueByArray($scope.manager_users, "employment", "manager_id", false, true)

    $http(
      method: 'get'
      url: Routes.list_locations_company_company_path $scope.companyId, format: 'json'
    ).success (response) ->
      $scope.locations = response.locations
      $scope.locations = $.map($scope.locations, (location) ->
        {value: location.id, label: location.name}
      )
      $scope.updateValueByArray($scope.locations, "location_id")

    $scope.genders = GENDER
    $scope.marital_statuses = MARITAL_STATUS
    $scope.account_types = ACCOUNT_TYPES
    $scope.tax_status = TAX_STATUS
    $scope.tax_decline_chooses = BOOLEAN
    $scope.currency_chooses = CURRENCIES
    $scope.periods = PERIODS

    $scope.updateValueByArray = (array, objectDir1, objectDir2, arrayRec, multipleValue) ->
      i = 0

      while i < array.length
        if objectDir1 && !objectDir2
          $scope.user[objectDir1] = array[i] if $scope.user[objectDir1] == array[i].value
        else if multipleValue
          $scope.temp.push(array[i]) if $.inArray(array[i].value.toString() , $scope.user[objectDir1][objectDir2]) != -1

        else if objectDir1 && objectDir2 && !arrayRec
          $scope.user[objectDir1][objectDir2] = array[i] if parseInt($scope.user[objectDir1][objectDir2]) == parseInt(array[i].value)
        else if objectDir1 && objectDir2 && arrayRec
          j = 0
          while j < $scope.user[objectDir1].length
            $scope.user[objectDir1][j][objectDir2] = array[i] if parseInt($scope.user[objectDir1][j][objectDir2]) == parseInt(array[i].value)
            j++
        i++
      if multipleValue
        $scope.user[objectDir1][objectDir2] = $scope.temp

    $scope.updateValueByArray($scope.statuses, "status")
    #$scope.updateValueByArray($scope.icons, "employment_type")
    $scope.updateValueByArray($scope.genders, "personal", "gender")
    $scope.updateValueByArray($scope.marital_statuses, "personal", "marital_status")
    $scope.updateValueByArray($scope.account_types, "bank", "account_type", true)
    $scope.updateValueByArray($scope.tax_status, "tax", "filing_status")
    $scope.updateValueByArray($scope.tax_decline_chooses, "tax", "decline_withholding")
    $scope.updateValueByArray($scope.currency_chooses, "compensation", "currency", true)
    $scope.updateValueByArray($scope.periods, "compensation", "per", true)
    #Emergency
    $scope.emergency_template =
      name: ""
      relationship: ""
      phone_number: ""

    if !$scope.user.emergency.length
      $scope.user.emergency.push(angular.copy($scope.emergency_template))

    $scope.addNewEmergency = ->
      $scope.user.emergency.push(angular.copy($scope.emergency_template))

    $scope.removeEmergency = (index) ->
      $scope.user.emergency.splice(index, 1)


    #Bank info
    $scope.bank_template =
      name: ""
      account_type: null
      id_number: ""
      account_number: ""

    if !$scope.user.bank.length
      $scope.user.bank.push(angular.copy($scope.bank_template))

    $scope.addNewBank = ->
      $scope.user.bank.push(angular.copy($scope.bank_template))

    $scope.removeBank = (index) ->
      $scope.user.bank.splice(index, 1)

    #Compensation
    $scope.compensation_template =
      amount: ""
      currency: null
      per: null
      effective: ""
      reason: ""

    if !$scope.user.compensation.length
      $scope.user.compensation.push(angular.copy($scope.compensation_template))

    $scope.addNewCompensation = ->
      $scope.user.compensation.push(angular.copy($scope.compensation_template))

    $scope.removeCompensation = (index) ->
      $scope.user.compensation.splice(index, 1)

    $scope.updateEmploymentTypeInput = ->
      $('[name="user[employment_type]"]').val($scope.user.employment_type)
      return

    $scope.submitEmployee = (eventSubmit)->
      $scope.employeeForm["user[personal][first_name]"].$dirty = true
      $scope.employeeForm["user[personal][last_name]"].$dirty = true
      $scope.employeeForm["user[employment][work_email]"].$dirty = true

      if $scope.employeeForm.$invalid
        eventSubmit.preventDefault()

    return
]