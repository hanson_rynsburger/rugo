planBillingControllers = angular.module("easyhr.planAndBillingController", ["restangular"])

planBillingControllers.controller "PlanAndBillingIndexCtrl", ["$scope", "$timeout", ($scope, $timeout) ->
  $scope.plans
  $scope.tabs             ||= []
  $scope.tabs.activeTab   = 'Subscription'
  $scope.showChangePlan   = false
  $scope.showFormBilling
  $scope.showFormCard     = false
  $scope.initCCForm
  $scope.client_token
  $scope.showNewPayment   = false

  $scope.changePlan = ()->
    $scope.showChangePlan = true
    $timeout(->
      $scope.initPriceBar()
    , 1)

  $scope.cancelChangePlan = ()->
    $scope.showChangePlan = false
    $timeout(->
      $scope.initProgressBar()
    , 1)

  $scope.showFormAddressAndCard = (bool)->
    $scope.showFormBilling = bool
    if bool
      $scope.setupPaymentForm()

  # to generate js progressBar
  $scope.initProgressBar = ()->
    current     = $(".progress-bar").attr("aria-valuenow")
    max         = $(".progress-bar").attr("aria-valuemax")
    percentage  = current/max * 100

    $(".progress-bar").animate({
      width: percentage + "%"
    }, 350)
    return

  # to generate js progressbar for plan price
  $scope.initPriceBar = ()->
    $slider = $('#priceSlider')
    if $slider.length >= 1
      $slider.slider({tooltip: 'hide'})
      $slider.on 'change', (e) ->
        plan = $scope.plans[ (e.value.newValue / 5) - 1 ]
        $('#priceSliderVal').text plan.num_employees
        $('.price .value').text parseInt(plan.price)
    return

  $scope.setupPaymentForm = ()->
    $payment_form = $('#payment-form')
    braintree.setup $scope.client_token, "dropin",
      form: 'payment-form'
      container: 'credit-card-container'

  $scope.showNewCreditCardForm = (bool)->
    $scope.showNewPayment = bool
    if bool
      $scope.newPaymentForm()

  $scope.newPaymentForm = ()->
    $payment_form = $('#mew-payment-form')
    y = $(window).scrollTop();

    braintree.setup $scope.client_token, "dropin",
      form: 'new-payment-form'
      container: 'new-credit-card-container'
      onReady: ()->
        $("html, body").animate(
          scrollTop: y + $(window).height()
        , 600)

  # all doc ready here
  $timeout(->
    $scope.initProgressBar()
    $scope.initPriceBar()

    if $scope.initCCForm
      $scope.setupPaymentForm()

  , 1000)
]