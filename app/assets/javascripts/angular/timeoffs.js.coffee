timeoffControllers = angular.module("easyhr.timeoffController", [])

timeoffControllers.controller "TimeoffNewCtrl", ["$scope", "$timeout", ($scope, $timeout) ->

  # document ready
  $('.datetimepicker').datetimepicker({
    locale: 'en-gb'
    sideBySide: true
    showClose: true
    useStrict: true
    stepping: 15
    icons:
      time: "fa fa-clock-o"
      date: "fa fa-calendar"
      up: "fa fa-arrow-up"
      down: "fa fa-arrow-down"
      clear: "fa fa-eraser"
      close: "fa fa-close close-datetimepicker"
  });

]


timeoffControllers.controller "TimeoffIndexCtrl", ["$scope", "$timeout", ($scope, $timeout) ->


]