formBuildersController = angular.module("easyhr.formBuildersController", ['ui.sortable'])

formBuildersController.controller "FormBuilderCtrl", ["$scope", "$timeout", ($scope, $timeout) ->
  $scope.form
  $scope.fields
  $scope.field_types
  $scope.travelPreferTimes = TIME_TYPE
  $scope.travelTransportations = TRANSPORTATION_TYPE

  $scope.sortableOptions =
    update: (e, ui) ->
      $timeout (->
        angular.forEach $scope.fields, (field, index) ->
          field.position = index
      ), 100
    axis: 'y'


  $scope.sortableGroupOptions =
    update: (e, ui) ->
      $timeout (->
        # nothing to do
      ), 100
    axis: 'y'

  # create range array number with min & max & step
  $scope.range = (min, max, step) ->
    step  = ((step == undefined) ? 1 : step)
    input = []
    i = min
    while i <= max
      input.push(i)
      i = i+step
    return input


  $scope.defaultFieldOption = (field_type = "single")->
    return({
      persisted: false
      deleted: false
      id: $scope.unique_id()
      name: 'field_' + $scope.unique_id()
      field_type: field_type
      required: false
      position: ($scope.fields.length + 1)
      properties:
        label: 'Field label'
        hint: ''
        placeholder: 'Field placeholder'
    })


  # method to add field daterange (special field)
  $scope.addFieldDateRange = ()->
    field = $scope.defaultFieldOption('date_range')
    field.required = true
    field.properties =
      unremoved: true
      label: 'Date Range'
      hint: 'This field should be filled, because will appeared on calendar page'

    # add field
    $scope.fields.unshift( field )

  # method to add more field
  $scope.addField = (field_type) ->
    field = $scope.defaultFieldOption(field_type)

    switch field.field_type
      when 'address'
        field.properties.label = 'Address'

      when 'boolean'
        field.properties =
          label: 'Boolean'
          true_label: 'Yes'
          false_label: 'No'

      when 'date'
        field.properties.label = 'Date'

      when 'datetime'
        field.properties.label = "Date & Time"

      when 'email'
        field.properties.label = 'Email'

      when 'file'
        field.properties.label = 'File'

      # prepopulate field options if field type is mcq | dropdown
      when 'dropdown', 'checkbox', 'radio'
        if field.field_type == 'dropdown'
          field.properties.prompt = "Please select"

        field.properties.select_options = [
          {label: 'Option 1', value: 'Value 1'}
          {label: 'Option 2', value: 'Value 2'}
          {label: 'Option 3', value: 'Value 2'}
        ]

      when 'number'
        field.properties.label = 'Number'

      when 'percentage'
        field.properties.label = 'Percentage'

      when 'rating'
        field.properties =
          label: 'Rating'
          symbol: 'number'
          max_rating: 5
          # format: 'inline'

      when 'price'
        field.properties =
          label: 'Price'
          currency: '$'
          add_on: 'prepend'

      when 'date_range'
        field.properties =
          label: 'Date Range'

      when 'question_group'
        field.properties.label = 'Question Group'
        field.properties.max_rows = 1
        field.properties.groups = [
          {name: 'Column 1', add_on: 'none'}
          {name: 'Column 2', add_on: 'none'}
          {name: 'Column 3', add_on: 'none'}
        ]

      when 'advance_group'
        field.properties.label = 'Advance Group'
        field.properties.max_rows = 1
        field.properties.advance_groups = [
          {name: 'Column 1', add_on: 'none', type: 'text', options: ['Option 1', 'Option 2', 'Option 3']}
          {name: 'Column 2', add_on: 'none', type: 'text', options: ['Option 1', 'Option 2', 'Option 3']}
          {name: 'Column 3', add_on: 'none', type: 'text', options: ['Option 1', 'Option 2', 'Option 3']}
        ]

      when 'range'
        field.properties =
          label: 'Range'
          from_number: 0
          to_number: 0

      when 'section'
        field.properties = {
          description: "Section detail here..."
        }

      when 'statement'
        field.properties.label = 'Statement'
        field.properties.max_rows = 0
        field.properties.statements = {
          0: {name: 'Statement 1'}
          1: {name: 'Statement 2'}
          2: {name: 'Statement 3'}
        }

        field.properties.columns = {
          0: {name: 'Column 1'}
          1: {name: 'Column 2'}
          2: {name: 'Column 3'}
        }

      when 'website'
        field.properties.label = 'URL'

      when 'expense'
        field.properties = {
          label: 'Expense'
          hint: null
          required: false
          file: null
          categories: [
            { label: 'Air Ticket', value: 'air_ticket' }
          ]
          purposes: [
            { label: 'Rating meetings', value: 'rating_meetings' }
          ]
          payments: [
            { label: 'Credit Card', value: 'credit_card' },
            { label: 'Paypal', value: 'paypal' },
            { label: 'Bank Transfer', value: 'bank_transfer' }
          ]
          currencies: [
            { label: 'USD', value: 'usd' },
            { label: 'SGD', value: 'sgd' }
          ]
        }

      when 'travel'
        field.properties = {
          label: 'Travel'
          hint: null
          required: false
          transportations: [
            { label: 'Flight', value: 'flight' },
            { label: 'Train', value: 'train' },
            { label: 'Bus', value: 'bus' }
          ]
          classes: [
            { label: 'Economy', value: 'economy' },
            { label: 'Business', value: 'business' },
            { label: 'Executive', value: 'executive' }
          ]
        }

      when 'bank'
        field.properties = {
          label: 'Bank'
          hint: null
          required: false
          bank_account_types: [
            {label: 'Checking', value: 'checking'}
            {label: 'Saving', value: 'saving'}
          ]
        }

      when 'compensation'
        field.properties = {
          label: 'Compensation'
          hint: null
          required: false
          compensation_currencies: [
            { label: 'USD', value: 'usd' }
            { label: 'SGD', value: 'sgd' }
          ]
          compensation_periods: [
            { label: 'Year',     value: 'year' }
            { label: 'Quarter',  value: 'quarter' }
            { label: 'Month',    value: 'Month' }
          ]
        }

      when 'emergency'
        field.properties = {
          label: 'Emergency'
          hint: null
          required: false
        }

      when 'tax'
        field.properties = {
          label: 'Tax'
          hint: null
          required: false
          tax_filling_status_prompt: null
          tax_decline_with_holding_prompt: null
          tax_filling_status_options: [
            { label: 'Joint', value: 'joint' }
            { label: 'Single', value: 'single' }
          ]
          tax_decline_with_holdings: [
            { label: 'Yes', value: '1' }
            { label: 'No', value: '0' }
          ]
        }

      when 'personal'
        field.properties = {
          label: 'Personal'
          hint: null
          required: false
          personal_genders: [
            { label: 'Male', value: 'male' }
            { label: 'Female', value: 'female' }
          ]
          personal_marital_statuses: [
            { label: 'Single', value: 'single'}
            { label: 'Married', value: 'married' }
            { label: 'Divoced', value: 'divoced' }
            { label: 'Widowed', value: 'widowed' }
          ]
        }

    # end of switch

    $scope.fields.push( field )

  # method to remove field
  $scope.removeField = (field) ->
    if field.persisted
      if confirm( I18n.t('form').confirm_remove_field )
        field.deleted = true
    else
      $scope.fields.splice( $scope.fields.indexOf(field), 1 )

  $scope.addFieldPropertiesStatement = (field) ->
    obj = {name: 'New statement',  persisted: false}
    idx = Object.keys(field.properties.statements).length + 1
    field.properties.statements[idx] = obj


  $scope.removeFieldPropertiesStatement = (field, statement, idx)->
    if statement.persisted
      if confirm( I18n.t('form').confirm_remove_field )
        delete field.properties.statements[idx]
    else
      delete field.properties.statements[idx]



  $scope.addFieldPropertiesColumn = (field) ->
    obj = {name: 'New Column', persisted: false}
    idx = Object.keys(field.properties.columns).length + 1
    field.properties.columns[idx] = obj


  $scope.removeFieldPropertiesColumn = (field, column, idx)->
    if column.persisted || (column.persisted == undefined)
      if confirm( I18n.t('form').confirm_remove_field )
        delete field.properties.columns[idx]
    else
      delete field.properties.columns[idx]


  $scope.checkLimitGroupQuesiton = (field)->
    if field.properties
      if field.properties.groups
        return field.properties.groups.length < 5

  $scope.removeFieldPropertiesGroup = (field, group, idx)->
    if group.persisted
      if confirm( I18n.t('form').confirm_remove_field )
        field.properties.groups.splice(idx, 1)
    else
      field.properties.groups.splice(idx, 1)



  $scope.addFieldPropertiesGroup = (field)->
    if Object.keys(field.properties.groups).length < 5
      obj = {name: 'New Column', add_on: 'none', persisted: false}
      # idx = Object.keys(field.properties.groups).length + 1
      # while field.properties.groups[idx] != undefined
      #   idx += 1
      field.properties.groups.push(obj)


  $scope.addQuestionGroupRow = (field)->
    field.properties.dummy_group_rows ||= 1
    field.properties.dummy_group_rows += 1

  $scope.removeQuestionGroupRow = (field)->
    field.properties.dummy_group_rows ||= 1
    field.properties.dummy_group_rows -= 1 if (field.properties.dummy_group_rows > 1)

  $scope.isTheLastGroupQuestion = (field)->
    !(Object.keys(field.properties.groups).length  <= 1)


  $scope.checkLimitAdvanceGroupQuesiton = (field)->
    if field.properties
      if field.properties.advance_groups
        field.properties.advance_groups.length < 5
      else
        false

  $scope.removeFieldPropertiesAdvanceGroup = (field, advance_group, idx)->
    if advance_group.persisted
      if confirm( I18n.t('form').confirm_remove_field )
        field.properties.advance_groups.splice(idx, 1)
    else
      field.properties.advance_groups.splice(idx, 1)


  $scope.addFieldPropertiesAdvanceGroup = (field)->
    if Object.keys(field.properties.advance_groups).length < 5
      obj = {name: 'New Column', add_on: 'none', type: 'text', persisted: false}
      field.properties.advance_groups.push(obj)


  $scope.addQuestionAdvanceGroupRow = (field)->
    field.properties.dummy_group_rows ||= 1
    field.properties.dummy_group_rows += 1


  $scope.removeQuestionAdvanceGroupRow = (field)->
    field.properties.dummy_group_rows ||= 1
    field.properties.dummy_group_rows -= 1 if (field.properties.dummy_group_rows > 1)


  $scope.isTheLastAdvanceGroupQuestion = (field)->
    !(Object.keys(field.properties.advance_groups).length  <= 1)


  $scope.isTheLastAdavanceGroupQuestion = (field)->
    !(Object.keys(field.properties.advance_groups).length  <= 1)


  $scope.addFieldPropertiesAdvanceGroupColumn = (field)->
    if Object.keys(field.properties.advance_groups).length < 5
      obj = {name: 'New Column', add_on: 'none', type: 'text', options: ['Option 1', 'Option 2', 'Option 3'], persisted: false}
      field.properties.advance_groups.push(obj)

  $scope.addFieldPropertiesAdvanceGroupOption = (field, advance_group)->
    advance_group.options ||= []
    advance_group.options.push('New option')

  $scope.removeFieldPropertiesAdvanceGroupOption = (field, advance_group, advance_group_id, option_id)->
    advance_group.options.splice(option_id, 1)

  $scope.addPropertiesOptions = (field, type)->
    field.properties[type] ||= []
    field.properties[type].push({label: 'Label', value: 'value'})


  $scope.removePropertiesOptions = (field, type, idx)->
    field.properties[type].splice(idx, 1)


  # will return unique_id
  $scope.unique_id = () ->
    (new Date()).getTime()

  # toggle show / hide form field configuration
  $scope.toggleFieldConfig = (field) ->
    $container = $('#form-field-' + field.id)
    $field_container = $container.find('.field-config-container')
    $field_container.toggleClass('hide')
    return false

  # display field options based on field type
  $scope.needField = (field, field_needed) ->
    field_type = $scope.field_types[field.field_type]
    return ( field_type.options.indexOf(field_needed) >= 0 )


  $scope.getAdvanceGroupColumnWidthClass = (field)->
    default_class = ''
    switch  Object.keys(field.properties.advance_groups).length
      when 1 then "col-xs-10"
      when 2 then "col-xs-5"
      when 3 then "col-xs-3"
      when 4 then "col-xs-2"
      when 5 then "col-xs-2"

  $scope.getGroupColumnWidthClass = (field)->
    default_class = ''
    switch  Object.keys(field.properties.groups).length
      when 1 then "col-xs-10"
      when 2 then "col-xs-5"
      when 3 then "col-xs-3"
      when 4 then "col-xs-2"
      when 5 then "col-xs-2"


  # if form.show_on_calendar is true,
  # add field date-range to form
  # else... remove date range field if any
  #
  $scope.toggleUseDateRange = ()->
    if $scope.form.show_on_calendar
      $scope.addFieldDateRange()

    else
      angular.forEach $scope.fields, (field, idx)->
        if (field.properties['unremoved'] == true) || (field.properties['unremoved'] == 'true') || (parseInt(field.properties['unremoved']) == 1)
          if field.persisted
            if confirm( 'Are you sure to remove this date range field? ' )
              field.deleted = true
            else
              $scope.form.show_on_calendar = true
              field.deleted = false

          else
            $scope.fields.splice( $scope.fields.indexOf(field), 1 )

  angular.element(document).ready ()->
    $timeout (->
      $('.form-icon-picker').iconpicker
        mustAccept:true
        placement:'bottomLeft'

      $('#form-container').on 'click', '.thumbnail', ()->
        $(this).closest('.row').find('.thumbnail').removeClass('active')
        $(this).addClass('active')

      $scope.form.introduction ||= I18n.t('form').content_should_go_in_text_area
      angular.forEach $scope.fields, (field, index) ->
        field.properties ||= {
          statements: []
          columns: []
        }

        switch field.field_type
          when 'rating'
            field.properties.max_rating = parseInt(field.properties.max_rating)

          when 'number', 'range', 'percentage'
            field.properties.from_number = parseInt(field.properties.from_number)
            field.properties.to_number = parseInt(field.properties.to_number)

          when 'question_group'
            field.properties.groups =  $.map(field.properties.groups, (value, index)->
              value['persisted'] = true
              value
            )

          when 'advance_group'
            field.properties.groups =  $.map(field.properties.groups, (value, index)->
              value['persisted'] = true
              value
            )
    )

]
