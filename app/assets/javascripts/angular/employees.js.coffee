# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
employeeControllers = angular.module("easyhr.employeeController", [
])

employeeControllers.controller "EmployeeIndexCtrl", [
  "$scope"
  "Restangular"
  "ngTableParams"
  "$http"
  "$routeParams"
  "$modal"
  "UserService"
  "$upload"
  "$window",
  "flash",
  ($scope, Restangular, ngTableParams, $http, $routeParams, $modal, UserService, $upload, $window, flash) ->
    $scope.I18n = I18n
    $scope.paginateArray = PAGINATE_ARRAY
    $scope.perPage = $scope.paginateArray[0]
    $scope.command =
      listCheckAll: false

    $scope.sortBy = ""
    mySetTeamModal = null
    mySetLocationModal = null
    $scope.litsUserId = []
    $scope.teams = []
    $scope.locations = []
    $scope.newLocattion = false
    $scope.newTeam = false
    $scope.locationName = ""
    $scope.teamName = ""

    $scope.getUsers = (type) ->
      $scope.tableParams = new ngTableParams(
        page: START_PAGE # show first page
        count: $scope.perPage # count per page
        filter_employee: $scope.searchEmployee
        sort_by: $scope.sortBy
      ,
        total: 0 # length of data
        getData: ($defer, params) ->
          UserService.getListUser($scope, params, $defer)
      )

    $scope.getUsers()

    $scope.imported_employees = {}
    $scope.invalidErrors = []
    $scope.importShow = false
    $scope.nextImportStep = false
    $scope.fileValid = true
    $scope.fileValidServer = false

    $scope.filterEmployee = ->
      $scope.tableParams.reload()

    $scope.sortByEmployee = (type) ->
      $scope.sortBy = type
      $scope.tableParams.reload()

    $scope.getListUserChecked = ->
      $scope.litsUserId = []
      $($scope.tableParams.data.data.employees).each (index, item) ->
        $scope.litsUserId.push(item.id) if item.is_check

    $scope.setTerminateModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_teminate')
          show: true
        )
        return

      params = {
        uid: $scope.litsUserId
        status: TERMINATE_STATUS
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.bulk_terminate_company_employees_path params
      ).success (response) ->
        if !response.success
          flash.error = response.messages
        $scope.tableParams.reload()

    $scope.openSetLocationModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_location')
          show: true
        )
        return

      $http(
        method: 'get'
        url: Routes.company_locations_path format: 'json'
      ).success (response) ->
        $scope.locations = response.locations
        if $scope.locations.length > 0
          $scope.setLocationId = $scope.locations[0].id

        mySetLocationModal = $modal(
          title: I18n.t('js.templates.users.assign_to_location')
          scope: $scope
          template: "users/set_location_modal.html"
          contentTemplate: "users/set_location_modal_content.html"
          show: true
          id: "set_location_modal"
        )

    $scope.openSetTeamModal = ->
      $scope.getListUserChecked()

      if $scope.litsUserId.length == 0
        myModal = $modal(
          title: I18n.t('js.templates.users.confirm')
          content: I18n.t('js.templates.users.select_before_team')
          show: true
        )
        return

      $http(
        method: 'get'
        url: Routes.company_teams_path format: 'json'
      ).success (response) ->
        $scope.teams = response.teams

        if $scope.teams.length > 0
          $scope.setTeamId = $scope.teams[0].id

        mySetTeamModal = $modal(
          title: I18n.t('js.templates.users.assign_to_team')
          scope: $scope
          template: "users/set_team_modal.html"
          contentTemplate: "users/set_team_modal_content.html"
          show: true
          id: "set_team_modal"
        )

    $scope.updateCheckStatusAll = ->
      i = 0
      while i < $scope.tableParams.data.data.employees.length
        $scope.tableParams.data.data.employees[i].is_check = $scope.command.listCheckAll
        i++

    $scope.updateCheckStatus = ($index) ->
      if !$scope.tableParams.data.data.employees[$index].is_check && $scope.command.listCheckAll
        $scope.command.listCheckAll = false
      else if !$scope.command.listCheckAll
        i = 0
        checkAll = true
        while i < $scope.tableParams.data.data.employees.length
          if !$scope.tableParams.data.data.employees[i].is_check
            checkAll = false
          i++
        $scope.command.listCheckAll = checkAll if checkAll
      return

    $scope.submitSetLocation = () ->
      $scope.getListUserChecked()

      $scope.locationId = mySetLocationModal.$scope.setLocationId

      params = {
        uid: $scope.litsUserId
        location_id: $scope.locationId
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.bulk_location_employees_path params
      ).success (response) ->
        if !response.success
          flash.error = response.messages

        mySetLocationModal.hide()
        $scope.tableParams.reload()


    $scope.submitSetTeam = () ->
      $scope.getListUserChecked()

      $scope.teamId = mySetTeamModal.$scope.setTeamId

      params = {
        uid: $scope.litsUserId
        team_id: $scope.teamId
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.bulk_team_company_employees_path params
      ).success (response) ->
        if !response.success
          flash.error = response.messages

        mySetTeamModal.hide()
        $scope.tableParams.reload()

    $scope.addNewLocation = ->
      $scope.newLocattion = true

    $scope.saveLocation = (isValid) ->
      if isValid
        params = {
          location_name: $scope.$$childTail.locationName
          format: 'json'
        }

        $http(
          method: 'post'
          url: Routes.company_locations_path params
        ).success (success) ->
          $scope.$$childTail.locationName = ""
          $scope.newLocattion = false
          $http(
            method: 'get'
            url: Routes.company_locations_path format: 'json'
          ).success (response) ->
            $scope.locations = response.locations
            if $scope.locations.length > 0
              $scope.setLocationId = success.location_id

    $scope.cancelAddLocation = ->
      $scope.newLocattion = false
      $scope.$$childTail.locationName = ""

    $scope.addNewTeam = ->
      $scope.newTeam = true

    $scope.saveTeam = (isValid) ->
      if isValid
        params = {
          team_name: $scope.$$childTail.teamName
          format: 'json'
        }

        $http(
          method: 'post'
          url: Routes.company_teams_path params
        ).success (success) ->
          $scope.$$childTail.teamName = ""
          $scope.newTeam = false

          $http(
            method: 'get'
            url: Routes.company_teams_path format: 'json'
          ).success (response) ->
            $scope.teams = response.teams

            if $scope.teams.length > 0
              $scope.setTeamId = success.team_id

    $scope.cancelAddTeam = ->
      $scope.newTeam = false
      $scope.$$childTail.teamName = ""

    $scope.uploadFile = ->
      if $scope.file
        $scope.fileValid = /((.csv)|(.xls)|(.xlsx))$/.test($scope.file[0].name)
        return if !$scope.fileValid
        $scope.uploadFiled = $upload.upload(
          url: Routes.import_file_company_employees_path format: 'json'
          method: 'post'
          file: $scope.file,
        ).success (data, status, headers, config) ->
          if !data.success
            $scope.messageWrongFile = data.message
            $scope.fileValidServer = true
            $scope.importShow = true
            $scope.nextImportStep = false
            return
          else
            $scope.importShow = true
            $scope.nextImportStep = true
            $scope.invalidErrors = data.invalid_employee_list
            $scope.imported_employees = data.import_employee_list

    $scope.cancelFile = ->
      $scope.importShow = false
      $scope.nextImportStep = false
      $scope.file = null
      $scope.fileValid = true
      $scope.fileValidServer = false

    $scope.backImport = ->
      $scope.file = null
      $scope.importShow = true
      $scope.nextImportStep = false
      $scope.fileValidServer = false

    $scope.downLoadTemplate = ->
      url = Routes.download_template_company_employees_path()
      $window.open(url, "_blank")
      return

    $scope.saveEmployeeWithConfirm = ->
      params = {
        format: 'json'
      }

      $http(
        method: 'post'
        url: Routes.save_employee_with_confirm_company_employees_path params
        data:
          users: $scope.imported_employees
      ).success (response) ->
        if !response.success
          flash.error = response.message
        else
          $scope.importShow = false
          $scope.nextImportStep = false
          $scope.imported_employees = []
          $scope.invalidErrors = []
          $scope.file = null
          flash.success = response.message
          $scope.tableParams.reload()

    $scope.cancelConfirm = ->
      $scope.importShow = false
      $scope.nextImportStep = false
      $scope.fileValidServer = false
      $scope.imported_employees = []
      $scope.invalidErrors = []
      $scope.file = null

    $scope.triggerImport = ->
      $scope.importShow = true

    $scope.dropdown = [
      {
        "text": I18n.t('js.templates.users.new_employee'),
        "href": Routes.new_company_employee_path()
      },
      {
        "text": I18n.t('js.templates.users.import_employees'),
        "click": "triggerImport()"
      }
    ]
]



employeeControllers.controller "EmployeeDetailCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http"
  ($scope, Restangular, $timeout, $http) ->


    return
]

employeeControllers.controller "EmployeeEditCtrl", [
  "$scope"
  "Restangular"
  "$timeout"
  "$http"
  ($scope, Restangular, $timeout, $http) ->
    $scope.user = user
    $scope.user.employment = {} if !$scope.user.employment
    $scope.user.personal = {} if !$scope.user.personal
    $scope.user.tax = {} if !$scope.user.tax
    $scope.user.emergency = [] if !$scope.user.emergency
    $scope.user.bank = [] if !$scope.user.bank
    $scope.user.compensation = [] if !$scope.user.compensation
    $scope.temp = []
    $scope.locations = []

    $scope.statuses = STATUS
    $scope.icons = ICONS

    if $(".select-managers").length
      tags = $('.select-managers').data("multi-managers")
      $(".select-managers").select2(
        multiple: true
        ajax:
          url: Routes.list_company_employees_path()
          dataType: "json"
          data: (params) ->
            manager: params

          results: (data) ->
            results: $.map(data, (user, i) ->
              id: user.id
              text: user.email
            )

        initSelection: (element, callback) ->
          data = $(element).data("tags")
          callback data
      ).select2('data', tags)

    if $(".select-backup-person").length
      tag = $('.select-backup-person').data("backup-person")
      $(".select-backup-person").select2(
        ajax:
          url: Routes.list_company_employees_path()
          dataType: "json"
          data: (params) ->
            manager: params

          results: (data) ->
            results: $.map(data, (user, i) ->
              id: user.id
              text: user.email
            )
        initSelection: (element, callback) ->
          data = $(element).data("backup-person")
          callback data
      ).select2('data', tag)

    $http(
      method: 'get'
      url: Routes.company_locations_path format: 'json'
    ).success (response) ->
      $scope.locations = response.locations
      $scope.locations = $.map($scope.locations, (location) ->
        {value: location.id, label: location.name}
      )
      $scope.updateValueByArray($scope.locations, "location_id")

    $http(
      method: 'get'
      url: Routes.company_teams_path format: 'json'
    ).success (response) ->
      $scope.teams = response.teams
      $scope.teams = $.map($scope.teams, (team) ->
        {value: team.id, label: team.name}
      )
      $scope.updateValueByArray($scope.teams, "team_id")

    $scope.genders = GENDER
    $scope.marital_statuses = MARITAL_STATUS
    $scope.account_types = ACCOUNT_TYPES
    $scope.tax_status = TAX_STATUS
    $scope.tax_decline_chooses = BOOLEAN
    $scope.currency_chooses = CURRENCIES
    $scope.periods = PERIODS

    $scope.updateValueByArray = (array, objectDir1, objectDir2, arrayRec, multipleValue) ->
      i = 0

      while i < array.length
        if objectDir1 && !objectDir2
          $scope.user[objectDir1] = array[i] if $scope.user[objectDir1] == array[i].value
        else if multipleValue
          $scope.temp.push(array[i]) if $.inArray(array[i].value.toString() , $scope.user[objectDir1][objectDir2]) != -1

        else if objectDir1 && objectDir2 && !arrayRec
          $scope.user[objectDir1][objectDir2] = array[i] if $scope.user[objectDir1][objectDir2] == array[i].value
        else if objectDir1 && objectDir2 && arrayRec
          j = 0
          while j < $scope.user[objectDir1].length
            $scope.user[objectDir1][j][objectDir2] = array[i] if $scope.user[objectDir1][j][objectDir2] == array[i].value
            j++
        i++
      if multipleValue
        $scope.user[objectDir1][objectDir2] = $scope.temp

    $scope.updateValueByArray($scope.statuses, "status")
    #$scope.updateValueByArray($scope.icons, "employment_type")
    $scope.updateValueByArray($scope.genders, "personal", "gender")
    $scope.updateValueByArray($scope.marital_statuses, "personal", "marital_status")
    $scope.updateValueByArray($scope.account_types, "bank", "account_type", true)
    $scope.updateValueByArray($scope.tax_status, "tax", "filing_status")
    $scope.updateValueByArray($scope.tax_decline_chooses, "tax", "decline_withholding")
    $scope.updateValueByArray($scope.currency_chooses, "compensation", "currency", true)
    $scope.updateValueByArray($scope.periods, "compensation", "per", true)

    #Emergency
    $scope.emergency_template =
      name: ""
      relationship: ""
      phone_number: ""

    if !$scope.user.emergency.length
      $scope.user.emergency.push(angular.copy($scope.emergency_template))

    $scope.addNewEmergency = ->
      $scope.user.emergency.push(angular.copy($scope.emergency_template))

    $scope.removeEmergency = (index) ->
      $scope.user.emergency.splice(index, 1)


    #Bank info
    $scope.bank_template =
      name: ""
      account_type: null
      id_number: ""
      account_number: ""

    if !$scope.user.bank.length
      $scope.user.bank.push(angular.copy($scope.bank_template))

    $scope.addNewBank = ->
      $scope.user.bank.push(angular.copy($scope.bank_template))

    $scope.removeBank = (index) ->
      $scope.user.bank.splice(index, 1)

    #Compensation
    $scope.compensation_template =
      amount: ""
      currency: null
      per: null
      effective: ""
      reason: ""

    if !$scope.user.compensation.length
      $scope.user.compensation.push(angular.copy($scope.compensation_template))

    $scope.addNewCompensation = ->
      $scope.user.compensation.push(angular.copy($scope.compensation_template))

    $scope.removeCompensation = (index) ->
      $scope.user.compensation.splice(index, 1)

    $scope.updateEmploymentTypeInput = ->
      $('[name="user[employment_type]"]').val($scope.user.employment_type)
      return

    $scope.submitEmployee = (eventSubmit)->
      $scope.employeeForm["user[personal][first_name]"].$dirty = true
      $scope.employeeForm["user[personal][last_name]"].$dirty = true
      $scope.employeeForm["user[employment][work_email]"].$dirty = true

      if $scope.employeeForm.$invalid
        eventSubmit.preventDefault()

    return
]