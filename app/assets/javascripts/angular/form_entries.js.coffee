formsController = angular.module("easyhr.formsController", ['ui.sortable'])

formsController.controller "FormEntryCtrl", ["$scope", "$timeout", "$compile", ($scope, $timeout, $compile)->
  $scope.answers
  $scope.formel
  $scope.form_object
  $scope.model_object


  # Add row question group
  $scope.addRowQuestionGroup = () ->
    partial = row_question_group.replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('.question-group-rows').append partial
    return

  # Add row advance question group
  $scope.addRowAdvanceQuestionGroup = (question_id) ->
    partial = advance_row_question_groups[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#question-advance-group-' + question_id).append partial
    return


  $scope.addRowExpense = (question_id)->
    partial = form_expense_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#expense-field-' + question_id + ' .table-view').after partial
    initBootstrapDP()
    return

  $scope.addRowTravel = (question_id)->
    partial = form_travel_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#travel-field-' + question_id + ' .list-group').after partial
    initBootstrapDP()
    return

  $scope.addRowCompensation = (question_id)->
    partial = form_compensation_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#compensation-field-' + question_id + ' .compensation-lists').append partial
    initBootstrapDP()
    return


  $scope.addRowTax = (question_id)->
    partial = form_tax_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#tax-field-' + question_id + ' .tax-lists').append partial
    return

  $scope.addRowBank = (question_id)->
    partial = form_bank_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#bank-field-' + question_id + ' .bank-lists').append partial
    return

  $scope.addRowEmergency = (question_id)->
    partial = form_emergency_row_partials[question_id].replace(/UNIQUE_ID/g, $scope.unique_id())
    $scope.form_object.find('#emergency-field-' + question_id + ' .emergency-lists').append partial
    return

  $scope.addRow = (question_id, field_id)->
    partial = form_row_partials[question_id]
    $scope.form_object.find(field_id).append partial
    return

  # will return unique_id
  $scope.unique_id = () ->
    (new Date()).getTime()


  $timeout (->
    $scope.form_object = $( $scope.formel )

    $('.datetimepicker').datetimepicker
      locale: 'en-gb'
      sideBySide: false
      showClose: true
      useStrict: true
      stepping: 15
      icons:
        time: "fa fa-clock-o"
        date: "fa fa-calendar"
        up: "fa fa-arrow-up"
        down: "fa fa-arrow-down"
        clear: "fa fa-eraser"
        close: "fa fa-close close-datetimepicker"


    $('.list-group .travel-row .edit-label').on 'click', (event)->
      unless $(this).hasClass('editing')
        event.preventDefault()
        $(this).closest('div.travel-row').addClass("editing")

    $('.list-group .travel-row .link-cancel').on 'click', (event)->
      event.preventDefault()
      $(this).closest('div.travel-row').toggleClass("editing")

  )
]
