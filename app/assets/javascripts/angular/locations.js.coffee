locationControllers = angular.module("easyhr.locationController", [])

locationControllers.controller "LocationIndexCtrl", ["$scope", ($scope, Restangular) ->

  $scope.showNewLocation = ()->
    $('#new-location-container').show()
    return

  $scope.hideNewLocation = ()->
    $('#new-location-container').hide()
    return

  return
]