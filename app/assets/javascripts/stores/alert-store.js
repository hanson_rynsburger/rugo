//TODO: implement alertify or toastr instead of triggering change on alert div
var Reflux = require('reflux');
var AlertActions = require("../actions/alert-action.js");

var AlertStore = Reflux.createStore({
	listenables: AlertActions,
    msg : {},
    onAlert: function(msg, type, target){
        if (typeof target === 'undefined'){
            //target will specify target for alerts (e.g; expense field, form header ,...)
            target = 'content';
        }
        this.trigger({
            type: type,
            msg: msg,
            target: target
        });
    },
    onError: function(msg, target){
        this.onAlert(msg, 'danger', target);
    },
    onSuccess: function(msg, target){
        this.onAlert(msg, 'success', target);
    }
});
export default AlertStore;
