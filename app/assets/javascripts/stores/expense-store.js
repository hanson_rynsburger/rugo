var Reflux = require('reflux');
var ExpenseActions = Reflux.createActions([
        'update'
]);

var ExpenseStore = Reflux.createStore({
	listenables: ExpenseActions,
    onUpdate: function(id, expense){
        console.log(expense);
        request
        .put('/task/'+id)
        .send(expense)
        .end(function(err, res){
            if (err){
                AlertActions.error('expense data save failed');
            }else{
                AlertActions.success('expense data saved successfully');
                //reload data
                FormActions.init();
            }
        });
    }
});
export default ExpenseStore;
