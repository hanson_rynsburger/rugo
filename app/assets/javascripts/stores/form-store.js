var Reflux = require('reflux');
var FormActions = require("../actions/form-action.js");
var AlertActions = require("../actions/alert-action.js");
var request = require('superagent');
require('superagent-csrf')(request);

var FormStore = Reflux.createStore({
	listenables: FormActions,
    data: {},
    getData: function(){
        return this.data;
    },
    onUpdateAnswer: function(id, answer, notsave){
        this.data.answers[id] = answer;
        if ((typeof notsave === 'undefined') || !notsave){
            this.onUpdate(this.data);    
        }
    },
    onDelete: function(id){
        request
        .delete('/form/' + id)
        .end(function(err, res){
            if (err){
                AlertActions.error('form data delete failed');
            }else{
                AlertActions.success('form data deleted successfully');
            }
            // TODO: Goto dashboard page
            // _this.onInit();
        });
    },
    onInit: function(){
        var _this = this;
        _this.data = task_data;
        _this.trigger();
    },
    onCreate: function(id, data){
        var formEntry = {};

        formEntry.status = 'pending';
        formEntry.description = data.description;
        if (data.start_at){
            formEntry.start_at = data.start_at;
        }
        if (data.end_at){
            formEntry.end_at = data.end_at;
        }
        formEntry.user_id = data.user_id;
        formEntry.acceptance_check = data.acceptance_check;
        formEntry.answers = {};

        var _this = this;
        var token = window._csrf;
        request
        .post('/tasks')
        .csrf(token)
        .set('Accept', 'application/json')
        .send({form_entry: formEntry, form_id: id})
        .end(function(err, res){
            if (err){
                AlertActions.error('form data save failed');
            }else{
                console.log(res);
                AlertActions.success('form data saved successfully');
                //Redirect to edit page for now
                window.location.href = '/tasks/' + res.body.id;
            }
        });
    },
    onUpdate: function(data){
        var formEntry = {};

        formEntry.id = data.id;
        formEntry.status = data.status;
        formEntry.description = data.description;
        formEntry.start_at = data.start_at;
        formEntry.end_at = data.end_at;
        formEntry.acceptance_check = data.acceptance_check;
        formEntry.answers = this.data.answers;

        var _this = this;
		var token = window._csrf;

        var r = request
        .put('/tasks/' + data.id )
		.csrf(token)
        .set('Accept', 'application/json')
        .send({form_entry: formEntry})
        .end(function(err, res){
            if (err){
                AlertActions.error('form data save failed');
            }else{
                AlertActions.success('form data saved successfully');
            }
            _this.onInit();
        });
    }
});
export default FormStore;
