//TODO: manage color scheme in db
var Reflux = require('reflux');
var d3 = require('d3');

var ColorStore = Reflux.createStore({
	init: function(){
		this.colors = d3.scale.category20();
	}
});
export default ColorStore;