var Reflux = require('reflux');
var EmployeeActions = require("../actions/employee-action.js");
var AlertActions = require("../actions/alert-action.js");
var EmployeeStore = Reflux.createStore({
	listenables: EmployeeActions,
    getData: function(){
      return this.data;
    },
    onGet: function(){
      var _this = this;
        request
        .get('/employees.json')
        .type('json')
        .end(function(err, res){
            if (err){
                AlertActions.error('employee data load failed');
            }else{
                _this.data = res.body;
                _this.trigger();
            }
        });
    }
});
export default EmployeeStore;
