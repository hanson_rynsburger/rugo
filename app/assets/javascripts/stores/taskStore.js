// Creates a DataStore
var Reflux = require('reflux');
var actions = require('../actions/tasks');
var taskStore = Reflux.createStore({

    listenables: actions,

    // Callback
    onTaskUpdate: function(value) {
        this.updateList(value);
    },
    getInitialState: function () {
        this.task = task_data;
        return this.task;
    },
    updateList: function(list){
        this.task = list;
        this.trigger(list); // sends the updated list to all listening components (TodoApp)
    },
});

module.exports = taskStore;
