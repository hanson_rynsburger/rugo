var Reflux = require('reflux');
var CurrencyRateActions = require("../actions/currency-rate-action.js");
var AlertActions = require("../actions/alert-action.js");

var CurrencyRateStore = Reflux.createStore({
	listenables: CurrencyRateActions,
  onGet: function(from, to){
    var _this = this;
    request
    .get('/api/currency.json')
    .query({from: from, to: to})
    .type('json')
    .end(function(err, res){
        if (err){
            AlertActions.error('currency rate data load failed');
        }else{
            _this.data = res.body;
            _this.trigger(from, to, res.body.rate);
        }
    });
  }
});
export default CurrencyRateStore;
