var Reflux = require('reflux');
var ValidationActions = require("../actions/validation-action.js");
var FormActions = require("../actions/form-action.js");
var FormStore = require("./form-store.js");
var AlertActions = require("../actions/alert-action.js");

var ValidationStore = Reflux.createStore({
	listenables: ValidationActions,
	validated: {},
	isValidated: function(){
        return _.reduce(this.validated, function(flag, d){
            return flag && d;
        }, true);
    },
	onInit: function(){
        var data = FormStore.getData();
        var _this = this;
		_.each(data.form.fields, function(d){
			_this.validated[d.id] = false;
		});
	},
	onValidate: function(save){
		this.trigger(save);
	},
    onValidationSuccess: function(id, save){
    	this.validated[id] = true;
    	if (save && this.isValidated()){
    		FormActions.update(FormStore.getData());
    	}
    },
    onValidationError: function(id, msg){
        this.validated[id] = false;
        AlertActions.error(msg);
    }
});
export default ValidationStore;