// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require turbolinks
//= require modernizr/modernizr
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require picturefill
//= require seiyria-bootstrap-slider
//= require_self

$(document).on('ready page:load', function(){
  var headerPos = $("header .navbar").height();
  var scrollPos = 0;
  var winWidth = 0;

  $(window).on('load scroll', function(){
    scrollPos = $(window).scrollTop();
    winWidth = $(window).width();

    if(scrollPos > headerPos){
      $("header .navbar").addClass("header-sm");
    }else{
      if(winWidth >= 768){
        $("header .navbar").removeClass("header-sm");
      }
    }
  })
  $(".scrollto").bind("click.scrollto", function(e) {
    e.preventDefault();
    var t = this.hash,
    n = $(t),
    offsetPadding = parseInt($(t).css("paddingTop"));

    $("html, body").stop().animate({
      scrollTop: n.offset().top - (offsetPadding + 10)
    }, 900, "swing", function() {
      window.location.hash = t
    })
  })
  $(window).on('resize load', function(){
    winWidth = $(window).width();

    if(winWidth <= 768){
     $("header .navbar").addClass("header-sm");
   }else{
     $("header .navbar").removeClass("header-sm");
   }
 })
  $(".navbar li a").click(function(event) {
    if($(".navbar-toggle").is(":visible")){
      $(".navbar-toggle").trigger('click');
    }
  });
});
