//= require_self

// AngularJS Stuff
//= require angular
//= require angular-sanitize
//= require angular-resource
//= require angular-elastic
//= require fullcalendar
//= require Chart

//= require angular-moment
//= require angular-strap
//= require angular-strap/dist/angular-strap.tpl
//= require restangular
//= require angular-ui-calendar
//= require angular-chart
//= require angular/timeoff_policies
//= require ng-table
//= require angular-route
//= require angular-flash
//= require angular/modules/sortable
//= require angular-rails-templates

//= require easyhr_app
//= require_tree ./easyhr
//= require angular/tasks
//= require angular/users
//= require angular/teams
//= require angular/companies
//= require angular/employees
//= require angular/timeoff_policies
//= require angular/form_builders
//= require angular/form_entries
//= require angular/timeoffs
//= require angular/locations
//= require angular/calendars
//= require angular/plan_and_billings
