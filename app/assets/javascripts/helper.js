var Helper = (function(){
	return {
		convertToAssociativeArray: function (obj){
			var result = {};
			for (var i in obj){
				result[obj[i].value] = obj[i].label;
			}
			return result;
		}
	}
})();

export default Helper;