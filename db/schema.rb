# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150630095023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "uuid-ossp"

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "billing_addresses", force: true do |t|
    t.integer  "company_id"
    t.string   "company_name"
    t.string   "address"
    t.string   "address2"
    t.string   "city"
    t.string   "zipcode"
    t.string   "state"
    t.string   "country"
    t.boolean  "primary",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uid"
  end

  add_index "billing_addresses", ["company_id"], name: "index_billing_addresses_on_company_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "description"
    t.string   "logo"
    t.text     "details"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.string   "status",         default: "new"
    t.string   "token"
    t.boolean  "use_tenant",     default: false
    t.boolean  "data_generated", default: false
    t.string   "customer_uid"
  end

  add_index "companies", ["email"], name: "index_companies_on_email", using: :btree
  add_index "companies", ["url"], name: "index_companies_on_url", using: :btree

  create_table "embedded_forms", force: true do |t|
    t.string   "klass"
    t.integer  "company_id"
    t.integer  "form_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "follows", force: true do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "form_approvals", force: true do |t|
    t.integer  "form_id"
    t.integer  "approver_id"
    t.string   "role"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "form_entries", force: true do |t|
    t.integer  "form_id"
    t.integer  "user_id"
    t.hstore   "answers"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "approver_id"
    t.string   "status",      default: "pending"
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "description"
  end

  add_index "form_entries", ["approver_id"], name: "index_form_entries_on_approver_id", using: :btree
  add_index "form_entries", ["form_id"], name: "index_form_entries_on_form_id", using: :btree
  add_index "form_entries", ["user_id"], name: "index_form_entries_on_user_id", using: :btree

  create_table "form_fields", force: true do |t|
    t.integer  "form_id"
    t.string   "field_type"
    t.string   "name"
    t.boolean  "required",   default: false
    t.hstore   "properties"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "forms", force: true do |t|
    t.integer  "creator_id"
    t.integer  "company_id"
    t.string   "name"
    t.text     "introduction"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "icon"
    t.boolean  "default",          default: false
    t.boolean  "show_on_calendar", default: false
    t.boolean  "require_sign",     default: false
  end

  create_table "hrdates", force: true do |t|
    t.string   "name"
    t.date     "start"
    t.boolean  "repeat"
    t.string   "day_type"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "end"
  end

  add_index "hrdates", ["company_id"], name: "index_hrdates_on_company_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["company_id"], name: "index_locations_on_company_id", using: :btree

  create_table "locations_hrdates", force: true do |t|
    t.integer "location_id"
    t.integer "hrdate_id"
  end

  add_index "locations_hrdates", ["hrdate_id"], name: "index_locations_hrdates_on_hrdate_id", using: :btree
  add_index "locations_hrdates", ["location_id"], name: "index_locations_hrdates_on_location_id", using: :btree

  create_table "plans", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "uid"
    t.boolean  "trial_period",                                default: false
    t.integer  "trial_duration"
    t.string   "trial_duration_unit"
    t.string   "currency_iso_code"
    t.decimal  "price",               precision: 8, scale: 2, default: 0.0
    t.integer  "num_employees",                               default: 0
  end

  create_table "subscriptions", force: true do |t|
    t.string   "uid"
    t.integer  "company_id"
    t.string   "payment_method_token"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "plan_id"
  end

  add_index "subscriptions", ["plan_id"], name: "index_subscriptions_on_plan_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "ancestry"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teams", ["ancestry"], name: "index_teams_on_ancestry", using: :btree
  add_index "teams", ["company_id"], name: "index_teams_on_company_id", using: :btree

  create_table "timeoff_policies", force: true do |t|
    t.string   "name"
    t.integer  "base_vacation_days",   default: 20,                                         null: false
    t.integer  "accrue_type",          default: 0,                                          null: false
    t.integer  "accrue_rate",          default: 0,                                          null: false
    t.boolean  "allow_negative"
    t.integer  "negative_cap"
    t.boolean  "carry_over",           default: false,                                      null: false
    t.integer  "carry_over_cap"
    t.date     "carry_over_expire_on"
    t.integer  "total_sick_days"
    t.integer  "total_personal_days"
    t.integer  "work_hours_per_day",   default: 8,                                          null: false
    t.string   "beginning_of_workday", default: "9:00 AM",                                  null: false
    t.string   "end_of_workday",       default: "5:00 PM",                                  null: false
    t.text     "work_week",            default: "---\n- mon\n- tue\n- wed\n- thu\n- fri\n", null: false
    t.text     "work_hours",           default: "--- {}\n"
    t.text     "tentures",             default: "--- []\n"
    t.text     "holidays",             default: "--- []\n"
    t.text     "work_dates",           default: "--- []\n"
    t.integer  "company_id"
    t.string   "zone",                 default: "UTC",                                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "timeoff_policies", ["company_id"], name: "index_timeoff_policies_on_company_id", using: :btree

  create_table "timeoff_policy_categories", force: true do |t|
    t.integer  "timeoff_policy_id"
    t.string   "name"
    t.string   "category_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "default",           default: false
  end

  create_table "timeoffs", force: true do |t|
    t.string   "name"
    t.integer  "author_id"
    t.integer  "user_id"
    t.integer  "assignee_id"
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "ancestry"
    t.text     "attachments"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "properties",        default: {}, null: false
    t.integer  "timeoff_parent_id"
    t.string   "timeoff_type"
    t.string   "status"
    t.integer  "category_id"
    t.text     "memo"
  end

  add_index "timeoffs", ["ancestry"], name: "index_timeoffs_on_ancestry", using: :btree
  add_index "timeoffs", ["assignee_id"], name: "index_timeoffs_on_assignee_id", using: :btree
  add_index "timeoffs", ["author_id"], name: "index_timeoffs_on_author_id", using: :btree
  add_index "timeoffs", ["category_id"], name: "index_timeoffs_on_category_id", using: :btree
  add_index "timeoffs", ["name"], name: "index_timeoffs_on_name", using: :btree
  add_index "timeoffs", ["user_id"], name: "index_timeoffs_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",          null: false
    t.string   "encrypted_password",     default: "",          null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,           null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "role"
    t.date     "join_on"
    t.string   "avatar"
    t.integer  "status",                 default: 0,           null: false
    t.integer  "employment_type",        default: 0,           null: false
    t.string   "timezone",               default: "Singapore"
    t.integer  "company_id"
    t.integer  "team_id"
    t.integer  "location_id"
    t.integer  "backup_id"
    t.integer  "policy_id"
    t.hstore   "properties",             default: {},          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "manager1_id"
    t.integer  "manager2_id"
  end

  add_index "users", ["backup_id"], name: "index_users_on_backup_id", using: :btree
  add_index "users", ["company_id"], name: "index_users_on_company_id", using: :btree
  add_index "users", ["email", "company_id"], name: "index_users_on_email_and_company_id", unique: true, using: :btree
  add_index "users", ["location_id"], name: "index_users_on_location_id", using: :btree
  add_index "users", ["manager1_id"], name: "index_users_on_manager1_id", using: :btree
  add_index "users", ["manager2_id"], name: "index_users_on_manager2_id", using: :btree
  add_index "users", ["policy_id"], name: "index_users_on_policy_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["team_id"], name: "index_users_on_team_id", using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

end
