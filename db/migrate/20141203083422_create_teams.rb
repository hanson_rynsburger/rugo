class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.text :description
      t.string :ancestry
      t.references :company, index: true
      t.timestamps
    end

    add_index :teams, :ancestry
  end
end
