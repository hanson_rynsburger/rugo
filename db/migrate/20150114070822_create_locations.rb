class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.text :data
      t.references :company, index: true

      t.timestamps
    end
  end
end
