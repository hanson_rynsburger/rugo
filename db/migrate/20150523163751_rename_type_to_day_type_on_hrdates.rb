class RenameTypeToDayTypeOnHrdates < ActiveRecord::Migration
  def change
    rename_column :hrdates, :type, :day_type
  end
end
