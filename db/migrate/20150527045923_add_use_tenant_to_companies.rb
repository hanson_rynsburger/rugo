class AddUseTenantToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :use_tenant, :boolean, default: false
  end
end
