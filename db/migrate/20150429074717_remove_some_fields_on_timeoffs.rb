class RemoveSomeFieldsOnTimeoffs < ActiveRecord::Migration
  def change
    remove_column :timeoffs, :description,  :text
    remove_column :timeoffs, :details,      :text
  end
end
