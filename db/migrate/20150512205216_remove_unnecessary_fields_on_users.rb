class RemoveUnnecessaryFieldsOnUsers < ActiveRecord::Migration
  def change
    remove_column :users, :personal, :hstore
    remove_column :users, :tax, :hstore
    remove_column :users, :bank, :hstore
    remove_column :users, :compensation, :hstore
    remove_column :users, :emergency, :hstore
    remove_column :users, :employment, :hstore
  end
end
