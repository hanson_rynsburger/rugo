class CreateLocationHrdate < ActiveRecord::Migration
  def change
    create_table :locations_hrdates do |t|
      t.belongs_to :location, index: true
      t.belongs_to :hrdate, index: true
    end
  end
end
