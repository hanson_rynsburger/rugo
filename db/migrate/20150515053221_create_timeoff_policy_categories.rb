class CreateTimeoffPolicyCategories < ActiveRecord::Migration
  def change
    create_table :timeoff_policy_categories do |t|
      t.references :timeoff_policy
      t.string :name
      t.string :category_type
      t.timestamps
    end
  end
end
