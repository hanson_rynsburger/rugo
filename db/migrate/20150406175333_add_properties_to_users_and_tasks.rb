class AddPropertiesToUsersAndTasks < ActiveRecord::Migration
  def change
    add_column :users, :properties, :hstore, :null => false, :default => ""
    add_column :tasks, :properties, :hstore, :null => false, :default => ""
  end
end
