class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.references :author, index: true
      t.references :user, index: true
      t.references :assignee, index: true
      t.datetime :start_at
      t.datetime :end_at
      t.integer :task_type
      t.string :ancestry
      t.string :workflow_state
      t.text  :attachments
      t.text :details
      t.float :amount
      t.timestamps
    end
    add_index :tasks, :ancestry
    add_index :tasks, :task_type
    add_index :tasks, :name
  end
end