class RemoveWorkflowStateFromTimeoffs < ActiveRecord::Migration
  def up
    remove_column :timeoffs, :workflow_state
  end

  def down
    add_column :timeoffs, :workflow_state, :string
  end
end
