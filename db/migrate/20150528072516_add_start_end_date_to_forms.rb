class AddStartEndDateToForms < ActiveRecord::Migration
  def change
    add_column :forms, :show_on_calendar, :boolean, default: false
  end
end
