class AddHideContactToJobs < ActiveRecord::Migration
  def up
    change_column :tasks, :task_type, :string
  end

  def down
    remove_column :tasks, :task_type
    add_column :tasksm, :task_type, :integer
  end
end
