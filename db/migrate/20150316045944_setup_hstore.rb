class SetupHstore < ActiveRecord::Migration
  def up
    # Create Schema
    execute 'CREATE SCHEMA IF NOT EXISTS shared_extensions;'

    # Enable Hstore
    execute 'CREATE EXTENSION IF NOT EXISTS HSTORE SCHEMA shared_extensions;'

    # Enable UUID-OSSP
    execute 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp" SCHEMA shared_extensions;'
  end

  def down
    execute "DROP SCHEMA IF EXISTS shared_extensions"
    execute "DROP EXTENSION IF EXISTS hstore"
    execute "DROP EXTENSION IF EXISTS uuid-ossp"
  end
end
