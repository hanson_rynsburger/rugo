class CreateTimeoffPolicies < ActiveRecord::Migration
  def change
    create_table :timeoff_policies do |t|
      t.string :name
      t.integer :base_vacation_days, :null => false, :default => 20
      t.integer :accrue_type, :null => false, :default => 0
      t.integer :accrue_rate, :null => false, :default => 0
      t.boolean :allow_negative
      t.integer :negative_cap
      t.boolean :carry_over,:null => false, :default => false
      t.integer :carry_over_cap
      t.date :carry_over_expire_on
      t.integer :total_sick_days
      t.integer :total_personal_days
      t.integer :work_hours_per_day, :null => false, :default => 8
      t.string :beginning_of_workday,:null => false, :default => "9:00 AM"
      t.string :end_of_workday,:null => false, :default => "5:00 PM"
      t.text :work_week,:null => false, :default => %w(mon tue wed thu fri)
      t.text :work_hours, :default => {}
      t.text :tentures, :default => []
      t.text :holidays, :default => []
      t.text :work_dates, :default => []
      t.text :timeoff_types,:null => false, :default => [{:name => "Vacation", :type => "PTO"}]
      t.references :company, index: true
      t.string :zone,:null => false, :default => 'UTC'
      t.timestamps
    end
  end
end
