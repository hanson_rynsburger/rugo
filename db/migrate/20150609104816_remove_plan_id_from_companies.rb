class RemovePlanIdFromCompanies < ActiveRecord::Migration
  def change
    remove_reference :companies, :plan, index: true
  end
end
