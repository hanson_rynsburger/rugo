class AddSomeFieldsToPlans < ActiveRecord::Migration
  def change
    # add columns
    add_column :plans, :uid,                  :string
    add_column :plans, :trial_period,         :boolean, default: false
    add_column :plans, :trial_duration,       :integer
    add_column :plans, :trial_duration_unit,  :string
    add_column :plans, :currency_iso_code,    :string
    add_column :plans, :price,                :decimal, precision: 8, scale: 2, default: 0

    # remove columns
    remove_column :plans, :type,              :integer
    remove_column :plans, :amount,            :float
  end
end
