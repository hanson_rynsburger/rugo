class UpdateSerializeToHstoreInUsers < ActiveRecord::Migration
  def up
    remove_column :users, :personal
    remove_column :users, :tax
    remove_column :users, :bank
    remove_column :users, :emergency
    remove_column :users, :compensation
    remove_column :users, :employment

    add_column :users, :personal, :hstore
    add_column :users, :tax, :hstore
    add_column :users, :bank, :hstore
    add_column :users, :emergency, :hstore
    add_column :users, :compensation, :hstore
    add_column :users, :employment, :hstore
  end

  def down
    remove_column :users, :personal
    remove_column :users, :tax
    remove_column :users, :bank
    remove_column :users, :emergency
    remove_column :users, :compensation
    remove_column :users, :employment

    add_column :users, :personal, :text
    add_column :users, :tax, :text
    add_column :users, :bank, :text
    add_column :users, :emergency, :text
    add_column :users, :compensation, :text
    add_column :users, :employment, :text
  end
end
