class CreateEmbeddedForms < ActiveRecord::Migration
  def change
    create_table :embedded_forms do |t|
      t.string :klass
      t.references :company
      t.references :form
      t.timestamps
    end
  end
end
