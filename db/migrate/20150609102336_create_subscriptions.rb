class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string      :uid
      t.references  :company
      t.string      :payment_method_token
      t.string      :plan_uid
      t.string      :status
      t.timestamps
    end
  end
end
