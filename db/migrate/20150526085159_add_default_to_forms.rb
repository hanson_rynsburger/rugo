class AddDefaultToForms < ActiveRecord::Migration
  def change
    add_column :forms, :default, :boolean, default: false
  end
end
