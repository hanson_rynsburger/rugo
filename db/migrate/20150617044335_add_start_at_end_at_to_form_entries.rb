class AddStartAtEndAtToFormEntries < ActiveRecord::Migration
  def change
    add_column :form_entries, :start_at,  :datetime
    add_column :form_entries, :end_at,    :datetime
  end
end
