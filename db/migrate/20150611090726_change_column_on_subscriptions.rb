class ChangeColumnOnSubscriptions < ActiveRecord::Migration
  def change
    add_column :plans, :num_employees, :integer, default: 0
    remove_column :subscriptions, :plan_uid, :string
    add_reference :subscriptions, :plan, index: true
  end
end
