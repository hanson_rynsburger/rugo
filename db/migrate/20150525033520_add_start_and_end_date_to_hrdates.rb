class AddStartAndEndDateToHrdates < ActiveRecord::Migration
  def change
    rename_column :hrdates, :date, :start
    add_column :hrdates, :end, :date
  end
end
