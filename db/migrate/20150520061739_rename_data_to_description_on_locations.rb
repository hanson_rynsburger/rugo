class RenameDataToDescriptionOnLocations < ActiveRecord::Migration
  def change
    rename_column :locations, :data, :description
  end
end
