class ChangeUsersUniqueColumn < ActiveRecord::Migration
  def up
    remove_index :users, :email
    add_index    :users, [:email, :company_id], unique: true
  end

  def down
    remove_index :users, [:email, :company_id]
    add_index    :users, :email,                unique: true
  end
end
