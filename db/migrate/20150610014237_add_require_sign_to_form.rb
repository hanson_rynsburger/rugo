class AddRequireSignToForm < ActiveRecord::Migration
  def change
  	add_column :forms, :require_sign, :boolean, default: false
  end
end
