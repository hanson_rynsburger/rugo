class AddDescriptionToFormEntry < ActiveRecord::Migration
  def change
    add_column :form_entries, :description, :string
  end
end
