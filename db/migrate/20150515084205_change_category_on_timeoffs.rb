class ChangeCategoryOnTimeoffs < ActiveRecord::Migration
  def change
    remove_column :timeoffs, :category, :string
    add_reference :timeoffs, :category, index: true
  end
end
