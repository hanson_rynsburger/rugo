class AddTimeoffTypeToTimeoffs < ActiveRecord::Migration
  def change
    add_column :timeoffs, :timeoff_type, :string
    add_column :timeoffs, :status, :string
    add_column :timeoffs, :category, :string
  end
end
