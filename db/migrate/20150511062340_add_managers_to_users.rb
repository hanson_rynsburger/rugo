class AddManagersToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :manager1, index: true
    add_reference :users, :manager2, index: true
  end
end
