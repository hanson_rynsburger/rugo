class AddDefaultToTimeoffPolicyCategories < ActiveRecord::Migration
  def change
    add_column :timeoff_policy_categories, :default, :boolean, default: false
  end
end
