class AddMemoToTimeoffs < ActiveRecord::Migration
  def change
    add_column :timeoffs, :memo, :text
  end
end
