class AddDataGeneratedToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :data_generated, :boolean, default: false
  end
end
