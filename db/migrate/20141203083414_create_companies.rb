class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :url
      t.text :description
      t.string :logo
      t.text :details
      t.references :plan, index: true
      t.timestamps
    end
  end
end