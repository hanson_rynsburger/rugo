class RenameParentIdFromTasks < ActiveRecord::Migration
  def change
    rename_column :tasks, :parent_id, :task_parent_id
  end
end
