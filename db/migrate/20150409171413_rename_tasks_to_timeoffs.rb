class RenameTasksToTimeoffs < ActiveRecord::Migration
  def up
    remove_reference  :tasks, :task_parent
    remove_column     :tasks, :task_type
    rename_table      :tasks, :timeoffs
    add_reference     :timeoffs, :timeoff_parent
  end

  def down
    remove_reference  :timeoffs, :timeoff_parent
    rename_table      :timeoffs, :tasks
    add_column        :tasks, :task_type, :string
    add_reference     :tasks, :task_parent
  end
end
