class AddHrFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :join_on, :date
    add_column :users, :avatar, :string
    add_column :users, :handle_travel, :boolean,:null => false, :default => false
    add_column :users, :handle_accouting, :boolean,:null => false, :default => false
    add_column :users, :status, :integer,:null => false, :default => 0
    add_column :users, :employment_type, :integer,:null => false, :default => 0
    add_column :users, :personal, :text
    add_column :users, :emergency, :text
    add_column :users, :bank, :text
    add_column :users, :tax, :text
    add_column :users, :employment, :text
    add_column :users, :compensation, :text
    add_column :users, :timezone, :string, :default => "Singapore"
    add_reference :users, :company, index: true
    add_reference :users, :team, index: true
    add_reference :users, :location, index: true
    add_reference :users, :backup, index: true
    add_reference :users, :policy, index: true
  end
end