class AddEmailToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :email, :string, unique: true
    add_index :companies, :email

    add_column :companies, :status, :string, default: 'new'
    add_column :companies, :token, :string
  end
end
