class RemoveTimeoffTypes < ActiveRecord::Migration
  def change
    remove_column :timeoff_policies, :timeoff_types, null: false, default: [{:name => "Vacation", :type => "PTO"}]
  end
end
