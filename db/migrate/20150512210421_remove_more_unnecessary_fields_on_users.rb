class RemoveMoreUnnecessaryFieldsOnUsers < ActiveRecord::Migration
  def change
    remove_column :users, :handle_travel,     :boolean
    remove_column :users, :handle_accouting,  :boolean
  end
end
