class AddIndexToCompanyUrl < ActiveRecord::Migration
  def change
    add_index :companies, :url
  end
end
