class AddCustomUidToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :customer_uid, :string
  end
end
