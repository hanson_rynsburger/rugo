class CreateHrdates < ActiveRecord::Migration
  def change
    create_table :hrdates do |t|
      t.string :name
      t.date :date
      t.boolean :repeat
      t.string :type
      t.references :company, index: true

      t.timestamps
    end
  end
end
