# generate company
company = Company.create!(name:"Trio Sky", url: 'triosky', email: 'ziwei0@gmail.com', url: 'triosky')

# generate default company data
company.generate_default_timeoff_policy
company.generate_default_dyno_forms

location = Location.create!(name:"New York")
team = Team.create!(name:"Ninjas")
names = ["Lucius Hokanson",
  "Treena Dipalma",
  "Josef Rager",
  "Mariette Marquis",
  "Wiley Maxham",
  "Reuben Gosha",
  "Susie Haile",
  "Gillian Soderlund",
  "Golden Kaspar",
  "Willa Clubb",
  "Delpha Upshaw",
  "Louella Grainger",
  "Alana Walkowiak",
  "Gregg Longwell",
  "Jenell Whitton",
  "Chelsie Eskridge",
  "Leandra Magnuson",
  "Albertina Joo",
  "Norene Plata",
  "Dorthy Gatz"
]
for i in 0..10
  user1 = User.find_or_create_by(email: "ziwei#{i}@gmail.com") do |u|
    u.first_name = names[i].split[0]
    u.last_name = names[i].split[1]
    u.status = 0
    u.password = "temp1234"
    u.password_confirmation = "temp1234"
    u.join_on = Date.today
    u.company = company
    u.location = location
    u.policy = company.default_policy
    u.team = team
    u.confirm!
    u.user!
  end
end

location = Location.create!(name:"Singapore")
team = Team.create!(name:"Sumo")
for i in 11..19
  user1 = User.find_or_create_by(email: "ziwei#{i}@gmail.com") do |u|
    u.first_name = names[i].split[0]
    u.last_name = names[i].split[1]
    u.status = 0
    u.password = "temp1234"
    u.password_confirmation = "temp1234"
    u.join_on = Date.today
    u.company = company
    u.location = location
    u.policy = company.default_policy
    u.team = team
    u.confirm!
    u.user!
  end
end


user1 = User.find_or_create_by(email: "ziweizhou@gmail.com") do |u|
  u.company = company
  u.first_name = "Ziwei"
  u.last_name = "Zhou"
  u.password = "temp1234"
  u.password_confirmation = "temp1234"
  u.join_on = Date.today
  u.status = 0
  u.employment_type = 0
  u.policy = company.default_policy
  # u.personal = {
  #   :middle_name => "",
  #   :street1 => "1 5TH Ave",
  #   :street2 => "Apt 1200",
  #   :city => "New York",
  #   :state => "New York",
  #   :country => "USA",
  #   :gender => "Male",
  #   :dob => "1/1/1977"
  # }
  # u.employment = {
  #   :title => "Manager",
  #   :phone => "123456",
  #   :email => "ziwei@test.com"
  # }
  # u.emergency = [{
  #   :name => "J Zhou",
  #   :relationship => "Father",
  #   :phone => "123456"
  # }]
  # u.bank = [{
  #   :name => "China Bank",
  #   :type => "Checking",
  #   :routing => "1234567",
  #   :number => "12345678"
  # }]
  # u.compensation = [{
  #   :since => '1/1/2000',
  #   :amount => 123456,
  #   :curreny => "CNY",
  #   :reason => "1234"
  #   }]
  # u.tax = {
  #   :status => ""
  # }
  u.confirm!
  u.admin!
  u.user!
end


user2 = User.find_or_create_by(email: "ziweizhou@yahoo.com") do |u|
  u.first_name = "Jet"
  u.last_name = "Lee"
  u.password = "temp1234"
  u.password_confirmation = "temp1234"
  u.company = company
  u.join_on = Date.today
  u.policy = company.default_policy
  u.confirm!
  u.user!
end

# user1 is the manager of user2
user2.follow(user1)
