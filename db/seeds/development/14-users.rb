module SeedUser
  def self.seed
    Company.all.each do |company|
      if company.use_tenant
        Apartment::Tenant.switch!(company.url)
      else
        Apartment::Tenant.switch!
      end

      # destroy all users with role: 'user'
      company.users.where(role: 0).destroy_all

      # get first admin
      admin = company.users.where(role: 1).first

      teams     = company.teams.all
      locations = company.locations.all

      (company.subscription.plan.num_employees - 1).times do |idx|
        company.users.create do |user|
          Time.zone = 'Asia/Singapore'

          user.email         = "user#{idx+1}@gmail.com"
          user.first_name    = FFaker::Name.first_name
          user.last_name     = FFaker::Name.last_name
          user.password      = '123123123'
          user.confirmed_at  = Time.zone.now
          user.timezone      = admin.timezone
          user.policy        = company.default_policy
          user.role          = 'user'
          user.team          = teams.sample
          user.location      = locations.sample
          user.manager1      = admin
          user.join_on       = (1..10).to_a.sample.years.ago.beginning_of_year
        end
      end
    end

    # back to public tenant
    Apartment::Tenant.switch!
  end
end