module SeedTeam
  def self.seed
    Company.all.each do |company|
      if company.use_tenant
        Apartment::Tenant.switch!(company.url)
      else
        Apartment::Tenant.switch!
      end

      %w{Avengers Marvel NASA Devcops Skylight}.each do |name|
        company.teams.create(name: name, description: FFaker::Lorem.sentence)
      end
    end

    # back to public tenant
    Apartment::Tenant.switch!
  end
end