module SeedCompany
  def self.seed
    Company.all.each do |company|
      begin
        if company.use_tenant && company.url.present?
          Apartment::Tenant.drop company.url
        end
        company.destroy

      rescue
        company.destroy
        next
      end
    end
    Apartment::Tenant.reload!

    # create Triosky Company
    company = Company.create!(name:"Trio Sky", url: 'triosky', email: 'ziwei@gmail.com', url: 'triosky')
    company.generate_default_data

    # assign admin
    Time.zone = 'Asia/Singapore'
    company.users.create do |admin|
      admin.email         = 'ziwei@gmail.com'
      admin.first_name    = 'Ziwei'
      admin.last_name     = 'Lastname'
      admin.password      = '123123123'
      admin.confirmed_at  = Time.zone.now
      admin.timezone      = 'Asia/Singapore'
      admin.policy        = company.default_policy
      admin.role          = 'corp_admin'
      admin.join_on       = Time.zone.now.beginning_of_year
    end

    # create braintree customer
    company.reload
    company.create_braintree_customer

    # create Codecampz Company & use tenant
    company2 = Company.create!(name:"Codecampz", url: 'codecampz', email: 'hendragunz@codecampz.com', url: 'codecampz', use_tenant: true)
    company2.create_tenant
    Apartment::Tenant.switch!(company2.url)


    # generate default data
    company2.generate_default_data

    # assign admin
    Time.zone = 'Asia/Jakarta'
    company2.users.create do |admin|
      admin.email         = 'hendragunz@codecampz.com'
      admin.first_name    = 'Hendra'
      admin.last_name     = 'Gunawan'
      admin.password      = '123123123'
      admin.confirmed_at  = Time.zone.now
      admin.timezone      = 'Asia/Jakarta'
      admin.policy        = company2.default_policy
      admin.role          = 'corp_admin'
      admin.join_on       = Time.zone.now.beginning_of_year
    end

    # create braintree customer
    company2.reload
    company2.create_braintree_customer

    # reload tenant
    Apartment::Tenant.reload!
  end
end
