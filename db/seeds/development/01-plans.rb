module SeedPlan
  def self.seed
    # make sure switch to public tenant
    Apartment::Tenant.switch!
    Plan.destroy_all

    num_employees = 5

    Braintree::Plan.all.each do |plan|
      Plan.new do |rec|
        rec.uid                 = plan.id
        rec.name                = plan.name
        rec.description         = plan.description
        rec.currency_iso_code   = plan.currency_iso_code
        rec.price               = plan.price
        rec.trial_period        = plan.trial_period
        rec.trial_duration      = plan.trial_duration
        rec.trial_duration_unit = plan.trial_duration_unit
        rec.num_employees       = num_employees

        # save and validate
        rec.save!
      end

      # increase num_employees
      num_employees += 5
    end
  end
end