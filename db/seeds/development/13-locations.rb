module SeedLocation
  def self.seed
    Company.all.each do |company|
      if company.use_tenant
        Apartment::Tenant.switch!(company.url)
      else
        Apartment::Tenant.switch!
      end

      10.times do
        company.locations.create(name: FFaker::Address.street_name, description: FFaker::Lorem.sentence)
      end
    end

    # back to public tenant
    Apartment::Tenant.switch!
  end
end