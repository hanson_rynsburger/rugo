module SeedTimeoff
  def self.seed
    Company.all.each do |company|
      if company.use_tenant
        Apartment::Tenant.switch!(company.url)
      else
        Apartment::Tenant.switch!
      end

      # by default, all company timeoff policy are
      # - accrue_by_time
      # - yearly
      # - work days: monday - friday
      # - work hours: 9am - 15pm
      # - base_vacation_days = 20
      # - total_sick_days = nil / unlimited
      #

      # for to generate yearly timeoff
      3.times{ TimeoffPolicyJob.accrue_yearly company.id, true }

      # generate 10 timeoff request each user
      company.users.where(role: 'user').each do |user|
        10.times do |idx|
          Time.zone = user.timezone

          user.timeoffs.create do |timeoff|
            # prepare category
            policy     = user.policy
            category   = policy.categories.default.where(category_type: 'vacation').first

            # prepare start and end time
            start_date = (idx + 1).weeks.from_now.beginning_of_week
            end_date   = (start_date + rand(3).days).end_of_day

            timeoff.timeoff_type = 'manual'
            timeoff.name          = "Traveling #{idx+1}"
            timeoff.start_at      = start_date
            timeoff.end_at        = end_date
            timeoff.author        = [user.manager1, user.manager2, user].compact.sample
            timeoff.category      = category
            timeoff.assignee      = user.manager1
          end
        end
      end


      # get pending timeoffs, pick 5 from them, and cancel it
      company.timeoffs.manual_entries.to_a.shuffle[0..4].each do |timeoff|
        canceler = [timeoff.user, timeoff.assignee, company.users.first].compact.uniq.sample
        timeoff.save!
        timeoff.cancel!(canceler)
      end

      # get pending timeoffs, pick 5 from them, and decline it
      company.timeoffs.manual_entries.to_a.shuffle[0..4].each do |timeoff|
        timeoff.decline!(timeoff.assignee)
      end

      # get pending timeoffs, pick 5 from them, and approve it
      company.timeoffs.manual_entries.to_a.shuffle[0..4].each do |timeoff|
        timeoff.approve!(timeoff.assignee)
      end
    end
  end
end