require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

module Clockworke
  # Accure yearly process
  #
  every(1.day, 'Accrue Rate Yearly Job', at: '00:00', tz: 'UTC', if: lamda { |time| t.day == 1 && t.month == 1 } ) {
    TimeoffPolicyJob.accrue_yearly
  }


  # Accrue monthly process
  #
  every(1.day, 'Accrue Rate Monthly Job', at: '00:00', tz: 'UTC', if: lamda { |time| t.mday == 1 } ) {
    TimeoffPolicyJob.accrue_monthly
  }


  #  Accrue weekly process
  #
  every(1.day, 'Accrue Rate Weekly Job', at: '00:00', tz: 'UTC', if: lamda { |time| t.wday == 1 } ) {
    TimeoffPolicyJob.accrue_weekly
  }


  # Carry over process
  #
  every(1.day, 'CarryOver Job', at: '0:00', tz: 'UTC', if: lamda { |time| t.day == 1 && t.month == 1 } ) {
    TimeoffPolicyJob.carry_over_cap
  }


  # Expiring carry over process
  #
  every(1.day, 'CarryOverExpiration Job', at: '0:00', tz: 'UTC' ) {
    TimeoffPolicyJob.carry_over_cap_expiration
  }

  # Tentures process
  #
  every(1.day, 'Tentures Job', at: '0:00', tz: 'UTC' ) {
    TimeoffPolicyJob.tentures_accrue
  }
end