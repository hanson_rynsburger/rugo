class CompanyConstraint
  def self.matches?(request)
    Company.where.not(url: nil).pluck(:url).include?(request.subdomain)
  end
end
