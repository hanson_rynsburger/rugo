module TimeoffPolicyJob
  module_function

  def accrue_yearly(company_id = nil, force = false)
    now = Time.zone.now.utc

    if (now.day == 1 && now.month == 1) || force
      companies = if company_id.present?
                    Company.where(id: company_id)
                  else
                    Company.all
                  end

      companies.each do |company|
        switch_schema(company) rescue next

        company.timeoff_policies.having_accrue_rate(:yearly).each do |policy|
          vacation_category = policy.categories.default.where(category_type: 'vacation').first
          personal_category = policy.categories.default.where(category_type: 'personal').first

          vacation_hours_per_year = policy.base_vacation_hours
          personal_hours_per_year = policy.total_personal_days * policy.work_hours_per_day

          policy.users.each do |user|

            # accrue process
            #
            if policy.accrue_type == :accrue_by_time_adv
              # auto add vacation hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: vacation_hours_per_year, category: vacation_category,
                              status: 'approved', name: 'Yearly-AccrueByTime Adv (Vacation)')

              # auto add personal hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: personal_hours_per_year, category: personal_category,
                              status: 'approved', name: 'Yearly-AccrueByTime Adv (Personal)')

            elsif policy.accrue_type == :accrue_by_time
              if user.months_after_join < 12
                months = 12 - user.months_after_join
                vacation_hours_per_year = (months.to_f / 12) * vacation_hours_per_year
                personal_hours_per_year = (months.to_f / 12) * personal_hours_per_year
              end

              # auto add vacation hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: vacation_hours_per_year, category: vacation_category,
                              status: 'approved', name: 'Yearly-AccrueByTime (Vacation)')

              # auto add personal hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: personal_hours_per_year, category: personal_category,
                              status: 'approved', name: 'Yearly-AccrueByTime (Personal)')

            end
          end
        end
      end
    end
  end


  def accrue_monthly(company_id = nil, force = false)
    now = Time.zone.now.utc.beginning_of_month

    if now.mday == 1
      companies = if company_id.present?
                    Company.where(id: company_id)
                  else
                    Company.all
                  end

      companies.each do |company|
        switch_schema(company) rescue next

        company.timeoff_policies.having_accrue_rate(:monthly).each do |policy|
          vacation_category = policy.categories.default.where(category_type: 'vacation').first
          personal_category = policy.categories.default.where(category_type: 'personal').first

          vacation_hours_per_month = (policy.base_vacation_hours.to_f / 12).round(2)
          personal_hours_per_month = ((policy.total_personal_days * policy.work_hours_per_day).to_f / 12).round(2)

          policy.users.each do |user|

            # accrue process
            #
            if policy.accrue_type == :accrue_by_time_adv
              # auto add vacation hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: vacation_hours_per_month, category: vacation_category,
                              status: 'approved', name: 'Auto Monthly Accrue By Time Adv')

              # auto add personal hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: personal_hours_per_month, category: personal_category,
                              status: 'approved', name: 'Auto Monthly Accrue By Time Adv')

            elsif policy.accrue_type == :accrue_by_time
              if user.months_after_join < 1
                today = now.to_date
                days_current_month = Time.days_in_month(today.month, today.year)

                vacation_hours_per_month = (user.days_after_join.to_f / days_current_month) * vacation_hours_per_month
                personal_hours_per_month = (user.days_after_join.to_f / days_current_month) * personal_hours_per_month
              end

              # auto add vacation hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: vacation_hours_per_month, category: vacation_category,
                              status: 'approved', name: 'Auto Monthly Accrue By Time')

              # auto add personal hours
              Timeoff.create!( user: user, timeoff_type: 'auto', amount: personal_hours_per_month, category: personal_category,
                              status: 'approved', name: 'Auto Monthly Accrue By Time')
            end

          end
        end
      end
    end
  end


  def accrue_weekly(company_id = nil, force = false)
    now = Time.zone.now.utc.beginning_of_week

    if now.wday == 1
      companies = if company_id.present?
                    Company.where(id: company_id)
                  else
                    Company.all
                  end

      companies.each do |company|
        switch_schema(company) rescue next

        company.timeoff_policies.having_accrue_rate(:weekly).each do |policy|
          vacation_category = policy.categories.default.where(category_type: 'vacation').first
          personal_category = policy.categories.default.where(category_type: 'personal').first

          vacation_hours_per_week = (policy.base_vacation_hours.to_f / Time.zone.now.end_of_year.to_date.cweek)
          personal_hours_per_week = ((policy.total_personal_days.to_f * policy.work_hours_per_day) / Time.zone.now.end_of_year.to_date.cweek)

          policy.users.each do |user|
            # accrue process
            #
            if policy.accrue_type == :accrue_by_time_adv
              # auto add vacation hours
              Timeoff.create( user: user, timeoff_type: 'auto', amount: vacation_hours_per_week, category: vacation_category,
                              status: 'approved', name: 'Auto Weekly Accrue By Time Adv')

              # auto add personal hours
              Timeoff.create( user: user, timeoff_type: 'auto', amount: personal_hours_per_week, category: personal_category,
                              status: 'approved', name: 'Auto Weekly Accrue By Time Adv')

            elsif policy.accrue_type == :accrue_by_time
              if user.months_after_join < 12
                vacation_hours_per_week = (user.days_after_join / 7) * vacation_hours_per_week
                personal_hours_per_week = (user.days_after_join / 7) * personal_hours_per_week
              end

              # auto add vacation hours
              Timeoff.create( user: user, timeoff_type: 'auto', amount: vacation_hours_per_week, category: vacation_category,
                              status: 'approved', name: 'Auto Weekly Accrue By Time')

              # auto add personal hours
              Timeoff.create( user: user, timeoff_type: 'auto', amount: personal_hours_per_week, category: personal_category,
                              status: 'approved', name: 'Auto Weekly Accrue By Time')
            end
          end

        end
      end
    end
  end

  def carry_over_cap(company_id = nil, force = false)
    now = Time.zone.now.utc.beginning_of_year
    if now.day == 1 && now.month == 1
      companies = if company_id.present?
                    Company.where(id: company_id)
                  else
                    Company.all
                  end

      companies.each do |company|
        # we need switch schema first, before continue modify the data
        # the schema is base on company.url
        #
        switch_schema(company) rescue next

        company.timeoff_policies.each do |policy|
          carry_over_cap_hours  = policy.carry_over_cap.to_f * policy.work_hours_per_day

          vacation_category = policy.categories.default.where(category_type: 'vacation').first
          personal_category = policy.categories.default.where(category_type: 'personal').first

          policy.users.each do |user|
            remain_vacation_hours = user.total_remain_vacation_hours
            remain_personal_hours = user.total_remain_personal_hours

            # if no carry over allowed, then expired remaining vacation and personal days (or hours)
            Timeoff.create( user: user, timeoff_type: 'auto', amount: -(remain_vacation_hours), category: vacation_category,
                            status: 'approved', name: 'Auto Yearly Expired Vacation Hours')

            Timeoff.create( user: user, timeoff_type: 'auto', amount: -(remain_personal_hours), category: personal_category,
                            status: 'approved', name: 'Auto Yearly Expired Personal Hours')

            # expiration process
            if policy.carry_over
              if carry_over_cap_hours < remain_personal_hours
                Timeoff.create( user: user, timeoff_type: 'auto', amount: carry_over_cap_hours, category: vacation_category,
                                status: 'approved', name: 'Auto Yearly Carry Over Cap for Vacation Hours')
              else
                Timeoff.create( user: user, timeoff_type: 'auto', amount: remain_personal_hours, category: personal_category,
                                status: 'approved',name: 'Auto Yearly Carry Over Cap for Personal Hours')

              end
            end
          end
        end
      end
    end
  end

  def carry_over_cap_expiration(company_id = nil, force = false)
    companies = if company_id.present?
                  Company.where(id: company_id)
                else
                  Company.all
                end

    companies.each do |company|
      switch_schema(company) rescue next

      company.timeoff_policies.where(carry_over: true)
                              .where("timeoff_policies.carry_over_cap > ?", 0)
                              .where("timeoff_policies.carry_over_expire_on IS NOT NULL").each do |policy|
        now = Time.zone.now.utc
        exp_date = policy.carry_over_expire_on

        vacation_category = policy.categories.default.where(category_type: 'vacation').first

        # check if the date is the expiration date
        # and trigger the expiration carry over cap if any
        #
        if exp_date.present? && (Time.zone.now.month == exp_date.month) && (Time.zone.now.mday == exp_date.mday)
          policy.users.each do |user|
            remain_expired_vacation_hours = user.total_remain_vacation_hours - policy.base_vacation_hours
            if remain_expired_vacation_hours > 0
              Timeoff.create( user: user, timeoff_type: 'auto', amount: -(remain_expired_vacation_hours), category: vacation_category,
                              status: 'approved', name: 'Auto expiration carry over cap')
            end
          end
        end

      end
    end
  end

  def tentures_accrue(company_id = nil, force = false)
    now = Time.zone.now.utc

    companies = if company_id.present?
                  Company.where(id: company_id)
                else
                  Company.all
                end

    companies.each do |company|
      switch_schema(company) rescue next

      company.timeoff_policies.each do |policy|
        vacation_category = policy.categories.default.where(category_type: 'vacation').first

        if policy.tentures.present?
          policy.users.each do |user|
            if (now.month == user.join_on.to_time.utc.month) && (now.mday == user.join_on.to_time.utc.mday)
              policy.tentures.each do |tenture|
                months = tenture['anniversary'].to_i * 12

                if user.months_after_join >= months
                  Timeoff.create( user: user, timeoff_type: 'auto', amount: (tenture['extra_days'].to_i * policy.work_hours_per_day), category: vacation_category,
                                  status: 'approved', name: 'Auto Tenture Accrue base on Timeoff Policy')
                end

              end
            end
          end
        end
      end
    end
  end

  def switch_schema(company)
    if company.use_tenant
      Apartment::Tenant.switch!(company.url)
    else
      Apartment::Tenant.switch!('public')
    end
  end
end