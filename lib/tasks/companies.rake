require 'rake'
require 'optparse'

namespace :company do
  desc "To create all tenant for all Companies"
  task :remove_company => :environment do |t, args|
    options = {}

    # parsing parameters
    OptionParser.new(args) do |opts|
      opts.banner = "Usage: rake company:remove -id xx"
      opts.on("-id", "--company-id {username}","Company ID", String) do |company_id|
        options[:company_id] = company_id
      end
    end.parse!

    company = Company.find(options[:company_id])

    if company.present?
      Apartment::Tenant.drop(company.url)
      company.destroy
      Rails.logger.info "Data for company with url='#{ company.url }' has been removed"

    else
      Rails.logger.info "Company not found"

    end

  end
end