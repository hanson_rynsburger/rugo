namespace :db do
  desc %q{Load dummy data for development}

  task seed_data: :environment do |t, args|
    if ENV["VERSION"]
      Dir[Rails.root.join('db', 'seeds', Rails.env, '*')].sort.each do |seed|
        next unless File.basename(seed).split('-').first.to_f == ENV["VERSION"].to_f
        puts "executing file #{seed.inspect}"
        require "#{seed}"
        klass_name = ("seed_" + File.basename("#{seed}", '.rb').split('-').last).classify
        klass = klass_name.constantize
        klass.send(:seed)
      end

    else
      Dir[Rails.root.join('db', 'seeds', Rails.env, '*')].sort.each do |seed|
        puts "executing file #{seed.inspect}"
        require "#{seed}"
        klass_name = ("seed_" + File.basename("#{seed}", '.rb').split('-').last).classify
        klass = klass_name.constantize
        klass.send(:seed)
      end
    end
  end
end