namespace :db do
  desc "To create all tenant for all Companies"
  task :create_tenant => :environment do |t, args|
    Company.all.each do |company|
      begin
        Apartment::Tenant.create(company.url) if company.url.present?
        puts "Create schema #{company.url}"

      rescue Apartment::TenantExists => e
        puts e.message
      end
    end
  end
end