if defined?(Rails::Console)
  if ENV['TENANT'].present?
    puts "Select tenant ---> #{ENV['TENANT']}"
    Apartment::Tenant.switch!(ENV['TENANT']) rescue "Tenant not found"
  end
end