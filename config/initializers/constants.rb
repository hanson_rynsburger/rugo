PER_PAGE = 10
START_PAGE = 1
ACTIVITY_AVAILABLE_KEYS = {
  task_approved: 'task.approved',
  task_cancelled: 'task.cancelled',
  task_booked: 'task.booked',
  task_paid: 'task.paid',
  task_add_attachment: 'task.attachment.add',
  task_new_comment: 'task.newcomment'
}
DEFAULT_TIMEOFF_TYPES = ['Sick', 'Other']