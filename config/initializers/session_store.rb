# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, key: '_easyhr_ror_session', domain: {
  production:   ".#{ Rails.application.secrets.domain_name }",
  development:  ".#{ Rails.application.secrets.domain_name }"
}.fetch(Rails.env.to_sym, :all)