module Enum
  module Company
    STATUS      = { options: [:new, :confirmed],
                    default: :new }
  end

  module FormTask
    TASK_TYPE   = { options: [:timeoff, :travel, :expense],
                    default: :timeoff }
  end

  module Timeoff
    TIMEOFF_TYPE = {  options: [:auto, :manual, :custom],
                      default: :manual }

    CATEGORY     = {  options: [:vacation, :personal, :sick, :tenture, :others] }

    STATUS       = {  options: [:pending, :approved, :declined, :canceled],
                      default: :pending }
  end

  module FormField
    FIELD_TYPE = {
      options: [:single_line, :boolean, :textarea, :rating, :number, :checkbox, :dropdown, :date, :datetime, :email, :website, :price,
                :file, :section, :range, :date_range, :radio, :percentage, :question_group, :advance_group, :expense, :travel, :bank,
                :personal, :emergency, :tax, :compensation],
      default: :single_line
    }
  end

  module FormEntry
    STATUS = {
      options: [:draft, :pending, :canceled, :approved],
      default: :draft
    }
  end

  module FormApproval
    ROLE = {
      options: [:submit_manager, :previous_approver_manager, :specific_person],
      default: :submit_manager
    }
  end

  module TimeoffPolicy
    ACCRUE_RATE = { options: { weekly: 0, monthly: 1, yearly: 2 },
                    default: :yearly }

    ACCRUE_TYPE = { options: { accrue_by_time: 0, accrue_by_time_adv: 1 },
                    default: :accrue_by_time }

    TYPE        = { options: [:personal, :vacation, :sick, :others] }
    TENTURE     = { options: (1..50).to_a.map{ |num| num.to_s } }
  end

  module TimeoffPolicyCategory
    CATEGORY_TYPE   = { options: [:vacation, :personal, :sick, :others],
                        default: :personal }
  end

  module Hrdate
    DAY_TYPE         = {  options: [:holiday, :workday],
                          default: :holiday }
  end
end


module FontAwesome
  AVAILABLE = [
    ['Number', 'number'],
    ['Heart', 'fa-heart'],
    ['Thumbs', 'fa-thumbs-up'],
    ['Star', 'fa-star']
  ]
end
