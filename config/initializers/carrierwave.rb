require 'carrierwave'
require 'carrierwave/storage/fog'

CarrierWave::SanitizedFile.sanitize_regexp = /[^[:word:]\.\-\+]/

CarrierWave.configure do |config|
  config.enable_processing = true

  if Rails.env.production?
    config.storage = :fog

    config.fog_credentials = {
      :provider               => 'AWS',                                               # required
      :aws_access_key_id      => Rails.application.secrets.aws_access_key_id,         # required
      :aws_secret_access_key  => Rails.application.secrets.aws_secret_access_key,     # required
      :region                 => Rails.application.secrets.aws_region,                # optional, defaults to 'us-east-1'
      :host                   => Rails.application.secrets.aws_host,                  # optional, defaults to nil
      # :endpoint               => ENV['AWS_ENDPOINT']                                # optional, defaults to nil
    }
    config.fog_provider   = 'fog/aws'
    config.fog_directory  = Rails.application.secrets.fog_directory                   # required
    config.fog_public     = true                                                      # optional, defaults to true
    config.fog_attributes = { 'Cache-Control'=> "max-age=#{365.day.to_i}" }           # optional, defaults to {}
  else
    config.storage = :file
  end
end