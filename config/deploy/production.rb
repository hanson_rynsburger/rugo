# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

server '104.131.150.32', user: fetch(:user), roles: %w{app db web}, primary: true
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
# server 'example.com', user: 'deploy', roles: %w{app web}, other_property: :other_value
# server 'db.example.com', user: 'deploy', roles: %w{db}

set :npm_flags, '--silent --no-spin'


# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any  hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

# role :app, %w{deploy@example.com}, my_property: :my_value
# role :web, %w{user1@primary.com user2@additional.com}, other_property: :other_value
# role :db,  %w{deploy@example.com}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.com/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
set :ssh_options, {
  forward_agent: true,
  auth_methods: %w(publickey),
  user: 'deployer',
}
set :default_env, {
  'DOMAIN_NAME' => 'justitguy.com',
  'SECRET_KEY_BASE' => '015731aa37d035a7a7552506eaeae523a7d271a34d8d32d6635b56e4d16a2bc9f14f1c80463fe38c9c821667dd0a269820d0c60b6de1515c5d86eea3b73d7cde',
  'TZ' => 'Asia/Singapore',
  'MANDRILL_USERNAME' => 'ziweizhou@triosky.com',
  'MANDRILL_APIKEY' => 'rImInwOUNEJbTIIgtCKRJg',
  'S3_KEY' => 'AKIAJT24F7HW37LUMIAA',
  'S3_SECRET' => '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',
  'S3_BUCKET_NAME' => 'triosky-airhost',
  'S3_REGION' => 'ap-northeast-1',
  'S3_ENDPOINT' => "https://s3-ap-northeast-1.amazonaws.com",
  'S3_HOST' => 's3-ap-northeast-1.amazonaws.com',
  'WEB_CONCURRENCY' => 2,
  'DB_POOL' => 5
}

set :rails_env, :production
set :conditionally_migrate, true
