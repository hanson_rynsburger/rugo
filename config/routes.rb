Rails.application.routes.draw do
  devise_for :users, controllers: {
    confirmations: "users/confirmations",
    sessions: "users/sessions",
    registrations: "users/registrations"
  }, skip: [:passwords]
  devise_scope :user do
    get   '/users/change_email',      to: 'users/registrations#change_email'
    get   '/users/change_password',   to: 'users/registrations#change_password'
    get   '/users/company',           to: 'users/sessions#company'
    post  '/users/company_verify',    to: 'users/sessions#company_verify'
    post  '/companies/admins',        to: 'companies/admins#create',  as: :companies_admins
    get   '/companies/:token/admin',  to: 'companies/admins#new',     as: :new_companies_admin
  end

  namespace :api do
    resource :currency, only: [:show], controller: 'currency'
  end

  # With subdomain constraint
  # -----------------------------------------
  constraints(CompanyConstraint) do
    devise_for :users, controllers: {
      passwords: "users/passwords"
    }, skip: [:registrations, :sessions, :confirmations]

    scope module: 'private', as: :company do
      namespace :admin do
        resources :forms,             except: [:show]
        resources :embedded_forms,    only: [:index, :new, :create, :destroy]
        resource  :plan_and_billings, controller: 'plan_and_billings',  only: [:show, :create] do
          collection do
            put :change_plan
          end
        end
        resource  :billing_address,   controller: 'billing_address',    only: [:update]
        resources :credit_cards,      controller: 'credit_cards',       only: [:create, :destroy]
        root to: 'forms#index'
      end

      concern :trackable do
        resources :public_activity_activities, :path => 'activities', controller: 'activities', :only => [:create]
      end

      resources :form_entries, path: '/tasks', :concerns => :trackable do
        collection do
          get :all
        end

        member do
          # delete attachment
          delete  'destroy_attachment/:field_id', to: 'form_entries#destroy_attachment',    as: :destroy_attachment
          delete  'destroy_expense_attachment/:field_id/row/:row_index/index/:file_index',  to: 'form_entries#destroy_expense_attachment', as: :destroy_expense_attachment

          # upload attachment
          # post    'upload_attachment/:field_id',  to: 'form_entries#upload_attachment',     as: :upload_attachment
          post    'upload_expense_attachment/:field_id/row/:row_index',   to: 'form_entries#upload_expense_attachment', as: :upload_expense_attachment

          # change status
          put     'change_status/:status',        action: :change_status,                   constraints: { status: /cancel|approve|/ }, as: :change_status
        end
      end

      resources :timeoff_policies, except: [:show, :edit, :update, :new] do
        resources :settings, only: [:show, :update], controller: 'timeoff_policies/settings'
      end
      resources :timeoffs, except: [:destroy], :concerns => :trackable do
        collection do
          get :logs
        end

        member do
          put 'change_status/:status',    action: :change_status,   constraints: { status: /decline|approve|cancel/ }, as: :change_status
          put 'change_assignee/:user_id', action: :change_assignee, as: :change_assignee
        end
      end
      resources :locations, except: [:new, :show]
      resources :teams,     except: [:new, :show]
      resources :employees, except: [:show] do
        collection do
          post :bulk_terminate
          post :bulk_location
          post :bulk_team

          post :import_file
          get  :dashboard
        end
      end
      resources :calendars
      namespace :employees do
        resources :export_imports, only: [:index, :new, :create] do
          collection do
            get :template
          end
        end
      end

      root to: 'employees#dashboard', as: 'company_root'
    end
  end



  # Public domain (www, nil)
  # -----------------------------------------
  # try to use the REST default action, respond_with, location features.
  constraints(PublicConstraint) do
    get 'visitors/tour'
    get 'visitors/pricing'
    get 'visitors/privacy'
    get 'visitors/terms'
    # dummy static page
    get :static_tasks_list,           to: 'visitors#static_tasks_list'
    get :static_show_expense_detail,  to: 'visitors#static_show_expense_detail'
    get :static_show_travel_detail,   to: 'visitors#static_show_travel_detail'
    get :static_show_timeoff_detail,  to: 'visitors#static_show_timeoff_detail'

    namespace :companies do
      get '/sign_up' => 'registrations#new'
      get '/welcome' => 'registrations#index'
      resources :registrations, only: [:create]
      resources :confirmations, only: [:create, :update, :new, :edit]
    end

    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  end


  # error pages
  get '/500', to: 'errors#internal_server_error', as: 'internal_error'
  get '/404', to: 'errors#not_found',             as: 'not_found'

  # root page
  root to: 'visitors#index'
end
