# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'easyhr'
set :repo_url, 'git@bitbucket.org:triosky/easyhr_ror.git'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
#set :branch, 'dev'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/deployer/apps'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

set :ssh_options, {
    verbose: :debug
}
# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
set :user, "deployer"
set :use_sudo, false
set :bundle_binstubs, nil

after 'deploy:publishing', 'deploy:restart'
after 'deploy:publishing', 'deploy:cleanup_assets'

namespace :deploy do
  task :restart do
    invoke 'unicorn:legacy_restart'
    # invoke 'unicorn:reload'
    # invoke 'clockwork:restart'
  end
  task :stop do
    invoke 'unicorn:stop'
  end
end

namespace :clockwork do
  desc "Stop clockwork"
  task :stop do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :clockworkd, "-c lib/clock.rb --pid-dir=#{cw_pid_dir} --log-dir=#{cw_log_dir} --log stop"
        end
      end
    end
  end

  desc "Clockwork status"
  task :status do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :clockworkd, "-c lib/clock.rb --pid-dir=#{cw_pid_dir} --log-dir=#{cw_log_dir} --log status"
        end
      end
    end
  end

  desc "Start clockwork"
  task :start do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :clockworkd, "-c lib/clock.rb --pid-dir=#{cw_pid_dir} --log-dir=#{cw_log_dir} --log start"
        end
      end
    end
  end

  desc "Restart clockwork"
  task :restart do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :clockworkd, "-c lib/clock.rb --pid-dir=#{cw_pid_dir} --log-dir=#{cw_log_dir} --log restart"
        end
      end
    end
  end

  def cw_log_dir
    "#{shared_path}/log"
  end

  def cw_pid_dir
    "#{shared_path}/tmp/pids"
  end

  def rails_env
    fetch(:rails_env, false) ? "RAILS_ENV=#{fetch(:rails_env)}" : ''
  end
end
